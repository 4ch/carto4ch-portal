---
title: Pilotage par l'ontologie
tags: [ontologie]
---

# Pilotage par l'ontologie
```
┌────────────────────────────────────────────┐
│ Et si l'ontologie pilotait l'application ? |
│--------------------------------------------│
│       Ontologie              Application   |
│   ┌───────────────┐                        │
│   │    O---O---O  │                        │
│   │   /   / \  |  │          ┌─────────┐   │ 
│   │  O---O---O-O  │  <====>  |   App   │   │
│   │   \ /     \|  │          └─────────┘   │
│   │   O-O---O--0  │                        |
│   └───────────────┘                        │
└────────────────────────────────────────────┘
```

Un vieux rêve des ontologistes (informatiques) est de pouvoir gérer / paramétrer / piloter le fonctionnement d'une application directement via une ou plusieurs ontologies.

C'est un objectif de SemApps à long terme, mais il en est encore loin.

Nous donnerons donc dans ce portail uniquement des recommandations à ce sujet. 

Nous parlerons des manques dans SemApps, des améliorations allant dans ce sens qui ont été mises en place et des souhaits pour l'avenir.

Voir [**Recommandations > Pilotage par l'ontologie**](/docs/recommendations/ontologies-driven)

---
title: Ateliers compétences
tags: [atelier, compétence]
---

# Ateliers compétence
Afin de maintenir le lien entre l'équipe cœur du projet et les partenaires, nous avons mis en place des rendez-vous réguliers appelés "ateliers compétences".

Pour plus d'information sur ces ateliers, voir [cette page](/docs/workshops)

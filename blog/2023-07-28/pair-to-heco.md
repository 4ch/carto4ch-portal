---
title: Remplacement de l'ontologie PAIR par HeCO
tags: [ontologie, pair, heco]
---

# Remplacement de l'ontologie PAIR par HeCO
```
┌────────────────────────┐      ┌────────────────────────┐
│         PAIR           |      │       HECO v1.0        |
│------------------------│      │------------------------│
│ ┌────────────────────┐ │----->│ ┌────────────────────┐ │
│ │   pair:hasSkill    │ │      │ │ heco:hasCompetence │ │
│ └────────────────────┘ │      │ └────────────────────┘ │
└────────────────────────┘      └────────────────────────┘
```
## Qu'est-ce l'ontologie PAIR ?
L'objectif premier de l'Assemblée virtuelle est de cartographier des communautés.
Elle a donc créé, en plus de la boite à outils SemApps, une ontologie PAIR, permettant de cartographier des **P**rojets, des **A**cteurs, des **I**dées et des **R**essources.
Plus tard, se sont ajoutés des événements et encore d'autres concepts...

## Une migration progressive...
Au début, les premières versions du démonstrateur Carto4CH cartographiaient des personnes (acteurs), des organisations et des compétences (skills). Il y avait donc dans PAIR tout ce qu'il nous fallait pour faire une preuve de concept.

En avançant dans le projet, nous avons souhaité nous éloigner de PAIR, car sa raison d'être n'était pas la même que celle d'HeCo. Nous avons donc ajouté des classes et des relations venant de HeCo en plus de PAIR. 

Pendant la première année, les deux ontologies ont très bien cohabitées, et c'est là toute la force du web sémantique de pouvoir utiliser et mélanger plusieurs ontologies ! Mais au bout d'un moment, et pour la maintenance de l'application, il est prudent de faire des choix pérennes.

## Libérons les ontologies !

Il est important de voir le démonstrateur Carto4CH comme **"une solution technique qui peut s'interfacer avec l'ontolgie HeCo"**. 
Autrement dit, **HeCo ne doit être spécifique à aucune implémentation**. 
Elle doit pouvoir vivre sa vie d'ontologie sans se préocuper de la manière dont les développeurs vont l'utiliser.

Fin juin 2023, [l'ontologie HeCo](/docs/technical-documentation/ontology) est passée en V1.0, ce qui signifie pour nous qu'elle peut entrer dans un cycle de "production" et de "maintenance", et nous avons pu commencer à envisager de nous séparer de l'ontologie PAIR.

En juillet-aout 2023, un travail d'alignement entre l'ontologie HeCo et toutes les classes / relations présentes dans le code et dans la base du démonstrateur a été réalisé pour devenir le moins possible dépendant de PAIR.

On peut dire qu'à présent, le démonstrateur utilise l'ontologie **HeCo** à 95%. 

En effet, certaines spécificités de fonctionnement de SemApps nous ont contraint à conserver des références à PAIR à certains endroits. Mais si nous avons fait cela, c'est plus pour le confort ou par simplicité, cela ne signifie pas qu'il manque des choses à l'ontologie HeCo.

## Nouvelle version de l'ontologie HeCo 1.0 !
Nous avons mis à jour l'ontologie **HeCO en version 1.0**, vous pouvez consulter cette nouvelle version, en ligne ou via un schéma, un fichier OWL, ainsi qu'un document de présentation : [Ontologie HeCo](/docs/technical-documentation/ontology)

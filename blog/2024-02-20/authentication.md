---
title: Authentification / SSO
tags: [authentification, sso, oidc]
---

# Authentification / SSO
```
┌────────────────────────────────────────────┐
│           SSO / OIDC / SOLID-OIDC          |
│--------------------------------------------│
│       Application            Server OIDC   |
│   ┌───────────────┐                        │
│   │  ┌─────────┐  │                        │
│   │  |  Login  │  │          ┌─────────┐   │
│   │  └─────────┘  │  <====>  |  OIDC   │   │
│   │  ┌─────────┐  │          └─────────┘   │
│   │  |  pass   │  │                        │
│   │  └─────────┘  │                        |
│   └───────────────┘                        │
└────────────────────────────────────────────┘
```

L'étape d'[**integration dans le cloud de 4CH**](/docs/technical-documentation/integration) nous a permis de tenter l'utilisation d'un deuxième serveur OIDC/CAS.

Vous trouverez un détail du fonctionnement de l'authentification ici : [**Doc Technique > Authentification**](/docs/technical-documentation/authentication)

Au final, nous avons ressorti des axes d'amélioration dans SemApps, permettant de donner le choix à l'utilisateur de différents modes d'authentification.

Vous trouverez ces recommandations dans la rubrique [**Recommandations > Authentification**](/docs/recommendations/authentication)

et [**Standards et notions > OIDC**](/docs/technical-documentation/standards/oidc)

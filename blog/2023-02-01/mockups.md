---
title: Mise à disposition des maquettes
tags: [maquettes]
---

# Mise à disposition des maquettes
Afin d'offir un autre media que les applications aux partenaires de Carto4ch, nous avons ajouté à ce portail les [maquettes](/docs/project-documentation/mockup) qui nous ont ensuite servi à développer nos applications.

Ces maquettes peuvent donner un autre apperçu de ce que nous souhaitons proposer comme interfaces aux utilisateurs, pour entrer leurs compétences.

Elles pourront être mises à jour régulièrement en fonction de l'évolution du projet (au même titre que les schémas).

Elles pourront aussi être réutilisées ou complétées par les partenaires.

---
title: Publication de l'ontologie en format HTML
tags: [ontologie, widoco, webvowl, owl]
---

# Publication de l'ontologie en format HTML
```
┌────────────────────────┐
│  / /  HeCo V1.0        |
│-/ /--------------------│
│/ /                     │
│ / Version              │
│/  Auteurs              │
│   Résumé               │
│   Graphe               │
│ ┌────────────────────┐ │
│ │  O---O-----O O     │ │
│ │   \ /      |/ \    │ │
│ │    O ------O   O   │ │
│ └────────────────────┘ │
│   Classes...           │
│   Propriétés...        │
└────────────────────────┘
```
Pour faciliter la consultation de l'ontologie HeCo, nous avons ajouté un nouveau format de publication **plus lisible** pour les profils non technique.

En plus de l'ontologie en format OWL/TTL, du résumé, du schéma, et du document PDF, nous fournissons une **interface HTML**.

Nous avons utilisé le logiciel [**WIDOCO**](https://github.com/dgarijo/Widoco) pour générer automatiquement cette interface à partir de notre fichier OWL.

WIDOCO devient de plus en plus un standard dans la livraison d'une ontolgie. On retrouve de plus en plus souvent ce format sur le web.

Elle décrit toutes **les classes et les propriétés**, en fournissant des liens pour naviguer plus facilement à l'intérieur du document.

WIDOCO créer deux sorties HTML, une en français et une en anglais.

Un **Graph** en réseau de l'ontologie est aussi présent et permet de visualiser l'ontologie avec le logiciel **WebVOWL**.

Pour profiter de cette nouvelle interface, rendez-vous sur la page de l'[**Ontologie HeCo**](/docs/technical-documentation/ontology)
---
title: Ajout de la complétion via des référentiels
tags: [référentiels]
---

# Ajout de la complétion via des référentiels dans SemApps
Il est à présent possible d'interroger des référentiels comme **[ESCO](https://esco.ec.europa.eu/fr)** ou **[Wikidata](https://www.wikidata.org/)** pour aider à la saisie des compétences.

Nous avons implémenté cela dans le démonstrateur MSH :
* ESCO sert pour aider à la saisie des compétences
* Wikidata aide à la saisie des thèmes (reliés à une personne)

Nous utilisons d'ailleurs **un serveur ESCO "maison"**, maintenu par des [membres de l'équipe SemApps](/docs/technical-documentation/presentation-semapps) de l'[Assemblée virtuelle](https://www.virtual-assembly.org/), contenant une copie (RDF) du référentiel ESCO.
Ce serveur maison a l'avantage de fournir un point d'accès SPARQL (même si pour l'instant, nous ne l'utilisons pas), ce qui n'est plus le cas du serveur de production.
Cela nous permettra aussi à l'avenir de le compléter par du contenu plus spécifique au monde du patrimoine culturel.

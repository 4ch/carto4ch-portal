---
title: Nouvelle application My-competences
tags: [my-competences]
---

# Nouvelle application My-competences

Pour faciliter le renseignement des compétences et simplifier l'expérience utilisat.eur.rice, nous avons ajouté au démonstrateur une nouvelle application appelée **My-competence**.

Dans cette application, l'utilisat.eur.rice se loge, et ensuite, **ne voit que ses données personnelles**. Ielle peut alors remplir son CV dans une interface épurée.

Elle pourra être déclinée en fonction de l'instance, comme par exemple dans le démonstrateur, nous l'avons appelé **My-ServerA** (pour l'instance A).

Vous retrouverez la documentation utilisat.eur.rice de cette application dans [Doc. projet > doc. démonstrateur > My-competences](/docs/project-documentation/doc-demonstrator/my-competences).

Techniquement, c'est une deuxième interface utilisant la boite à outils [SemApps](/docs/technical-documentation/presentation-semapps). Elle pointe sur les mêmes données que l'application de back-office.

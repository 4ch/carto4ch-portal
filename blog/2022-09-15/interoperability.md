---
title: L'interopérabilité arrive dans SemApps !
tags: [interopérabilité]
---

# Paramétrage de l'interopérabilité dans SemApps
Les mois d'été ont été l'occasion pour l'équipe SemApps de terminer le développement de l'interopérabilité dans SemApps.

C'est à dire qu'à partir de maintenant :
* deux serveurs SemApps peuvent partager leurs données entre eux (interconnexion), c'est à dire qu'un acteur d'un serveur A peut pointer sur les compétences d'un serveur B.
* un serveur SemApps peut devenir le miroir des données d'un autre serveur

Nous avons mis à jour les serveurs A et B du démonstrateur pour tester ces nouvelles fonctionnalités.

La notion de miroirs pourra être utilisée par la suite par le serveur "central" de la cartographie des compétences.

Vous trouverez des [schémas](/docs/schemas/distributed) expliquant ces deux nouveaux concepts.

---
title: Cartographie générale = Visualisation du serveur miroir
tags: [visualisation, flodio, miroir]
---

# Cartographie générale = Visualisation du serveur miroir
```
┌────────────────────────────────┐
│      Cartographie Générale     |
│ Visualisation du serveur Miroir|
│--------------------------------│
│     ┌────────────────────┐     │
│     │    O---O---O O-O   │     │
│     │   /   / \  |/   \  │     │
│     │  O---O---O-O O   O │     │
│     │   \ /     \|/ \ /  │     │
│     │   O-O---O-O-O O    │     │
│     └────────────────────┘     │
│                |               │
│                v               │
│ ┌───┐    ┌──────────┐    ┌───┐ │
│ | A | -> |  Miroir  | <- | B | │
│ └───┘    └──────────┘    └───┘ │
└────────────────────────────────┘
```
Suite à la visualisation des instances A et B, nous avons ajouté **le serveur miroir** au démonstrateur. Ce dernier **synchronise** régulièement les données des serveurs A et B dans une sorte de "cache" permettant de proposer une visualisation en réseau contenant les données du serveur A et du serveur B.

Le serveur mirroir est tout simplement un autre serveur SemApps que nous avons configuré pour se synchroniser avec les deux autres serveurs. Pour cela, il utilise le protocole **Activity Pub**.

Pour accéder à cette visualisation, rendez-vous sur la page de la [**Page du démonstrateur**](/docs/applications/demonstrator)

Pour comprendre le fonctionnement technique de cette interface utilisateur, rendez-vous sur la page de la [**Visualisation**](/docs/technical-documentation/visualisation)
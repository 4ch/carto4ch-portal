---
title: Finalisation de la documentation technique
tags: [documentation, SemApps, SOLID, React-admin]
---

# Finalisation de la documentation technique
```
┌────────────────────────────────┐
│     Technical documentation    |
│--------------------------------│
│ SOLID                          │
│ SemApps                        │
│ Demonstrator                   │
└────────────────────────────────┘

```
Depuis que le démonstrateur est fonctionnel en version 1.0, nous avons pu terminer la documentation technique, pour aider les développeurs à comprendre le fonctionnement du démonstrateur, comprendre comment déployer leur propre serveur, etc.

Nous n'avons pas ajouté les liens vers les nombreux PAD de développement (plus précis) pour ne pas polluer la documentation, mais n'hésitez pas à nous demander l'accès.

Vous pourrez retrouver tous ces détails techniques dans la rubrique [**Documentation technique**](/docs/category/technical-documentation-category-label)

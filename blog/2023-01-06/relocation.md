---
title: Déménagement du serveur A
tags: [infrastructure]
---

# Déménagement du serveur A
En 2022, nous avions utilisé deux hébergements (OVH & Huma-Num) pour stoquer les deux serveurs A et B.
L'objectif était de montrer l'interopérabilité jusqu'au bout.

Pour des raisons de simplification et de maintenance du [démonstrateur](/docs/applications/demonstrator), nous avons migré l'instance A sur le même hébergement Huma-Num que l'instance B.

La particularité de cette nouvelle infrastructure **multi-instance** est que les deux applications partagent le même triplestore, mais en ayant à l'intérieur de ce dernier deux dataset indépendants. Les données ne sont donc pas mélangées. Cela revient donc techniquement à la même chose qu'avant, on gagne juste en maintenance, en coût, en énergie, et nous sommes certain d'avoir pour les deux instances la même puissance machine, ce qui n'était pas le cas avant car les deux hébergements n'étaient pas dimensionnés à l'identique.

Pour comprendre la différence au niveau de l'infrastructure du serveur, vous pouvez consulter le [schema d'infrastructure](/docs/schemas/infrastructure)
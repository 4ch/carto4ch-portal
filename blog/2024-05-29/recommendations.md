---
title: Recommandations
tags: [recommandations, authentification, referentiels, rgpd]
---

# Recommandations
```
┌────────────────────────────────────────────┐
│                Recommendations             |
│--------------------------------------------│
│  ┌──────────────────┐ ┌──────────────────┐ │
│  │  Authentication  │ │       RGPD       │ │
│  └──────────────────┘ └──────────────────┘ │
│  ┌──────────────────┐ ┌──────────────────┐ │
│  │  Ontology driven │ │   Vocabularies   │ │
│  └──────────────────┘ └──────────────────┘ │
└────────────────────────────────────────────┘
```

Pour toutes les fonctionalités ou les améliorations importantes que nous n'avons pas pu mettre en place dans Carto4CH pour le moment (faute de temps, d'énergie, ou trop complexes), nous avons documenté des recommandations.

Voir [**Recommandations**](/docs/category/recommendations-category--label)

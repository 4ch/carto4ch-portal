---
title: Ajout des relations réifiées dans SemApps
tags: [relations réifiées, web sémantique, semapps]
---

# Ajout des relations réifiées dans SemApps
Pour saisir des compétences de manière plus riche, nous avions besoin de pouvoir faire fonctionner ce que l'on nomme en web sémantique des **relations réifiées** (ou relations ternaires).

C'est la possibilité d'apporter de l'information supplémentaire par dessus une relation "simple".

Par exemple, partons d'une relation du type **"Bob connait Alice"**

|Sujet|Predicat|Objet|
|-----|--------|-----|
|Bob |foaf:knows |Alice|

Si nous souhaitons qualifier plus précisément la relation entre ces deux personnes, nous appellerons cela une **réification** (et il existe plusieurs manières de faire).

C'est par exemple ce que nous avons fait ( [voir Schéma > Architecture](/docs/schemas/architecture/) ) pour gérer les **rôles** entre une personne et une organisation.

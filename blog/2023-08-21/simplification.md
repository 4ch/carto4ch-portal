---
title: Simplification du formulaire de création
tags: [formulaire, simplification]
---

# Simplification du formulaire de création
```
┌──────────────────────────┐      ┌───────────────────────────┐
│ Création                 |      │ Création                  | 
│--------------------------│      │---------------------------│
│             ┌─────────┐  │      │             ┌──────────┐  │
│ Titre       │         │  │      │ Titre       │          │  │
│             └─────────┘  |      │             └──────────┘  |
│  ┌────────────────┐      │ ---> │             ┌──────────┐  │
│  | Valider ../..  │      │      │ Description |          │  │
│  └────────────────┘      │      │             └──────────┘  │
|                          │      |  ┌────────────────┐       │
| Attention : 2 étapes !   |      │  | Enregistrer    │       │
└──────────────────────────┘      │  └────────────────┘       │
                                  └───────────────────────────┘
```
## Avant
Jusqu'à présent, **la création** d'une ressource dans les formulaires du démontrateur (dans l'interface BO ou MY), se faisait en deux temps. D'abord, le titre et une première validation (qui créait la ressource en base), puis un formulaire complémentaire, où on retrouvait la suite des champs qui composent la ressource.

**Problématique :** La plupart du temps, l'utilisat.eur.rice ne savait pas quoi entrer dans le champ "titre", car il ne voyait pas encore le contenu global du formulaire.

## A présent
Nous l'avons donc simplifié en fusionnant tout dans le même formulaire.

La majorité des formulaires du démontrateur ont été revus pour simplifier leur saisie.

# Suppression du titre pour les compétences
```
┌──────────────────────────┐      ┌───────────────────────────┐
│ Création compétence      |      │ Création compétence       | 
│--------------------------│      │---------------------------│
│             ┌─────────┐  │      │             ┌──────────┐  │
│ Titre       │         │  │      │ Description │          │  │
│             └─────────┘  |      │             └──────────┘  |
│             ┌─────────┐  │ ---> │  ┌────────────────┐       │
│ Description |         │  │      │  | Enregistrer    │       │
│             └─────────┘  │      │  └────────────────┘       │
|  ┌────────────────┐      │      └───────────────────────────┘
|  | Enregistrer    │      |                                           
│  └────────────────┘      │                                           
└──────────────────────────┘                                                
```
Dans le même ordre d'idée, les utilisateurs ne savaient pas quoi entrer dans le champ "titre" des compétences. 

Nous l'avons donc retiré, et nous avons généré un titre automatique en fonction de l'identifiant de l'utilisateur et d'un numéro incrémental.

Pour plus d'informations sur les interfaces du démonstrateur, consultez les manuels utilisateurs  : 
* [My-competences](/docs/project-documentation/doc-demonstrator/my-competences)
* [Back-office](/docs/project-documentation/doc-demonstrator/back-office)


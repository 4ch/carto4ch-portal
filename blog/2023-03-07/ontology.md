---
title: Nouvelle version de HeCo v0.8
tags: [ontologie, compétence]
---

# Nouvelle version de HeCo v0.8

Publication d'une nouvelle version de l'ontologie HeCo.

* [Page de l'ontologie](/docs/technical-documentation/ontology)
* [Schéma correspondant](/docs/schemas/ontology)
---
title: Ajout d'une page pour SemApps
tags: [semapps]
---

# Ajout d'une page pour SemApps

Vu que **SemApps** est pleinement utilisé dans ce projet, il semblait important de lui faire une entière place dans ce portail.

Au même niveau que SOLID, nous lui avons ajouté une [page de référence](/docs/technical-documentation/presentation-semapps), avec un lien vers le site https://semapps.org, qui a récemment été mis à jour suite à une nouvelle version stable.

Nous avons ajouté un [schéma](/docs/schemas/architecture), permettant d'expliquer la notion de "boite à outils sémantique" qui résume bien ce qu'est SemApps.
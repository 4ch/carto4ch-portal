---
title: Renommage des instances du démonstrateur
tags: [documentation, démonstrateur]
---

# Renommage des instances du démonstrateur

Depuis le début du projet, nous avions utilisé des noms d'organisations pour appeler les deux serveurs du démonstrateur.
Par soucis de neutralité, nous les appelons à présent **Server A** et **Server B**.

Nous avons donc renommé tous les éléments faisant référence aux organisations : 
* dans ce portail
* dans les URL d'accès au démonstrateur
* dans les schémas
* dans les fichiers du serveur

:::caution Attention
Les URLs d'accès aux interfaces du démonstrateur ont changées suite à cette opération.

Pensez à mettre à jour vos favoris !

**Accès à l'instance Server A**
* My-ServerA : https://my-a.carto4ch.huma-num.fr
* Back-office Server A : https://bo-a.carto4ch.huma-num.fr
* Data : https://data-a.carto4ch.huma-num.fr

**Accès à l'instance Server B**
* My-Server B : https://my-b.carto4ch.huma-num.fr
* Back-office Server B : https://bo-b.carto4ch.huma-num.fr
* Data : https://data-b.carto4ch.huma-num.fr
:::

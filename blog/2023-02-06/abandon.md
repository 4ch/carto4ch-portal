---
title: Abandon des relations réifiées et référentiels manuels
tags: [démonstrateur, relations réifiées, semapps]
---

# Abandon des relations réifiées et référentiels manuels

## Abandon des relations réifiées
Pour des raisons techniques et de délai d'avancement, nous avons décidé d'abandonner le mécanisme des relations réifiées pour mettre à disposition une première version fonctionnelle contenant la gestion : 
* des formations
* des postes
* des contributions
* des compétences (avec 4 qualités fixes)

En effet, après quelques mois d'implémentation complexe et de difficulté à expliquer l'interface lors des [ateliers de compétences](/docs/workshops), nous nous sommes orienté vers une solution plus simple, consistant à figer 4 qualités, reliées chacunes à un référentiel : 
* Discipline
* Secteur
* Objet d'étude
* Outil

**Avantage :** L'utilisat.eur.rice sera ainsi plus guidé dans sa saisie des compétences

**Inconvénient :** Si nous souhaitons à l'avenir ajouter une nouvelle qualité, nous devrons repasser par le code.

## Remise à plus tard des référentiels automatiques
Nous avons aussi décidé d'utiliser pour le moment des référentiels internes, renseignés par les saisies de chac.un.e dans chaque serveur. 

PS : Nous continuons d'utiliser les référentiels ESCO et Wikidata pour l'aide à la saisie.

**Avantage :** Gain de temps et simplicité d'architecture. Nous souhaitons commencer petit, mais que cela fonctionne, car cela reste un démonstrateur. Cela permettra d'avoir un début de référentiel pour le jour où nous aurons besoin d'en créer un automatique.

**Inconvénient :** Cela signifie qu'au début, les listes de choix ne seront pas beaucoup remplies, mais qu'au fil des saisies, ces listes se complèteront avec les qualités créées par chacun.e dans chaque instance.

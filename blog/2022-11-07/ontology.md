---
title: Publication d'une première version de l'ontologie HeCo
tags: [ontologie, compétence]
---

# Publication d'une première version de l'ontologie HeCo
Après un premier travail de l'équipe cœur de Carto4CH, nous avons stabilisé **une première version de l'ontologie HeCo** (version 0.5).

Celle-ci permettra d'avoir un premier support pour échanger avec les partenaires sur le projet.

L'objectif est que cette ontologie soit remise en cause, éprouvée, jusqu'à se stabiliser en vue d'être utilisée dans la cartographie.

Pour plus d'information sur cette ontologie, voir [cette page](/docs/technical-documentation/ontology)

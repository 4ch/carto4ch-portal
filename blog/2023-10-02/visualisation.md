---
title: Visualisation en réseau de la cartographie d'une instance
tags: [visualisation, flodio]
---

# Visualisation en réseau de la cartographie d'une instance
```
┌────────────────────────┐
│      Visualisation     |
│        en réseau       |
│------------------------│
│ ┌────────────────────┐ │
│ │    O---O---O O-O   │ │
│ │   /   / \  |/   \  │ │
│ │  O---O---O-O O   O │ │
│ │  |\ /     \|/ \ /  │ │
│ │  O-O-O---O-O-O O   │ │
│ │   \ /   /  |/ /    │ │
│ │    O --O---O-O     │ │
│ └────────────────────┘ │
└────────────────────────┘
```
Pour faciliter la visualisation des données de la cartographie Carto4CH, nous proposons comme prévu initialement, une **représentation en réseau**.

Cela permet de montrer l'avantage d'avoir des données interopérables en ayant une troisième interface utilisatrice qui accède aux mêmes données.

Dans un premier temps, nous allons effectuer une visualisation par instance Carto4CH.

On y retrouve les acteurs, les projets, les compétences, les postes, les contributions, les formations et les qualités, reliés entre eux par les relations prévues par l'**ontologie HeCo**.

Pour accéder à cette visualisation, rendez-vous sur la page de la [**Page du démonstrateur**](/docs/applications/demonstrator)

Pour comprendre le fonctionnement technique de cette nouvelle interface, rendez-vous sur la page de la [**Visualisation**](/docs/technical-documentation/visualisation)
---
title: Déploiement d'un nouveau serveur
tags: [documentation, démonstrateur]
---

# Déploiement d'un nouveau serveur

Le début de la phase de production commence.

Nous avons mis à disposition des partenaires un "paquet" technique leur permettant d'installer et de paramétrer leur propre serveur de production.

C'est un paquet reprenant le code du démonstrateur, mais pour un seul serveur.

Pour plus d'informations, consultez la rubrique [Documentation technique > Déploiement d'un serveur](/docs/technical-documentation/server-deploy)

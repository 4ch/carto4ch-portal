---
title: Publication du livre blanc
tags: [livre blanc]
---

# Publication de la première version du livre blanc
Suite à la vingtaine d'entretiens effectués depuis mi avril 2022 avec nos partenaires, nous publions une première version de notre livre blanc, contenant la synthèse de nos échanges.

Pour le consulter, ou le télécharger en PDF, rendez-vous dans la rubrique : [Documentation projet > Livre blanc](/docs/project-documentation/white-paper/presentation)

Dans la prochaine version, nous ajouterons une synthèse de notre veille de l'existant en matière de cartographie des compétences dans le domaine du patrimoine culturel.

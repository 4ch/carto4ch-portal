---
title: Migration React-Admin 4
tags: [technique, refactoring]
---

# Migration React-Admin 4
```
┌────────────────────────┐
│ Démonstrateur Carto4CH |
│------------------------│
│ ┌────────────────────┐ │
│ │      SemApps       │ │
│ │--------------------│ │
│ │ ┌────────────────┐ │ │
│ │ │  React-Admin   │ │ │
│ │ └────────────────┘ │ │
│ └────────────────────┘ │
└────────────────────────┘
```
## Qu'est-ce que React-Admin ?
L'interface utilisatrice du démonstrateur Carto4CH est basée sur la boite à outil **SemApps**, qui elle même est dépendante d'un socle technique (framework) appelé [**React-Admin**](https://marmelab.com/react-admin/). Ce socle technique est maintenu par la société (française) [**Marmelabs**](https://marmelab.com/fr/). Il utilise la bibliothèque JavaScript [**React**](https://fr.legacy.reactjs.org/).

En résumé, les composants SemApps utilisés pour l'interface utilisatrice, sont en fait un assemblage de composants issus du framework React-Admin. Ces composants ont été adapté (écriture de "sur-couches") afin qu'ils puissent s'interfacer avec un (ou plusieurs) serveurs SemApps (qui eux sont codés en Node JS, une autre bibliothèque Javascript).

## Pourquoi migrer ?
Cette nouveauté concerne une étape importante de [**réusinage de code**](https://fr.wikipedia.org/wiki/R%C3%A9usinage_de_code), qui permettra à notre démonstrateur : 
* d'être à jour par rapport aux composants de base dont il dépend, 
* d'être plus stable, en bénéficiant des corrections de bugs
* d'accéder à de nouvelles fonctionalités présentes ou futures

Nous allons donc profiter de cette migration pour vous donner des détails techniques sur son fonctionnement, en vulgarisant au maximum.

## Explication
Depuis le début du projet Carto4CH, nous utilisions la version 3 de React-Admin. Or, depuis quelques mois, Marmelab a sorti une version majeure, la version 4, et il nous fallait donc "migrer" le code des composants SemApps dans cette version, ainsi que toute les interfaces qui utilisent ces composants. 
Pour l'Assemblée virtuelle, cette opération est loin d'être neutre, et a nécessité plusieurs semaines à nos équipes pour mettre à jour les composants SemApps, pour qu'ils fonctionnent avec la nouvelle version de React-Admin. 
Cela a été terminé début juillet, ce qui nous a permis de mettre à jour le code de l'interface du démonstrateur Carto4CH à notre tour.

Ces adaptations ont d'abord été faites sur le **back-office**, puis sur **My-compétences**.

Une fois que nous sommes arrivé à iso-fonctionalité, nous nous sommes rendu compte que cette nouvelle version 4.0 permettait de simplifier certaines interfaces, ou de simplifier l'écriture du code, car en version 3, nous avions rajouté des "pansements" à cause de bugs qui ont été corrigés en version 4.
Nous avons donc repassé en revue une deuxième fois le code du démonstrateur, pour qu'il soit plus facile à maintenir à l'avenir.

Pour plus d'informations sur le fonctionnement interne de **SemApps**, consultez la rubrique [Documentation technique > Doc. technique SemApps](/docs/technical-documentation/presentation-semapps)

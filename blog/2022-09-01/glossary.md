---
title: Publication du lexique et du tutoriel SOLID
tags: [lexique]
---

# Publication du lexique et du tutoriel SOLID
Les mois d'été ont été l'occasion de terminer le lexique, ainsi que le tutoriel SOLID.

Ces deux livrables sont intimement liés, car l'un repose sur le code de l'autre.
En effet, ce lexique est aussi là pour vous montrer ce que nous pouvons faire avec SOLID.

Nous pourrions imaginer plus tard un lexique partagé basé sur des serveurs SOLID distribués dans le domaine du patrimoine culturel français (voir européen)... Mais ce n'est pas l'objet de ce projet, et nous nous contenterons d'un petit lexique et d'un modeste tutoriel... :)

Pour le consulter, rendez-vous dans la rubrique : [Documentation projet > Lexique](/docs/project-documentation/glossary)

Ce lexique pourra être complété tout au long du projet.

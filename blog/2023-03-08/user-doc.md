---
title: Documentation utilisat.eur.rice du démonstrateur
tags: [documentation, démonstrateur]
---

# Documentations utilisat.eur.rice du démonstrateur

Nous avons ajouté des documentations permettant aux utilisateurs d'ajouter plus simplement leurs compétences et de comprendre les différentes interfaces proposés par le [démonstrateur](/docs/applications/demonstrator).

Pour le moment, il existe 3 interfaces développées dans le démonstrateur : 
* [My-competences](/docs/project-documentation/doc-demonstrator/my-competences)
* [Back-office](/docs/project-documentation/doc-demonstrator/back-office)
* [Carto](/docs/project-documentation/doc-demonstrator/carto)


---
title: Maintenance d'un serveur Carto4CH
tags: [Compact, Backup]
---

# Maintenance d'un serveur Carto4CH
```
┌───────────────────────────────────┐
│           Maintenance             |
│-----------------------------------│
│   ___                     ___     │
│  /   \      ┌───\        /   \    │
│ |\___/|     |    \      |\___/|   │
│ |     | <=  |     |  => |     |   |
│ |     |     |     |     |     │   |
│  \___/      └─────┘      \___/    |
│                                   |
└───────────────────────────────────┘
```

Nous avons ajouté une nouvelle section "Maintenance", afin d'expliquer comment maintenir un serveur Carto4CH en production.

Cela inclue : 
* l'accès à l'interface web du triplestore
* le compactage de la base de donnée
* la sauvegarde des données du triplestore

Voir [**Maintenance**](/docs/technical-documentation/maintenance)
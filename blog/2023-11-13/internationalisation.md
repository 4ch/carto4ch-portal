---
title: Internationalisation des interfaces utilisatrices
tags: [internationalisation, SemApps, React-admin]
---

# Internationalisation des interfaces utilisatrices
```
┌────────────────────────────────┐
│      Internationalisation      |
│--------------------------------│
│          ┌───────────────────┐ │
│          |  ENGLISH      v   | │
│          └───────────────────┘ │
│           |      ENGLISH    │  |
│           |      FRENCH     │  |
│           └─────────────────┘  │
└────────────────────────────────┘

```
Afin de préparer une présentation du démonstrateur à l'équipe européenne du [projet 4CH](https://www.4ch-project.eu/), nous avons, comme prévu initialement, effectué la traduction de toutes les interface. 

Le Back-office, ainsi que My-competences sont donc à présent, par défaut, en anglais, et il y a la possibilité de passer en français dans le menu en haut à droite. Le choix de la langue par défaut est paramétrable lors du déploiement du serveur.

Pour comprendre le fonctionnement technique de cette fonctionalité, rendez-vous sur la page de la [**Internationalisation**](/docs/technical-documentation/presentation-semapps#internationalisation)
---
title: Ajout des postes, des formations et des contributions
tags: [postes, formations, contributions]
---

# Ajout des postes, des formations et des contributions
En ce début 2023, nous avons ajouté au [démonstrateur](/docs/applications/demonstrator) de nouveaux concepts, en lien avec l'[ontologie version 0.7](/docs/schemas/ontology/).

A présent, vous allez en quelque sorte pouvoir "remplir votre CV" dans votre profil.
Pour cela, plusieurs nouveaux concepts sont à votre disposition : 
* Créer des **professions** (métiers)
* Créer des **postes** (expériences professionnelles)
* Créer des **formations** (expériences liées à vos études)
* Créer des **contributions** (bénévoles)

Une fois ces objets créés dans la base, vous allez pouvoir les ajouter dans votre profil, et visualiser votre CV personnel. Le profil des utilisateur étant public par défaut, tout le monde peut ainsi parcourir les "compétences" des autres utilisateurs.

---
title: Internationalisation du portail
tags: [Internationalisation]
---

# Internationalisation du portail
Nous avons commencé à traduire ce portail, qui servira aussi de communication vers le [projet 4CH](https://www.4ch-project.eu/).

Progressivement, toutes les pages et toutes les présentations publiées seront traduites en anglais.

Par défaut, la langue sera en français tant que nous nous adressons à la communauté française pour le déploiement de la cartographie.

Vous pouvez accéder à la version en anglais en modifiant la langue grâce au menu en haut à droite.

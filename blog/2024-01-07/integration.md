---
title: Integration dans le cloud de 4CH
tags: [Integration, Cloud, 4CH]
---

# Integration dans le cloud de 4CH
```
┌───────────────────────────────────┐
│  Carto4CH    ===>     Cloud 4CH   |
│-----------------------------------│
│                         ___       │
│     ___                /___\      │
│    /   \              /     \     │
│   |     |    ===>    /_______\    |
│    \___/            /         \   |
│                    └───────────┘  |
│                                   |
└───────────────────────────────────┘
```

Nous avons tenté d'intégrer notre projet Carto4CH dans le cloud 4CH.

C'était très intéressant de comprendre leur architecture, nous avons appris de l'expérience, mais nous avons manqué d'anticipation.
En effet, nous avons pris trop de retard et le projet s'est arrêté fin décembre 2023, avant que nous puissons terminer l'intégration.

Nous avons quand même pu en ressortir des bonnes pratiques au niveau : 
* du SSO / Authentification
* du mode de déploiement attentu dans un cloud par l'équipe 4CH.

Nous espérons pouvoir la reprendre lorsque la suite de 4CH sera mise en place.

Vous retrouverez ce que nous avons retenu : 
* Au niveau de l'[**Integration dans le cloud de 4CH**](/docs/technical-documentation/integration)
* Au niveau de l'[**Authentification (SSO)**](/docs/technical-documentation/authentication)

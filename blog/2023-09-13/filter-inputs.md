---
title: Relation(s) entre les compétences et les postes/contributions/formations
tags: [interface, input, ontologie]
---

# MY - Relation(s) entre les compétences et les postes/contributions/formations

## Besoin de ne voir que mes données personnelles
```
┌────────────────────────┐
│    MY-competences      |
│------------------------│
│ ┌────────────────────┐ │
│ │ Mes postes         │ │
│ │ Mes contributions  │ │
│ │ Mes formations     │ │
│ └────────────────────┘ │
└────────────────────────┘
```
Sur l'interface de **My-compétences**, il est possible de relier les compétences aux postes, aux contributions et aux formations.

L'objectif de My-compétence est que l'utilisateur ne voit que ses données personnelles et pas celles remplies par les autres comme dans le Back-office.

Donc nous avons **filtré les listes de choix** afin que chacun.e ne puisse créer des liens que vers les postes, contributions ou formations qu'ielle a créé.

Pour plus d'information, voir le [manuel utilisateur de My-compétences](/docs/project-documentation/doc-demonstrator/my-competences)

## Utilisation d'un seul prédicat
```sas=
┌───────────────────────┐       ┌────────────┐
│ Competences           │       │ Discipline │
│-----------------------│       │------------│
│ id                    │   ┌──>│ uri        │
│ titre                 │   │   │ titre      │
│ description           │   │   │ description│
│ heco:hasDiscipline    │───┘   └────────────┘
│ xxxxxx                │       ┌────────────┐
│ xxxxxx                │       │ Poste      │
│ xxxxxx                │       │------------│
│ xxxxxx                │   ┌──>│ uri        │
│ xxxxxx                │   │   │ titre      │
│ xxxxxx                │   │   │ description│
│ heco:isCompAcquiredIn │───┘?  └────────────┘
└───────────────────────┘   │   ┌──────────────┐
                            │   │ Contribution │
                            │   │--------------│
                            └──>│ uri          │
                                │ titre        │
                                │ description  │
                                └──────────────┘       
```

Au niveau ontologie HeCo, nous avons souhaité n'utiliser qu'un seul prédicat (relation) entre une compétence et un poste / contribution / formation. Ainsi, le jour où nous ajouterons un nouvel objet, nous n'aurons pas à changer l'ontologie.

Pour permettre cela, nous avons dû ajouter une customisation côté serveur, afin qu'il fasse croire au client qu'il utilise un prédicat différent pour chaque objet, tout en conservant un seul prédicat en base.

Pour plus d'information sur HeCo, voir [Ontologie HeCo](/docs/technical-documentation/ontology)
import React from 'react';
import clsx from 'clsx';
import styles from './styles.module.css';
import useDocusaurusContext from '@docusaurus/useDocusaurusContext';
import Translate, {translate} from '@docusaurus/Translate';
import useBaseUrl from '@docusaurus/useBaseUrl';

// const FeatureList = [
//   {
//     title: 'Standardisée',
//     Svg: 'img/standardized.svg',
//     description: (
//       <>
//         Basée sur les standards du web sémantique (RDF, SPARQL, LDP, SOLID)
//       </>
//     ),
//   },
//   {
//     title: 'Distribuée',
//     Svg: 'img/distributed.svg',
//     description: (
//       <>
//         La cartographie est l'agrégation des données standardisées provenant de différents serveurs.
//       </>
//     ),
//   },
//   {
//     title: 'Respectant les données personnelles',
//     Svg: 'img/GDPR.svg',
//     description: (
//       <>
//         Chaque partenaire reste maitre de ses données.
//       </>
//     ),
//   },
// ];
//
// function Feature({Svg, title, description}) {
//   const {siteConfig} = useDocusaurusContext();
//   return (
//     <div className={clsx('col col--4')}>
//       <div className="text--center">
//         <img src={siteConfig.baseUrl + Svg} className={styles.featureSvg}/>
//       </div>
//       <div className="text--center padding-horiz--md">
//         <h3>{title}</h3>
//         <p>{description}</p>
//       </div>
//     </div>
//   );
// }

export default function HomepageFeatures() {
  return (
    <section className={styles.features}>
      <div className="container">
        <div className="row">
          <div className="col col--4">
            <div className="text--center">
              <img src={useBaseUrl('/img/standardized.svg')} className="featureSvg_src-components-HomepageFeatures-styles-module"></img>
            </div>
            <div className="text--center padding-horiz--md">
              <h3><Translate>title-feature-1</Translate></h3>
              <p>
                <Translate>
                  description-feature-1
                </Translate>
              </p>
            </div>
          </div>
          <div className="col col--4">
            <div className="text--center">
              <img src={useBaseUrl('/img/distributed.svg')} className="featureSvg_src-components-HomepageFeatures-styles-module"></img>
            </div>
            <div className="text--center padding-horiz--md">
              <h3><Translate>title-feature-2</Translate></h3>
              <p>
                <Translate>
                  description-feature-2
                </Translate>
              </p>
            </div>
          </div>
          <div className="col col--4">
            <div className="text--center">
              <img src={useBaseUrl('/img/GDPR.svg')} className="featureSvg_src-components-HomepageFeatures-styles-module"></img>
            </div>
            <div className="text--center padding-horiz--md">
              <h3><Translate>title-feature-3</Translate></h3>
              <p>
                <Translate>
                  description-feature-3
                </Translate>
              </p>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
}

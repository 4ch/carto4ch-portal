import React from 'react';
import clsx from 'clsx';
import Link from '@docusaurus/Link';
import useDocusaurusContext from '@docusaurus/useDocusaurusContext';
import Layout from '@theme/Layout';
import HomepageFeatures from '@site/src/components/HomepageFeatures';
import Translate, {translate} from '@docusaurus/Translate';
import { Button } from '@mui/material';
import MenuBookIcon from '@material-ui/icons/MenuBook';

import styles from './index.module.css';

function HomepageHeader() {
  const {siteConfig} = useDocusaurusContext();
  return (
    <header className={clsx('hero hero--primary', styles.heroBanner)}>
      <div className="container">
        <h1 className={styles.hero__title}>
          <Translate>big-title-1</Translate>
        </h1>
        <h1 className={styles.hero__title}>
          <Translate>big-title-2</Translate>
        </h1>
        <p className="hero__subtitle">
          <Translate>tag-line</Translate>
        </p>
        <Button variant="contained" href="docs/home" startIcon={<MenuBookIcon />} >
          <Translate>Home</Translate>
        </Button>
      </div>
    </header>
  );
}

export default function Home() {
  const {siteConfig} = useDocusaurusContext();
  return (
    <Layout
      title={`${siteConfig.themeConfig.navbar.title}`}
      description="4CH, Carto 4CH, cartographie, cartography, compétences, acteurs, competences, actors">
      <HomepageHeader />
      <main>
        <HomepageFeatures />
      </main>
    </Layout>
  );
}

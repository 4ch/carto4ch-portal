import React, { useEffect, useState } from 'react';
import {
  getSolidDataset,
  getThingAll,
  getDatetime,
  getUrl,
  getStringNoLocale,
  setStringNoLocale,
  setThing
} from "@inrupt/solid-client";
import {
  Table,
  TableColumn
} from "@inrupt/solid-ui-react";
import { SKOS, RDF, FOAF } from "@inrupt/vocab-common-rdf";
import Showdown from "showdown"
import ReactHtmlParser, { processNodes, convertNodeToElement, htmlparser2 } from 'react-html-parser';

const RDF_TYPE = RDF.type
const SKOS_CONCEPT = SKOS.Concept
const SKOS_PREFLABEL = SKOS.prefLabel
const SKOS_ALTLABEL = SKOS.altLabel
const SKOS_DEFINITION = SKOS.definition
const FOAF_HOMEPAGE = FOAF.homepage

export default function ListTerm(props){
//  console.log("FetchUriGlossary/props=",props);

  const [termList, setTermList] = useState()
  const [loading, setLoading] = useState(true)
  let myProfileDataset = "";

  // Markdown to Html for altLabel
  const converter = new Showdown.Converter();

  useEffect( () => {
      (async function () {
        myProfileDataset = await getSolidDataset(props.uriGlossary)
        setTermList(myProfileDataset);
        setLoading(false)
     })()
   }, []);

   // console.log("termList=",termList);

   const termThings = termList ? getThingAll(termList) : [];

   // console.log("termThings=",termThings);

   // Add html link on the prefLabel
   termThings.forEach( (term, index, table) => {
     const skosPreflabel = getStringNoLocale(term, SKOS_PREFLABEL)
     const foafHomepage = getUrl(term, FOAF_HOMEPAGE)
     const htmlLink = "<a href="+foafHomepage+" target='blank'>"+skosPreflabel+"</a>"
     table[index] = setStringNoLocale(term, SKOS_PREFLABEL, htmlLink)
   })

    const thingsArray = termThings
      .filter( (t) => {
        const url = getUrl(t, RDF_TYPE)
        return url === SKOS_CONCEPT
      })
      .map((t) => {
        return { dataset: termList, thing: t };
      });

   // console.log("thingsArray=",thingsArray);

   if (loading)
    return "Loading..."

    return (
        <>
        <Table className="table" things={thingsArray}>
          <TableColumn
            property={SKOS_PREFLABEL}
            header="Terme"
            sortable
            body= {( {value} ) => {
              const prefLabelHtml = converter.makeHtml(value);
              return <div>{ ReactHtmlParser(prefLabelHtml) }</div>;
            }}
          />
          <TableColumn
            property={SKOS_ALTLABEL}
            header="Signification"
            sortable
            body= {( {value} ) => {
              const altLabelHtml = converter.makeHtml(value);
              return <div>{ ReactHtmlParser(altLabelHtml) }</div>;
            }}
          />
          <TableColumn
            property={SKOS_DEFINITION}
            header="Description"
            sortable
          />
        </Table>
      </>
    )
}

---
title: New application My-competences
tags: [my-competences]
---

# New application My-competences

To make it easier to fill competencies and simplify the user experience, we have added a new application to the demonstrator called **My-competence**.

In this application, the user logs in, and then **only sees her personal data**. It can then fill in his CV in a streamlined interface.

It can be adapted to the instance, as for example in the demonstrator, we have called it **My-ServerA** (for the instance A).

You can find the user documentation of this application in [Project doc. > Doc. Demonstrator > My-competences](/docs/category/doc-demonstrator-category-label-2).

Technically, it is a second interface using the [SemApps] toolkit (/docs/technical-documentation/presentation-semapps). It points to the same data as the back-office application.
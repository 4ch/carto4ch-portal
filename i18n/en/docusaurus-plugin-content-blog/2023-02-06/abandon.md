---
title: Abandonment of reified relations and manual repositories
tags: [demonstrator, reified relations, semapps]
---

# Abandonment of reified relations and manual repositories

## Abandonment of reified relations
For technical reasons and time constraints, we have decided to abandon the reified relations mechanism to provide a first functional version containing the management of : 
* trainings
* jobs
* contributions
* competences (with 4 fixed qualities)

Indeed, after a few months of complex implementation and difficulty in explaining the interface during the [skills workshops](/docs/workshops), we moved towards a simpler solution, consisting in freezing 4 qualities, each linked to a repository: 
* Disciplines
* Sectors
* Studied Objects
* Tools

**Advantage:** The user will thus be better guided in entering competences

**Disadvantage:** If we want to add a new quality in the future, we will have to go through the code.

## Postponement of automatic repositories
We have also decided to move towards internal repositories, filled in by each person's entries in each server. We continue to use the ESCO and Wikidata repositories for input help.

**Advantage:** Time saving and simplicity of architecture. We want to start small, but we want it to work, because it is still a demonstrator. This will allow us to have a beginning of a repository for the day when we need to create an automatic one.

**Disadvantage:** This means that at the beginning, the choice lists will not be filled up much, but that as we go along, these lists will be filled up with the qualities created by everyone in each instance.
---
title: Interoperability is coming to SemApps!
tags: [interoperability]
---

# Setting up interoperability in SemApps
The summer months were the opportunity for the SemApps team to finish the development of interoperability in SemApps.

This means that from now :
* two SemApps servers can share their data with each other (interconnexion), i.e. an actor from server A can point to the competence of server B.
* a SemApps server can mirror the data of another server

We have updated the A and B servers of the demonstrator to test these new features.

The notion of mirrors can be used later by the "central" server of the competences cartography.

You will find [diagrams](/docs/schemas/distributed) explaining these two new concepts.

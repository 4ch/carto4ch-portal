---
title: Relation(s) betwin competences and jobs/commitment/training
tags: [interface, input, ontologie]
---

# MY - Relation(s) betwin competences and jobs/commitment/training

## Need to see only my personal data
```
┌────────────────────────┐
│    MY-competences      |
│------------------------│
│ ┌────────────────────┐ │
│ │ My jobs            │ │
│ │ My commitments     │ │
│ │ My trainings       │ │
│ └────────────────────┘ │
└────────────────────────┘
```
On the **My-competences** interface, it is possible to link skills to positions, contributions and training.

The aim of My-skills is for users to see only their personal details and not those filled in by others, as in the back office.

We have therefore **filtered the choice lists** so that each user can only create links to the posts, contributions or training courses that they have created.

For more information, see the [My-competences user manual](/docs/project-documentation/doc-demonstrator/my-competences).

## Use of a single predicate
```
┌───────────────────────┐       ┌────────────┐
│ Competences           │       │ Discipline │
│-----------------------│       │------------│
│ id                    │   ┌──>│ uri        │
│ title                 │   │   │ title      │
│ description           │   │   │ description│
│ heco:hasDiscipline    │───┘   └────────────┘
│ xxxxxx                │       ┌────────────┐
│ xxxxxx                │       │ Job      │
│ xxxxxx                │       │------------│
│ xxxxxx                │   ┌──>│ uri        │
│ xxxxxx                │   │   │ title      │
│ xxxxxx                │   │   │ description│
│ heco:isCompAcquiredIn │───┘?  └────────────┘
└───────────────────────┘   │   ┌──────────────┐
                            │   │ Commitment   │
                            │   │--------------│
                            └──>│ uri          │
                                │ titre        │
                                │ description  │
                                └──────────────┘       
```

At the HeCo ontology level, we wanted to use just one predicate (relationship) between a skill and a position / contribution / training. This way, the day we add a new object, we won't have to change the ontology.

To enable this, we had to add a customisation on the server side, so that it would make the client believe that it was using a different predicate for each object, while keeping a single predicate in the base.

For more information about HeCo, follow [Ontologie HeCo](/docs/technical-documentation/ontology)
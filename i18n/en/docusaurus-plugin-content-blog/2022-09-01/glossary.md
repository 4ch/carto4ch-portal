---
title: Publication of the glossary and SOLID tutorial
tags: [glossary]
---

# Publication of the glossary and SOLID tutorial
The summer months were the opportunity to finish the glossary, as well as the SOLID tutorial.

These two deliverables are closely linked, because one is based on the code of the other.
Indeed, this lexicon is also there to show you what we can do with SOLID.

We could imagine later on a shared lexicon based on SOLID servers distributed in the field of French (or even European) cultural heritage... But this is not the purpose of this project, and we will be satisfied with a small lexicon and a modest tutorial... :)

To consult it, go to : [Project documentation > Glossary](/docs/project-documentation/glossary)

This glossary can be completed throughout the project.

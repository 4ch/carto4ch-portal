---
title: React-Admin 4 Migration
tags: [technique, refactoring]
---

# React-Admin 4 Migration
```
┌────────────────────────┐
│ Demonstrator Carto4CH  |
│------------------------│
│ ┌────────────────────┐ │
│ │      SemApps       │ │
│ │--------------------│ │
│ │ ┌────────────────┐ │ │
│ │ │  React-Admin   │ │ │
│ │ └────────────────┘ │ │
│ └────────────────────┘ │
└────────────────────────┘
```
## What is React-Admin?
The Carto4CH demonstrator user interface is based on the **SemApps** toolbox, which itself depends on a technical foundation (framework) called [**React-Admin**](https://marmelab.com/react-admin/). This technical foundation is maintained by the (French) company [**Marmelabs**](https://marmelab.com/fr/). It uses the JavaScript library [**React**](https://fr.legacy.reactjs.org/).

In short, the SemApps components used for the user interface are in fact an assembly of components from the React-Admin framework. These components have been adapted (writing 'overlays') so that they can interface with one (or more) SemApps servers (which are coded in Node JS, another Javascript library).

## Why migrate?
This new feature concerns an important [**code refactoring**](https://fr.wikipedia.org/wiki/R%C3%A9usinage_de_code), which will enable our demonstrator : 
* be up to date with the basic components on which it depends, 
* be more stable, benefiting from bug fixes
* access to new current or future functionalities.

So we're going to take advantage of this migration to give you some technical details about how it works, in as simple a way as possible.

## Explanation
Since the start of the Carto4CH project, we've been using version 3 of React-Admin. However, a few months ago, Marmelab released a major version, version 4, and we therefore had to 'migrate' the code for the SemApps components to this version, as well as all the interfaces that use these components. 
For the Virtual Assembly, this operation was far from neutral, and it took our teams several weeks to update the SemApps components so that they worked with the new version of React-Admin. 
This was completed at the beginning of July, allowing us to update the Carto4CH demonstrator interface code ourselves.

These adaptations were first made to the **back-office**, then to **My-skills**.

Once we had achieved iso-functionality, we realised that this new version 4.0 made it possible to simplify certain interfaces, or to simplify code writing, because in version 3 we had added 'band-aids' because of bugs that were fixed in version 4.
So we've gone through the demonstrator code a second time, to make it easier to maintain in the future.

For more information about the inner workings of **SemApps**, see [Technical documentation > SemApps technical doc](/docs/technical-documentation/presentation-semapps)
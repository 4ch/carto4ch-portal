---
title: General mapping = Mirror server visualization
tags: [visualisation, flodio, mirror]
---

# General mapping = Mirror server visualization
```
┌────────────────────────────────┐
│        General mapping         |
│   Mirror server visualization  |
│--------------------------------│
│     ┌────────────────────┐     │
│     │    O---O---O O-O   │     │
│     │   /   / \  |/   \  │     │
│     │  O---O---O-O O   O │     │
│     │   \ /     \|/ \ /  │     │
│     │   O-O---O-O-O O    │     │
│     └────────────────────┘     │
│                |               │
│                v               │
│ ┌───┐    ┌──────────┐    ┌───┐ │
│ | A | -> |  Mirror  | <- | B | │
│ └───┘    └──────────┘    └───┘ │
└────────────────────────────────┘
```
Following the visualisation of instances A and B, we added **the mirror server** to the demonstrator. This server regularly **synchronises** the data from servers A and B in a sort of "cache", making it possible to offer a network visualisation containing the data from server A and server B.

The mirror server is simply another SemApps server that we have configured to synchronise with the other two servers. To do this, it uses the **Activity Pub** protocol.

To access this visualisation, go to the [**Demonstrator Page**](/docs/applications/demonstrator).

To understand the technical operation of this user interface, go to the [**Visualisation**](/docs/technical-documentation/visualisation).
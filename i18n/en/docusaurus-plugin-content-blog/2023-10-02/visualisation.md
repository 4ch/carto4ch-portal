---
title: Visualising network mapping for one instance
tags: [visualisation, flodio]
---

# Visualising network mapping for one instance
```
┌────────────────────────┐
│       Visualisation    |
│        in network      |
│------------------------│
│ ┌────────────────────┐ │
│ │    O---O---O O-O   │ │
│ │   /   / \  |/   \  │ │
│ │  O---O---O-O O   O │ │
│ │  |\ /     \|/ \ /  │ │
│ │  O-O-O---O-O-O O   │ │
│ │   \ /   /  |/ /    │ │
│ │    O --O---O-O     │ │
│ └────────────────────┘ │
└────────────────────────┘
```
To facilitate the visualisation of Carto4CH mapping data, we are proposing a **network representation**, as initially planned.

This shows the advantage of having interoperable data by having a third user interface accessing the same data.

Firstly, we will carry out a visualisation by Carto4CH instance.

This shows the players, projects, skills, positions, contributions, training and qualities, linked together by the relationships provided for by the **HeCoontology**.

To access this visualisation, go to the [**Demonstrator page**](/docs/applications/demonstrator).

To understand the technical operation of this new interface, go to the [**Network visualisation**](/docs/technical-documentation/visualisation).
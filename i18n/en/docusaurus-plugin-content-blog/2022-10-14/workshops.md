---
title: Competences workshops
tags: [workshop, competences]
---

# Skills workshops
In order to maintain the link between the core team and the partners, we have set up regular meetings called "skills workshops".

For more information on these workshops, see [this page](/docs/workshops)

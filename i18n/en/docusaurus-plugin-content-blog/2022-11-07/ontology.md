---
title: Publication of a first version of the HeCo ontology
tags: [ontology, competences]
---

# Publication of a first version of the HeCo ontology
After a first work of the Carto4CH core team, we have stabilized **a first version of the HeCo** ontology (version 0.5).

This version will allow us to have a first support to exchange with the partners of the project.

The objective is that this ontology is questioned, tested, until it stabilizes to be used in the cartography.

For more information on this ontology, see [this page](/docs/technical-documentation/ontology)

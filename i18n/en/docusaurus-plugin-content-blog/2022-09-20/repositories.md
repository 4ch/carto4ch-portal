---
title: Adding completion via repositories
tags: [repositories]
---

# Adding completion via repositories in SemApps
It is now possible to query repositories such as **[ESCO](https://esco.ec.europa.eu/en)** or **[Wikidata](https://www.wikidata.org/)** to help with competences entry.

We have implemented this in the demonstrator:
* ESCO is used to help with competences entry
* Wikidata helps to enter themes (linked to a person)

We are using **an "in-house" ESCO server**, maintained by [a SemApps team member](https://semapps.org/) of the [Virtual Assembly](https://www.virtual-assembly.org/), containing a copy (RDF) of the ESCO repository.
This homegrown server has the advantage of providing a SPARQL endpoint (although for the moment we are not using it), which is no longer active in the production server.
This will also allow us in the future to complete it with content more specific to the world of cultural heritage.

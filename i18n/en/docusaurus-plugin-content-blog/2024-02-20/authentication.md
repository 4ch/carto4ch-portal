---
title: Authentication / SSO
tags: [Authentication, sso, oidc]
---

# Authentication / SSO
```
┌────────────────────────────────────────────┐
│           SSO / OIDC / SOLID-OIDC          |
│--------------------------------------------│
│       Application            Server OIDC   |
│   ┌───────────────┐                        │
│   │  ┌─────────┐  │                        │
│   │  |  Login  │  │          ┌─────────┐   │
│   │  └─────────┘  │  <====>  |  OIDC   │   │
│   │  ┌─────────┐  │          └─────────┘   │
│   │  |  pass   │  │                        │
│   │  └─────────┘  │                        |
│   └───────────────┘                        │
└────────────────────────────────────────────┘
```

The [**integration into the 4CH cloud**](/docs/technical-documentation/integration) stage enabled us to try using a second OIDC/CAS server.

You can find details of how authentication works here: [**Technical Documentation > Authentication**](/docs/technical-documentation/authentication)

In the end, we identified areas for improvement in SemApps, giving users a choice of different authentication modes.

You can find these recommendations in the section [**Recommendations > Authentication**](/docs/recommendations/authentication)

and [**Standards and concepts > OIDC**](/docs/technical-documentation/standards/oidc)
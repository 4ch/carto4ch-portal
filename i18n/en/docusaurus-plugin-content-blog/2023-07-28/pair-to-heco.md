---
title: Replacement of the PAIR ontology by HeCO
tags: [ontology, pair, heco]
---

# Replacement of the PAIR ontology by HeCO
```
┌────────────────────────┐      ┌────────────────────────┐
│         PAIR           |      │       HECO v1.0        |
│------------------------│      │------------------------│
│ ┌────────────────────┐ │----->│ ┌────────────────────┐ │
│ │   pair:hasSkill    │ │      │ │ heco:hasCompetence │ │
│ └────────────────────┘ │      │ └────────────────────┘ │
└────────────────────────┘      └────────────────────────┘
```
## What is the PAIR ontology?
The primary objective of the Virtual Assembly is to map communities.
So, in addition to the SemApps toolbox, it created a PAIR ontology to map **P**projects, **A**ctors, **I**ideas and **R**resources.
Events and other concepts were added later...

## A gradual migration...
At the beginning, the first versions of the Carto4CH demonstrator mapped people (actors), organisations and skills, so PAIR had everything we needed for a proof of concept.

As the project progressed, we wanted to move away from PAIR, because its raison d'être was not the same as that of HeCo. So we added classes and relationships from HeCo in addition to PAIR. 

For the first year, the two ontologies coexisted very well, and that's the strength of the semantic web, being able to use and mix several ontologies! But after a while, and for the maintenance of the application, it's wise to make long-term choices.

## Let's liberate ontologies!
It is important to see the Carto4CH demonstrator as **"a technical solution that can interface with the HeCo ontology "**. 
In other words, **HeCo must not be specific to any implementation**. 
It must be able to live its life as an ontology without worrying about how developers are going to use it.

At the end of June 2023, [the HeCo ontology](/docs/technical-documentation/ontology) was upgraded to V1.0, which for us meant that it could enter a 'production' and 'maintenance' cycle, and we were able to start thinking about moving away from the PAIR ontology.

In July-August 2023, work was carried out to align the HeCo ontology with all the classes/relations present in the code and in the demonstrator database, so as to become as little dependent on PAIR as possible.

We can now say that the demonstrator uses the **HeCo** ontology 95% of the time. 

In fact, some of the specific features of SemApps meant that we had to keep references to PAIR in certain places. But if we've done this, it's more for comfort or simplicity, it doesn't mean that there are things missing from the HeCo ontology.

## New HeCo ontology version 1.0 !
We have updated the HeCo ontology to version 1.0. You can consult this new version online or via a schema, an OWL file and a presentation document: [HeCo Ontology](/docs/technical-documentation/ontology).
---
title: Internationalisation of user interfaces
tags: [internationalisation, SemApps, React-admin]
---

# Internationalisation of user interfaces
```
┌────────────────────────────────┐
│      Internationalisation      |
│--------------------------------│
│          ┌───────────────────┐ │
│          |  ENGLISH      v   | │
│          └───────────────────┘ │
│           |      ENGLISH    │  |
│           |      FRENCH     │  |
│           └─────────────────┘  │
└────────────────────────────────┘

```
In order to prepare a presentation of the demonstrator to the European team of the [4CH project](https://www.4ch-project.eu/), we have, as initially planned, translated all the interfaces. 

The back office and My-competences are now in English by default, with the option of switching to French in the top right-hand menu. The default language can be set when the server is deployed.

To understand the technical operation of this feature, visit the [**Internationalisation page**](/docs/technical-documentation/presentation-semapps#internationalisation).
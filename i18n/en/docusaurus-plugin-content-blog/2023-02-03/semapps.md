---
title: Adding a page for SemApps
tags: [semapps]
---

# Adding a page for SemApps

Since **SemApps** is fully used in this project, it seemed important to give it a full place in this portal.

At the same level as SOLID, we added a [reference page](/docs/technical-documentation/presentation-semapps), with a link to https://semapps.org, which has recently been updated with a new stable release.

We have added a [schema](/docs/schemas/architecture), explaining the notion of "semantic toolbox" which summarizes what SemApps is.
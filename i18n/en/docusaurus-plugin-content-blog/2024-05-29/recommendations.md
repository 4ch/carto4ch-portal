---
title: Recommendations
tags: [recommendations, authentication, vocabularies, rgpd, ontology]
---

# Recommendations
```
┌────────────────────────────────────────────┐
│                Recommendations             |
│--------------------------------------------│
│  ┌──────────────────┐ ┌──────────────────┐ │
│  │  Authentication  │ │       RGPD       │ │
│  └──────────────────┘ └──────────────────┘ │
│  ┌──────────────────┐ ┌──────────────────┐ │
│  │  Ontology driven │ │   Vocabularies   │ │
│  └──────────────────┘ └──────────────────┘ │
└────────────────────────────────────────────┘
```

For all the important functionalities or improvements that we were unable to implement in Carto4CH for the moment (due to lack of time or energy, or because they were too complex), we have documented recommendations.

Voir [**Recommendations**](/docs/category/recommendations-category--label)

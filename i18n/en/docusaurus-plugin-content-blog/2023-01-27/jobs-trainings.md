---
title: Adding jobs, trainings and contributions
tags: [jobs, trainings, contributions]
---

# Adding jobs, trainings and contributions
At the beginning of 2023, we have added new concepts to the [demonstrator](/docs/applications/demonstrator), related to the [ontology version 0.7](/docs/schemas/ontology/).

Now, you will be able to "fill your CV" in your profile.
For this, several new concepts are available to you: 
* Create **professions** (jobs)
* Create **jobs** (professional experience)
* Create **training** (experiences related to your studies)
* Create **contributions** (volunteers)

Once these objects are created in the database, you will be able to add them to your profile and view your personal CV. The user profile is public by default, so everyone can browse the "skills" of other users.

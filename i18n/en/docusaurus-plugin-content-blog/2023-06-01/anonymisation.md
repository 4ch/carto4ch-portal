---
title: Renaming the demonstrator instances
tags: [documentation, demonstrator]
---

# Renaming the demonstrator instances

Since the beginning of the project, we've been using organization names to call the two demonstrator servers.
For the sake of neutrality, we now call them **Server A** and **Server B**.

We have therefore renamed all elements referring to organizations : 
* in this portal
* in access URLs to the demonstrator
* in schemas
* in server files

:::caution Attention
The URLs for accessing the demonstrator interfaces have changed as a result of this operation.

Don't forget to update your bookmarks!

**Access to the Server A instance**
* My-ServerA : https://my-a.carto4ch.huma-num.fr
* Back-office Server A : https://bo-a.carto4ch.huma-num.fr
* Data : https://data-a.carto4ch.huma-num.fr

**Access to the Server B instance**
* My-Server B : https://my-b.carto4ch.huma-num.fr
* Back-office Server B : https://bo-b.carto4ch.huma-num.fr
* Data : https://data-b.carto4ch.huma-num.fr
:::

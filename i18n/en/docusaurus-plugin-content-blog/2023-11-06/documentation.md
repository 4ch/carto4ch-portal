---
title: Technical documentation
tags: [documentation, SemApps, SOLID, React-admin]
---

# Technical documentation
```
┌────────────────────────────────┐
│     Technical documentation    |
│--------------------------------│
│ SOLID                          │
│ SemApps                        │
│ Demonstrator                   │
└────────────────────────────────┘

```
Now that the demonstrator is up and running in version 1.0, we've been able to complete the technical documentation, to help developers understand how the demonstrator works, how to deploy their own server, etc.

We haven't added the links to the many development PADs (which are more detailed) so as not to pollute the documentation, but don't hesitate to ask us for access.

You can find all these technical details in the [**Technical documentation**](/docs/category/technical-documentation-category-label).
---
title: Adding reified relations in SemApps
tags: [concept, semantic web]
---

# Adding reified relations in SemApps
In order to capture skills in a richer way, we needed to be able to run what is called in semantic web **reified relations** (or ternary relations).

This is the possibility to bring additional information on top of a "simple" relationship.

For example, let's start with a relation like **"Bob knows Alice"**

|Subject|Predicate|Object|
|-----|--------|-----|
|Bob |foaf:knows |Alice|

If we want to qualify the relationship between these two people more precisely, we call this a **reification** (and there are several ways to do this).

This is for example what we did ( [see Schema > Architecture](/docs/schemas/architecture/) ) to manage the **roles** between a person and an organization.

---
title: Availability of the mockups
tags: [mockups]
---

# Mockups available
In order to offer another media than applications to Carto4ch partners, we have added to this portal the [mock-ups](/docs/project-documentation/mockup) that we used to develop our applications.

These mock-ups can give another view of what we want to propose as interfaces to users, to enter their skills.

They can be updated regularly according to the evolution of the project (as well as the schemas).

They can also be reused or completed by the partners.
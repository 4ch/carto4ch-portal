---
title: Publication of the ontology in HTML format
tags: [ontologie, widoco, webvowl, owl]
---

# Publication of the ontology in HTML format
```
┌────────────────────────┐
│  / /  HeCo V1.0        |
│-/ /--------------------│
│/ /                     │
│ / Version              │
│/  Authors              │
│   Abstract             │
│   Graph                │
│ ┌────────────────────┐ │
│ │  O---O-----O O     │ │
│ │   \ /      |/ \    │ │
│ │    O ------O   O   │ │
│ └────────────────────┘ │
│   Classes...           │
│   Properties...        │
└────────────────────────┘
```
To make the HeCo ontology easier to consult, we have added a new **more readable** publication format for non-technical profiles.

In addition to the ontology in OWL/TTL format, the summary, the schema, and the PDF document, we provide an **HTML interface**.

We used [**WIDOCO**](https://github.com/dgarijo/Widoco) to automatically generate this interface from our OWL file.

WIDOCO is increasingly becoming a standard in ontology delivery. This format is increasingly common on the web.

It describes all **classes and properties**, providing links for easier navigation within the document.

WIDOCO creates two HTML outputs, one in French and one in English.

A **networked graph** of the ontology is also available, enabling the ontology to be viewed using **WebVOWL** software.

To take advantage of this new interface, visit the [**HeCo Ontology**](/docs/technical-documentation/ontology).
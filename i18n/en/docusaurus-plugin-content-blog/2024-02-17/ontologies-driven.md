---
title: Ontology driven
tags: [Ontologie]
---

# Ontology driven
```
┌────────────────────────────────────────────┐
│ What if ontology drove the application?    |
│--------------------------------------------│
│       Ontology               Application   |
│   ┌───────────────┐                        │
│   │    O---O---O  │                        │
│   │   /   / \  |  │          ┌─────────┐   │ 
│   │  O---O---O-O  │  <====>  |   App   │   │
│   │   \ /     \|  │          └─────────┘   │
│   │   O-O---O--0  │                        |
│   └───────────────┘                        │
└────────────────────────────────────────────┘
```

An old dream of (IT) ontologists is to be able to manage / parameterise / control the operation of an application directly via one or more ontologies.

This is a long-term objective for SemApps, but it is still a long way off.

In this portal, we will therefore only give recommendations on this subject. 

We'll talk about what's missing in SemApps, the improvements that have been made and what we hope to see in the future.

See [**Recommendations > Ontology-driven**](/docs/recommendations/ontologies-driven)
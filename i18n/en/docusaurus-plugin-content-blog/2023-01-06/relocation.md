---
title: Relocation of the server A
tags: [infrastructure]
---

# Relocation of the server A
In 2022, we had used two différent hosts (OVH & Huma-Num) for our two servers A and B.
The goal was to show interoperability to the end.

For reasons of simplification and maintenance of the [demonstrator](/docs/applications/demonstrator), we migrated the instance A to the same Huma-Num hosting as the instance B.

The particularity of this new **multi-instance** infrastructure is that both applications share the same triplestore, but having within it two independent datasets. The data is therefore not mixed. This is technically the same as before, we just gain in maintenance, cost, energy, and we are sure to have the same machine power for both instances, which was not the case before because the two hostings were not identically sized.

To understand the difference in the server infrastructure, you can consult the [infrastructure diagram](/docs/schemas/infrastructure)
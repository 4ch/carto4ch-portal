---
title: Deploying a new server
tags: [documentation, demonstrator]
---

# Deploying a new server

The production phase has begun.

We've provided partners with a technical "package" enabling them to install and configure their own production server.

This package uses the same code as the demonstrator, but for a single server.

For more information, see [Technical documentation > Deploying a server](/docs/technical-documentation/server-deploy).
---
title: Internationalization of the portal
tags: [internationalization]
---

# Internationalization of the portal
We have started to translate this portal, which will also serve as a communication towards the [4CH project](https://www.4ch-project.eu/).

Gradually, all published pages and presentations will be translated into English.

By default, the language will be in French as long as we address the French community for the deployment of the cartography.

You can access the English version by changing the language using the menu at the top right.

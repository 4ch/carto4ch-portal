---
title: Maintenance of a Carto4CH serveur
tags: [Compact, Backup]
---

# Maintenance of a Carto4CH serveur
```
┌───────────────────────────────────┐
│           Maintenance             |
│-----------------------------------│
│   ___                     ___     │
│  /   \      ┌───\        /   \    │
│ |\___/|     |    \      |\___/|   │
│ |     | <=  |     |  => |     |   |
│ |     |     |     |     |     │   |
│  \___/      └─────┘      \___/    |
│                                   |
└───────────────────────────────────┘
```

We have added a new "Maintenance" section, to explain how to maintain a Carto4CH server in production.

This includes : 
* access to the triplestore web interface
* compacting the database
* backing up triplestore data

See [**Maintenance**](/docs/technical-documentation/maintenance)
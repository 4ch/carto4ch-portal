---
title: Simplification of the creation form
tags: [form, simplification]
---

# Simplification of the creation form
```
┌──────────────────────────┐      ┌──────────────────────────┐
│ Creation                 |      │ Creation                 | 
│--------------------------│      │--------------------------│
│             ┌─────────┐  │      │             ┌─────────┐  │
│ Title       │         │  │      │ Title       │         │  │
│             └─────────┘  |      │             └─────────┘  |
│  ┌───────────────────┐   │ ---> │             ┌─────────┐  │
│  | Validation ../..  │   │      │ Description |         │  │
│  └───────────────────┘   │      │             └─────────┘  │
|                          │      |  ┌────────────────┐      │
| Warning : 2 steps !      |      │  |     Save       │      │
└──────────────────────────┘      │  └────────────────┘      │
                                  └──────────────────────────┘
```
## Before
Until now, **creating** a resource in the demonstrator forms (in the BO or MY interface) was a two-stage process. Firstly, the title and an initial validation (which created the resource in the database), then a complementary form, where you would find the rest of the fields that make up the resource.

**Problem:** Most of the time, the user didn't know what to enter in the 'title' field, as they didn't yet see the overall content of the form.

## From now on
So we simplified it by merging everything into the same form.

Most of the forms in the demonstrator have been revised to simplify their input.

# Removal of the title for competences
```
┌──────────────────────────┐      ┌──────────────────────────┐
│ Competence creation      |      │ Competence creation      | 
│--------------------------│      │--------------------------│
│             ┌─────────┐  │      │             ┌─────────┐  │
│ Title       │         │  │      │ Description │         │  │
│             └─────────┘  |      │             └─────────┘  |
│             ┌─────────┐  │ ---> │  ┌────────────────┐      │
│ Description |         │  │      │  |      Save      │      │
│             └─────────┘  │      │  └────────────────┘      │
|  ┌────────────────┐      │      └──────────────────────────┘
|  |     Save       │      |                                                
│  └────────────────┘      │                                                
└──────────────────────────┘                                                
```
In the same vein, users didn't know what to enter in the 'title' field for skills. 

We have therefore removed it, and generated an automatic title based on the user ID and an incremental number.

For more information on the demonstrator's interfaces, please consult the user manuals: 
* [My-competences](/docs/project-documentation/doc-demonstrator/my-competences)
* [Back-office](/docs/project-documentation/doc-demonstrator/back-office)
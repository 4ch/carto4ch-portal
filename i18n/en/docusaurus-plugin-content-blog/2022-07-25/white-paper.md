---
title: Publication of the white paper
tags: [white paper]
---

# Publication of the first version of the white paper
Following the twenty interviews conducted since mid-April 2022 with our partners, we are publishing a first version of our white paper, containing a summary of our discussions.

To consult it, or download it in PDF, go to: [Project documentation > White paper (in french for the moment...)](/docs/project-documentation/white-paper/presentation)

In the next version, we will add a synthesis of our monitoring of the existing competences in the field of cultural heritage.

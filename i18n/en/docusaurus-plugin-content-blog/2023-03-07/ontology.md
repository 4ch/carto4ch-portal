---
title: New version of HeCo v0.8
tags: [ontology, competence]
---

# New version of HeCo v0.8

Publication of a new version of the HeCo ontology.

* [Page of the ontology](/docs/technical-documentation/ontology)
* [Corresponding schema](/docs/schemas/ontology)
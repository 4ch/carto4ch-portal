---
title: Demonstrator
sidebar_position: 2
pagination_next: null
---

# Demonstrator

## Introduction
|Schema|Présentation|
|----|----|
|![](/img/schema-demonstrator-A-B.png)|The **demonstrator** is a functional demonstration cartography.<br/>Its objective is to give an example of what is expected by the Carto4CH project.<br/><br/>It respects the proposed technical base (SOLID, ontology...), and contains:<br/><ul><li>Two demo data servers (A and B)</li><li>Web applications allowing to cartography the data of these two servers</li><li>Demo data</li></ul>

## Access

:::info links
**The source codes** of the projects are available:
* on the huma-Num Gitlab (docker + Archipelago): https://gitlab.huma-num.fr/4ch/

**Instance A**
* My-competence of instance A : https://my-a.carto4ch.huma-num.fr
* Back-office of instance A : https://bo-a.carto4ch.huma-num.fr
* Network visualisation of instance A : https://carto4ch-a.flod.io
* Data of instance A : https://data-a.carto4ch.huma-num.fr

**Instance B**
* My-competence of instance B : https://my-b.carto4ch.huma-num.fr
* Back-office of instance B : https://bo-b.carto4ch.huma-num.fr
* Network visualisation of instance B : https://carto4ch-b.flod.io
* Data of instance B : https://data-b.carto4ch.huma-num.fr

**Instance C**
* Network visualisation of Server C : https://carto4ch-c.flod.io
* Data of instance C : https://data-c.carto4ch.huma-num.fr

**Source code** :
* Huma-Num Gitlab (docker + Archipelago + My-competence) : https://gitlab.huma-num.fr/4ch/
:::

## Status

* ontology : HeCo v1.0
* Back-office A : OK
* Back-office B : OK
* My-A : OK
* My-B : OK
* Visualisation-A : OK
* visualisation-B : OK
* Generale cartography  : OK

:::caution
The 2 data servers use the [Archipelago](https://github.com/assemblee-virtuelle/archipelago) project, based on [SemApps](/docs/technical-documentation/presentation-semapps).
:::

Do not hesitate to contact us if you have specific expectations.

## Documentation of the demonstrator

The documentation for this demonstrator can be found : 
* [here, for the user documentation](/docs/category/doc-demonstrator-category-label-2)
* [here, for the technical documentation (code)](/docs/technical-documentation/demonstrator-manual)

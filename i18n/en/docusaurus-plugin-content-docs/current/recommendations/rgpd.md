---
title: RGPD
sidebar_position: 3
---

# RGPD & ActivityPOD

```
┌────────────────────────────────────────────┐
│        RGPD by SOLID (ActivityPOD)         |
│--------------------------------------------│
│       Application                Data      |
│--------------------------------------------│
│   ┌─────────────────┐            DB        |
│   │                 │       ┌───────────┐  |
│   │                 │       |   App     │  │
│   │                 │  <=>  |   Data    |  │
│   │      User       │       └───────────┘  │
│   │    Interface    │                      │
│   │                 │            POD       │
│   │                 │       ┌───────────┐  │
│   │                 │       |  Personal |  │
│   │                 │  <=>  |  Data     │  |
│   └─────────────────┘       └───────────┘  │
└────────────────────────────────────────────┘
```

## Introduction

On this page, we will give our recommendations concerning the RGPD, as part of the Carto4CH project.
We could have applied the same care as in a normal website, following the [RGPD recommendations](https://www.cnil.fr/fr/rgpd-de-quoi-parle-t-on), but we want to take advantage of this project (which is experimental for the moment) to propose an architecture that is RGPD in essence.

## General
For the last ten years or so, the inventor of the web (Tim Berners Lee) has been proposing a new architecture [SOLID](/docs/technical-documentation/standards/presentation-solid) so that every human being is responsible for their personal data.

Europe is asking us to comply with the RGPD, which basically amounts to the same thing.

The [Virtual Assembly](https://virtual-assembly.org/) is therefore proposing to create new applications that separate application data from personal data.

This will be called **POD** (Personal Online Data).

Every human being will be able to have a POD, which they will decide to store with the POD provider of their choice (kitten, town hall, department, region, government, whatever).

The POD will also hold their **authentication keys**, so they won't have to recreate a login/mdp for each application.

## ActivityPOD = SOLID x Activity POD
To meet this need, the [Virtual Assembly](https://virtual-assembly.org/) proposes a standards-based (when SOLID will be a standard) technology called ActivityPOD.

**ActivityPOD** is a new technology, based on the [SemApps](https://semapps.org) toolkit, which uses two main projects:
* [SOLID](https://fr.wikipedia.org/wiki/Solid_(web_project_d%C3%A9centralis%C3%A9)), 
* [ActivityPUB](https://fr.wikipedia.org/wiki/ActivityPub).

For more information, visit https://activitypods.org/.

## Conclusion
Thanks to this technical solution, Carto4CH users will be able to save their skills and other personal data in their POD, and will be able to choose to open them to applications that wish to display them.

Users will therefore remain responsible for their personal data.

Of course, the way things work and the contracts between individuals and organisations will also have to be respected. So there will be both a technical and organisational challenge.
---
title: Authentification
sidebar_position: 2
---

# Recommendations for authentification

```
┌────────────────────────────────────────────┐
│  Choice of authentification / SOLID-OIDC   |
│--------------------------------------------│
│       Application            choix         |
│   ┌───────────────┐                        │
│   │  ┌─────────┐  │          ┌─────────┐   │
│   │  |  Login  │  │          |  SOLID- │   │
│   │  └─────────┘  │  <====>  |  OIDC   │   │
│   │  ┌─────────┐  │          └─────────┘   │
│   │  |  pass   │  │          ┌─────────┐   │
│   │  └─────────┘  │  <====>  |  CAS    │   |
│   └───────────────┘          └─────────┘   │
└────────────────────────────────────────────┘
```

## Introduction

On this page, we will give our recommendations concerning authentication in SemApps. These are suggestions for future improvements to make it easier to use in different contexts.

## Offer several types of authentication
The first point for improvement is that we can't, at the moment, offer the user several authentication choices (as we can see on some sites that offer to connect with Google, Facebook, mail, etc.).

The solution, for the demonstrator (in the meantime...), will be to use : 
* For server A: the common SSO
* For server B: INFN SSO

## SOLID-OIDC
The SOLID community has developed specifications for an authentication standard called **SOLID-OIDC**: https://solidproject.org/TR/oidc.

The SemApps team started working on this at the end of 2023, integrating part of the specifications. We are waiting for additional funding to complete this compatibility.
---
title: Vocabularies
sidebar_position: 4
---

# Vocabularies

```
┌────────────────────────────────────────────┐
│      Vocabularies / Reference systems      |
│--------------------------------------------│
│       Application           Référentiels   |
│--------------------------------------------│
│   ┌─────────────────┐            R1        |
│   │      User       │       ┌───────────┐  |
│   │    Interface    │       |   voc1    │  │
│   │                 │  <=>  |   voc2    |  │
│   │   Choice list   │       └───────────┘  │
│   │      - voc1     │                      │
│   │      - voc2     │            R2        │
│   │      - voc3     │       ┌───────────┐  │
│   │      - voc4     │       |   voc3    |  │
│   │                 │  <=>  |   voc4    │  |
│   └─────────────────┘       └───────────┘  │
└────────────────────────────────────────────┘
```

## Introduction

During the Carto4CH project, we gave a lot of thought to how to maintain vocabularies, or refer to existing european vocabularies. We would have liked to set them up, but the task was too complex for the limited time/energy we had.

We therefore opted for internal lists for each Carto4CH server.
* Users can populate these lists, helped by suggestions from [ESCO](https://esco.ec.europa.eu/fr) or [Wikidata](https://m.wikidata.org/wiki/Wikidata:Main_Page?uselang=fr).
* Using interoperability between servers, it is possible to see the lists of server A on server B.

This is an acceptable solution for a demonstrator, but not for production use.

In this page, we propose a future architecture to implement **external vocabularies**.

## Reminder
To understand what we're talking about, you can reread [the user documentation prerequisites on the subject of vocabularies](/docs/project-documentation/doc-demonstrator/requirements/referentiels).

## From internal vocabularies to external vocabularies

### Internal vocabularies

For the moment, each server has its own lists.
```
┌──────────────────────────────────────────────────┐
│             Internal vocabularies                |
│--------------------------------------------------│
│        Server A                  Server B        |
│--------------------------------------------------│
│   ┌─────────────────┐       ┌─────────────────┐  |
│   │      User       │       │      User       │  |
│   │    Interface    │       │    Interface    │  |
│   │                 │  <=>  │                 │  │
│   │   Choice list   │       │   Choice list   │  │
│   │      - voc1     │       │      - voc3     │  │
│   │      - voc2     │       │      - voc4     │  │
│   └─────────────────┘       └─────────────────┘  │
│             ^                      ^             │
│             └──  Interoperability ─┘             │
└──────────────────────────────────────────────────┘
```

### External vocabularies

We propose to use external repositories, which will be managed as commons by communities. They can be specialised in a particular field and used by any Carto4CH server.

```
┌──────────────────────────────────────────────────┐
│             External vocabularies                |
│--------------------------------------------------│
│        Server A                  Server B        |
│--------------------------------------------------│
│   ┌─────────────────┐       ┌─────────────────┐  |
│   │      User       │       │      User       │  |
│   │    Interface    │       │    Interface    │  |
│   │                 │       │                 │  │
│   │   Choice list   │       │   Choice list   │  │
│   │      - voc1     │       │      - voc1     │  │
│   │      - voc2     │       │      - voc2     │  │
│   │      - voc3     │       │      - voc3     │  │
│   │      - voc4     │       │      - voc4     │  │
│   └─────────────────┘       └─────────────────┘  │
│             ^          \ /          ^            │
│             |           X           |            │
│             |          / \          |            │
│   ┌─────────────────┐       ┌─────────────────┐  |
│   │      Vocab 1    │       │      Vocab 2    │  |
│   │      - voc1     │       │      - voc3     │  |
│   │      - voc2     │       │      - voc4     │  │
│   └─────────────────┘       └─────────────────┘  │
└──────────────────────────────────────────────────┘
```

## Technology / standards
These vocabularies can also be **thesaurus implemented by SOLID** servers, using the [SKOS ontology](https://fr.wikipedia.org/wiki/Simple_Knowledge_Organization_System)  for example, like the one we used in the [SOLID Tutorial](/docs/technical-documentation/tutorials/solid) and the [Carto4CH Lexicon](/docs/project-documentation/glossary).

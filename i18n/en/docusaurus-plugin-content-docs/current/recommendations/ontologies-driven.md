---
title: Ontology driven
sidebar_position: 2
---

# Ontology driven

```
┌────────────────────────────────────────────┐
│   What if ontology drove the application?  |
│--------------------------------------------│
│       Ontology               Application   |
│   ┌───────────────┐                        │
│   │    O---O---O  │                        │
│   │   /   / \  |  │          ┌─────────┐   │ 
│   │  O---O---O-O  │   ====>  |   App   │   │
│   │   \ /     \|  │          └─────────┘   │
│   │   O-O---O--0  │                        |
│   └───────────────┘                        │
└────────────────────────────────────────────┘
```

## Introduction

An old dream of (IT) ontologists is to be able to manage / parameterise / control the operation of an application directly via one or more ontologies.

This page looks at the link between the application and the ontology.

## Example with Carto4CH
In the Carto4CH project, if, for example, we wanted to add a new quality (other than discipline, sector, etc.), or change the name of one of these qualities, all we would have to do is update the ontology class, and since the applications would be directly linked to the ontology, they would update themselves automatically.

## Complexity of implementation
This vision is very interesting, but very complex to put in place. 
In terms of the SemApps toolbox, we're just starting to make improvements in this direction.

## Inverse relations
An initial ‘ontology-driven’ approach was used for **inverse relationships**.
In fact, to search for inverse relationships, **SemApps consults the Heco** ontology (see explanations in [inverse relationships](/docs/technical-documentation/standards/inverse-relationship)). You therefore need to be very careful when modifying the online ontology, because if you make a mistake, it can have serious consequences for the operation of the inverse relationships, and therefore for the overall operation of the application.

## Duplication of logic
But for everything else, the logic of the ontology is ‘duplicated’ in the code, which ‘freezes’ things a little. When we need to modify the ontology, we also need to modify the code. That's how it is at the moment, but we regularly think about how we can do better.

## Improved management of JSON-LD ontologies and context
At the end of December 2023, the management of ontologies and JSON-LD context was improved in SemApps.

It was implemented in Carto4CH in February 2024.

It allows each ontology to be declared with its own JSON-LD context, which is much clearer. SemApps is then responsible for combining all the contexts into a single one, which is then available at : 
https://data-a.carto4ch.huma-num.fr/.well-known/context.jsonld


---
title: Infrastructure
sidebar_position: 3
---

# Infrastructure

## Introduction

The schema below will explain how the servers deployed for the project are hosted.

## Access

| Working Document| Image |
| -------- | -------- |
| Here is a schema detailing the infrastructure to deploy a new partner server. [schema can be improved collectively under Figma](https://www.figma.com/file/8wGJep8oXW86iPwoHQvupZ/Architecture-de-d%C3%A9ploiement-d'un-serveur-Carto4CH?type=design&t=kkEdD5LyVr89nCU4-0)|![](/img/infra-deploy.png)|
| Here is a schema detailing the infrastructure following the move of the instance A to the Huma-Num server. [schema can be improved collectively under Figma](https://www.figma.com/file/gTg62KjoVldTm1gIzqiWLa/Architecture-du-Serveur-HN-carto4ch)|![](/img/Infra-server-carto4ch.png)|
| Here is a schema detailing the infrastructure present in 2022 on 2 servers. [schema can be improved collectively under Figma](https://www.figma.com/file/CnOjxQhAKCBN3fOFPXm1Lj/D%C3%A9ploiement-des-serveurs-Carto4CH?node-id=0%3A1)|![](/img/infra-2-servers.png)|

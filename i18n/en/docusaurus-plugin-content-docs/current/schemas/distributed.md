---
title: Distributed mode
sidebar_position: 4
pagination_prev: null
---

# Distributed mode

## Introduction

The following diagrams explain what is meant by "distributed mode".
* Data are stored on decentralized servers
* The general mapping (or "Web application") will search these servers to display them

## Access to the schemas

| Working document| Image |
| -------- | -------- |
| Here is a very simple schema to avoid getting lost in the details. [Collectively improvable schema in Figma](https://www.figma.com/file/PbX8O3WIMqahZgu6rV3UCZ/Distribu%C3%A9-couleurs-Carto4CH?node-id=102%3A119)|![](/img/schema-distribue-carte.png)|
| Here is a schema with more details. [schéma améliorable collectivement sous Figma](https://www.figma.com/file/aPrHSKEigcKfcrVX1nXycy/4CH---Carto-distribu%C3%A9e-simplifi%C3%A9e?type=design&node-id=3-2&t=6sjNN1vqycYJCozp-0). |![](/img/schema-distribue-simplifie.png)|
| To see a "global view" of the Carto4CH project architecture, see this [collectively improvable schema in Figma](https://www.figma.com/file/K8eaErNiTpfZOn0LvfzQbq/Untitled?node-id=3%3A2). |![](/img/schema-distribue.png)|
| To better understand the different modes of data hosting, the notions of "decentralization" or "distribution", see this [collectively improvable schema under Figma](https://www.figma.com/file/Z81KZxM6xYsVQLeZnZxVCB/Organisation-de-l'h%C3%A9hosting-and-data%C3%A9es-Semapps?node-id=0%3A1). |![](/img/Organisation-hebergement-donnees.png)|
| To understand the data interconnection mechanism in SemApps. See this [collectively improvable schema under Figma](https://www.figma.com/file/3f74se3w8LaDmnulEMNQIx/Interconnexion-des-donn%C3%A9es-SemApps). |![](/img/Interco-msh-masa.png)|
| To understand the mirroring mechanism in SemApps, allowing a federated search, see this [collectively improvable schema under Figma](https://www.figma.com/file/BO2p508bSmQU1nqQSXj2eC/Recherche-f%C3%A9d%C3%A9r%C3%A9e---Mirroring-SemApps). |![](/img/mirroring-msh-masa.png)|

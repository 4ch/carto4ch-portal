---
title: Architecture
sidebar_position: 1
---

# Architecture

## Introduction

The diagrams below will explain the architecture of the demonstrator.

## Access

| Working Document| Image |
| -------- | -------- |
| This diagram explain the technical architecture of the demonstrator [This schema can be improved collectively under Figma](https://www.figma.com/file/HSG85jS1yXtw3FjPJN37KX/Demonstrator---en---choix-technologiques?type=design&node-id=102-119&mode=design&t=6b7q2mhZ8RQChDNb-0)|![](/img/demonstrator-architecture-schema.png)|
| This diagram (addressed to all) explains the notion of the SemApps "toolbox" [This schema can be improved collectively under Figma](https://www.figma.com/file/KLqHKZYak1YKREBQm2N7oS/Comprendre-la-boite-%C3%A0-outils-SemApps-pour-4CH)|![](/img/toolbox-semapps.png)|

---
title: Code
sidebar_position: 5
---

# Code

## Introduction

The diagrams below is adressed to developers. It explain how code is organized to run the demonstrator.

## Access

| Working Document| Image |
| -------- | -------- |
| This diagram explains how inputs components works [Schemas can be improved collectively under Figma](https://www.figma.com/file/quEMQ6L26UgKl9g1jUIwWJ/Fonctionnement-des-inputs-(auto-complete)?type=design&node-id=102-119&mode=design&t=6b7q2mhZ8RQChDNb-0)|![](/img/inputs.png)|
| This diagram explains how the code files between SemApps and Archipelago are articulated [Schemas can be improved collectively under Figma](https://www.figma.com/file/Y6qT1hcf9CciSUl1voeTPX/archipelago-et-yarn-links)|![](/img/schema-archipelago.png)|
| Reified Relations for competences (forkaken) |![](/img/reified-relations-competences.png)|
| Reified Relations for memberships (forkaken) |![](/img/reified-relations-roles.png)|

---
title: Ontology
sidebar_position: 2
---

# Ontology

## Introduction

The schemas below will attempt to explain the [HeCo ontology](../technical-documentation/ontology) used in this project.

## Schemas

| Document de travail| Image |
| -------- | -------- |
| HeCo Ontology version 1.0|![](/img/OntologyHeco-v1.0.svg)|
| HeCo Ontology version 0.9|![](/img/OntologyHeco-v0.9.png)|
| HeCo Ontology version 0.8|![](/img/OntologyHeco-v0.8.png)|
| HeCo Ontology version 0.7|![](/img/OntologyHeco-v0.7.png)|
| HeCo Ontology version 0.6|![](/img/OntologyHeco-v0.6.png)|
| HeCo Ontology version 0.5|![](/img/OntologyHeco-v0.5.png)|
| HeCo Ontology version 0.4|![](/img/OntologyHeco-v0.4.png)|
| HeCo Ontology version 0.3|![](/img/OntologyHeco-v0.3.png)|
| HeCo Ontology version 0.2|![](/img/OntologyHeco-v0.2.png)|
| HeCo Ontology version 0.1|![](/img/OntologyHeco-v0.1.png)|

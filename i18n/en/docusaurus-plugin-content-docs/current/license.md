---
title: License
sidebar_position: 60
---

# License

## Introduction
This project includes a set of deliverables, and we use in each deliverable, open-source software whose license we must quote.

## Carto4CH project licence
Globally, the Carto4CH project is published under the [CC-BY-SA version 4.0 license](https://creativecommons.org/licenses/by-sa/4.0/deed.fr).

This includes : 
* The **portal**
* The **White Paper**
* The **tutorials** (and associated code)
* HeCO **ontology**
* The **demonstrator**
  * Archipelago-Carto4CH
  * My-competence
* **Carto4CH-deploy** (for partners wishing to deploy their server)

## Projects used and their licences
Carto4CH mainly uses the following projects: 
* **SemApps** : [Under Apache license](https://github.com/assemblee-virtuelle/archipelago/blob/master/LICENSE)
* **Minicourse**: [Under Apache license](https://github.com/assemblee-virtuelle/minicourses/blob/master/LICENSE)
* **SOLID-client library** (for tutorials): [Under MIT licence](https://github.com/inrupt/solid-client-js/blob/main/LICENSE)
* **Docusaurus**: [Under MIT licence](https://github.com/facebook/docusaurus/blob/main/LICENSE)

---
title: Competences workshops
sidebar_position: 40
---

# Workshops

## Introduction

We are organizing **Competences workshops** from October 2022.

## Upcoming workshops

:::caution
We will soon be rescheduling workshops to support partners editing their competences on the demonstrator.
:::

## Objectives

* Gather regularly the opinion and the needs of the partners
* Present the new features of the [HeCo ontology] (./technical-documentation/ontology)
* Present the new features of the demonstrator
* Improve the user experience
* Support users in entering competences
* Adapt the [Schedule](./planning)
* Train partner developers

## When?

The dates will be communicated by email, and the recipients will be chosen according to the themes addressed.

## Frequency

A first series of workshops will be proposed at the end of 2022, to collect improvement requests, then another at the beginning of 2023, following the realization of the improvements.

## Method

These workshops will be online, the video link will be communicated in advance by email.

The people invited will come from several organizations.

Some workshops will be more technical, involving experts, in order to meet specific needs.
Others will be more open, adapted to the non-initiated, with a vocation of sharing, popularization and request for opinions, field needs.

## History of the workshops

* Friday **07/04/23** : Frontend tests (BO et MY)
* Friday **31/03/23** : Frontend tests (BO et MY)
* Friday **24/03/23** : Frontend tests (BO et MY)
* Thursday **December 8 at 3pm**: a collaborative synthesis workshop
* Tuesday **December 6 at 10am**: a workshop on ontology and repositories (you don't need to be an ontologist to participate)
* Friday **November 25 at 3pm**: a practical collaborative input workshop on the SemApps online interface (on the demonstrator) for skills input, to get your feedback, and discuss together the improvements to be made
* Monday **November 21 at 3pm**: an introductory workshop for partners, presenting the project, the state of the art, and our thoughts on how to manage and capture skills
* **14/10/2022** : Presentation of the [HeCo ontology](./technical-documentation/ontology) to the members of the Virtual Assembly.
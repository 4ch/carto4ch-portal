---
title: Demonstrator development manual
sidebar_position: 5
---

# Demonstrator development manual

![](/img/demonstrator-manual.png)

## Introduction

This documentation is intended for developers who want to understand how the demonstrator works, or how to implement their own server based on the [SemApps](/docs/technical-documentation/presentation-semapps).

Above all, we will explain which original projects it is made up of, and which overloads are specific to this project.

This documentation will not explain the internal workings of each original project; for this, see the [SemApps presentation](/docs/technical-documentation/presentation-semapps).

To find out more about deploying this demonstrator, see the [Deploying the demonstrator](/docs/technical-documentation/demonstrator-deploy) section.

## Technical requirements

* Understand the **semantic web** and the [HeCo ontology](/docs/technical-documentation/ontology)
* Have integrated the technical documentation for [SOLID](/docs/technical-documentation/standards/presentation-solid)
* Have integrated the technical documentation for [SemApps](/docs/technical-documentation/presentation-semapps)
* Master the protocols : 
  * [LDP](https://www.w3.org/TR/ldp/)
  * [SPARQL 1.1](https://www.w3.org/TR/sparql11-protocol/)
  * [Activity Pub](https://www.w3.org/TR/activitypub/)

## Reminder of the general architecture

|Reminder of architecture|Documented building blocks|
|----|----|
|![](/img/demonstrator-architecture-schema.png)|The **demonstrator** is a functional demonstration cartography.<br/>Its aim is to provide an example of what is expected from the Carto4CH project.<br/><br/>This documentation explains how the demonstrator is hosted and how each of its components works:<br/><ul><li>**The two middlewares** of the demonstrator (Instance A & Instance B)</li><li>**The 3 applications** to cartography the data in each instance</li><li>**The mirror instance**</li><li>The **Network visualisation**</li></ul>|

## Fork original projects

|Original projects|Customization|
|----|----|
|![](/img/demonstrator-forks.png)|Each **demonstrator** project is a fork of an original project maintained by the Virtual Assembly (VA).<br/><br/>- Archipelago, <br/>- Minicourse <br/>- CartoSemapps. <br/><br/>This means that the Carto4CH project is doubly supported, both by the members of the 4CH team and by the members of the AV association.<br/>|

## The two instances of middleware A et B

### Architecture of middleware
As described in the [SemApps technical presentation](/docs/technical-documentation/presentation-semapps), the middleware contains **the database**, and **a Javascript orchestrator** (moleculer) which organises the launch of different services.

### Moleculer services used
When the middleware is started with ``yarn run dev``, it runs the ``moleculer-runner --repl --hot services`` script (see package.json file), which starts the services and puts them on hold. The list of SemApps services can be found in the [Official SemApps Documentation](https://semapps.org/docs/middleware). In the Carto 4CH project, we use : 
* **auth**: authentication management
* **backup**: for regular backups of the database
* **core**: which is the first service that then calls the others
* **inference**: handles inferences and, in our case, inverse relations
* **sync**: handles synchronisation and mirroring
* **webacl**: manages permissions in the database
* **webid**: manages web ID profiles.

All these services can also instantiate others. For example, the **core** also launches the **activity pub** service if required, so there's no need to add it to the **packages.json** file.

### Carto4CH middleware features
#### Few overloads
Generally speaking, we haven't touched the middleware code very much. We've remained fairly faithful to the original [Archipelago](https://github.com/assemblee-virtuelle/archipelago), apart from a few specific features relating to containers and the ontology used (+ context).

#### Containers
We have declared containers that are useful for skills management. To do this, we modified the **config/containers.js** file.

#### HeCo ontology and context.json
We have also added the URI of the HeCo ontology to the list in the **config/ontologies.json** file.

We also added a **context.json** file containing the list of relations (predicates) used. To do this, we had to modify the **core.service.js** and **jsonld.service.js** files to specify the new local context to be used.

#### Managing the relationship between competences and P/C/Fs
Pour les besoins du projet, nous avons modifié le service **LDP**.
Nous avons ajouté un hook dans le fichier **ldp.service.js**, lors du GET, du PUT et du CREATE, pour modifier les prédicats utilisés.

## The 2 back-office(s)

### Archipelago
These 2 frontends (bo-a and bo-b) use the [Archipelago back-office interface](https://github.com/assemblee-virtuelle/archipelago).

For more details on the Archipelago project, you can read [the SemApps presentation](/docs/technical-documentation/presentation-semapps)

### The special features of Archipelago-carto4CH

Many overloads have been added to this project.
* Theme customisation is managed by the docker config.
* Interoperability management.
* Management of jobs, training and contributions
* Project management
* Skills management using the HeCo ontology
* Management of qualities and professions
* Query ESCO and Wikidata repositories
* Single-page forms
* Automatic titles for skills
* Relationship between skills and P/C/Fs

:::caution
Archipelago-Carto4CH regularly receives updates from : 
* Archipelago AV, 
* SemApps components 
* React-Admin developments (especially the move to V4)
:::

## Demonstrator user documentation

* See: [Demonstrator user documentation](/docs/category/doc-demonstrator-category-label-2)
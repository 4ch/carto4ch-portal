---
title: Network visualisation
sidebar_position: 6
---

# Network visualization 

## Introduction
We will explain here how the **networked visualization** used for Carto 4CH works.

|Drawing|Presentation|
|----|----|
|![](/img/visualisation-flodio.png)|Network visualisation provides a 3rd way of browsing data and searching for skills|

## How it works
This visualisation uses the [Flodio](https://flod.io) software and the [FLOD](https://pad.lescommuns.org/flodio) concept to navigate the data graph.

Flodio directly queries data conforming to the **HeCo** ontology on one of the Carto4CH instances, and uses the **D3JS** library to display the data in a graph.

It is possible to filter the data using the menu at the top left, and to search using the search engine at the top right.

## Access URL
See links on [Carto 4CH Demonstrator](/docs/applications/demonstrator)
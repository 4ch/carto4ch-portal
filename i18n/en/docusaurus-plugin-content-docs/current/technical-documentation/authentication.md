---
title: Authentication / SSO
sidebar_position: 9
---

# How authentication works

```
┌────────────────────────────────────────────┐
│           SSO / OIDC / SOLID-OIDC          |
│--------------------------------------------│
│       Application            Server OIDC   |
│   ┌───────────────┐                        │
│   │  ┌─────────┐  │                        │
│   │  |  Login  │  │          ┌─────────┐   │
│   │  └─────────┘  │  <====>  |  OIDC   │   │
│   │  ┌─────────┐  │          └─────────┘   │
│   │  |  pass   │  │                        │
│   │  └─────────┘  │                        |
│   └───────────────┘                        │
└────────────────────────────────────────────┘
```

## Introduction
We will explain here how **authentication** works in Carto 4CH, what are the current limits, and the next evolutions.

## Different protocols
### OpenID Connect (OIDC)
The OIDC protocol is one of the most widely used SSO (Single Sign On) protocols.

See [**Standards > OIDC**](/docs/technical-documentation/standards/oidc)

### Central Authentication Service (CAS)
CAS is another frequently used authentication protocol.

https://fr.wikipedia.org/wiki/Central_Authentication_Service

## Examples of authentication servers
### Common OIDC server
We currently use the Common OIDC server (https://login.lescommuns.org/). This is a server whose governance is shared by several organisations, on which we have installed the open-source software [Keycloak](https://www.keycloak.org/).

SemApps offers this type of authentication by default, but we can also configure other protocols, such as **CAS**.

### The authentication server at the University of Tours
When we want to connect to a digital service at the University of Tours, we are presented with an authentication request.

https://cas.univ-tours.fr/cas/login

This server uses the CAS protocol.
We asked the university if we could use it for Carto4CH, but a priori it is reserved for applications used by the university, and not for research projects.

### The 4CH authentication server

The 4CH project, or rather the [INFN](https://fr.wikipedia.org/wiki/Istituto_nazionale_di_fisica_nucleare), uses an SSO server called INDIGO-IAM, which uses the OIDC protocol.

We tried to replace the common OIDC server with the INFN server in SemApps.

Unfortunately, we encountered technical problems that could not be resolved before the end of the project.

## Future improvements in SemApps
See [**Recommendations > Authentication**](/docs/recommendations/authentication.md)

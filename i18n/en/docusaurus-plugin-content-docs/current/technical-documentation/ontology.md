---
title: Ontology
sidebar_position: 4
---

# HeCo Ontology

## Introduction

In this page we will describe the Carto4CH ontology. 
The deliverable will be a set of parts, versioned:
* The [**brut file (OWL/TTL)**](#brut-file-owlttl) containing the ontology in [turtle](https://fr.wikipedia.org/wiki/Turtle_(syntax))
* The [**WIDOCO format (HTML)**](#widoco-format-html) of the ontology in the form of an HTML page
* A [**schema (SVG)**](#schema-svg) of the ontology
* A [**résumé (TXT)**](#résumé-txt) the contributions of the latest version
* A [**document (PDF)**](#document-pdf) containing the history of the reflections

[ [Extract from wikipedia](https://en.wikipedia.org/wiki/Ontology_(information_science)) ]
In computer science and information science, an ontology encompasses a representation, formal naming, and definition of the categories, properties, and relations between the concepts, data, and entities that substantiate one, many, or all domains of discourse. More simply, an ontology is a way of showing the properties of a subject area and how they are related, by defining a set of concepts and categories that represent the subject.

In our case, the **knowledge set** to be modeled is represented by actors and their competences in the cultural heritage domain.

For the moment, we have given to this ontology the name : **HeCo** (for **He**ritage **Co**mpetence).

The objective of this ontology is to formalize how we want to organize the information within our cartography.
It also allows us to clearly define the **perimeter** of the project. We will limit the cartography to the concepts and relationships present in this ontology.

## Status

:::info
Published in version 1.0.
:::

## Versions

| Version | Date | Description| Comments|
| -------- | ----- | -------- | ------- |
| 1.0 | 01/09/2023 | Addition of all used inverse relations | Stable version that can be used in a production and maintenance cycle.|
| 0.9 | 21/08/2023 | Update of some relations | Link between contributions and projects.
| 0.8 | 07/03/2023 | Representation of the two possible modes for the skill facets | This version shows some stability, our goal is for the demonstrator to be as aligned as possible with this version. |
| 0.7 | 27/01/2023 | Clarifications on "positions", "training", "contribution", and on qualities | On the demonstrator side, this version accompanies the abandonment of reified relationships. |
| 0.6 | 25/11/2022 | Simplification by representing facets as "qualities" | This version is linked to the use of reified relations. |
| 0.5 | 07/11/2022 | Taking into account the training courses | This version is the first to be published to the french partners |
| 0.1 to 0.4 | 01/09/2022 | First version | Internal Working Versions, to start implementation in SemApps |

## Deliverable

### Brut file (OWL/TTL)

The **heco.owl** document has been edited with the [Protege](https://protege.stanford.edu/) tool.
We store it on the [Gitlab Carto4CH](https://gitlab.huma-num.fr/4ch) project, so that it can be edited collectively.

* [Link to OWL file on Gitlab](https://gitlab.huma-num.fr/4ch/heco/-/raw/main/heco.owl)

### WIDOCO Format (HTML)
The WIDOCO (WIzard for DOCumenting Ontologies) software provides an HTML interface that is more easily readable by humans.

* [Link to WIDOCO (HTML) format](pathname:///widoco/index-en.html)

### Schema (SVG)

* Find the versionned schemas [in the Schemas > Ontology section](/docs/schemas/ontology)

### Résumé (TXT)

The actors remain modelled as in CIDOC CRM. Three relationships are defined between people and groups (which represent projects, associations, organisations, etc.). These three relationships can be characterised by a start date and end date and by the competences acquired: the first, Job, represents an employee-employer relationship and is characterised by a type of job (JobType) and the job title (Occupation). The second, Commitment, represents a relationship of involvement in a project, association, or other type of group. The third, Training, represents a relationship of training, delivered by a group, it can be characterised by a diploma, certificate...

A person may have declared zero or more competences. A competence is defined by several facets, a facet can specify a knowledge (a scientific discipline such as archaeology, an area of activity such as archives, or an object of study such as the work of stone by the builders of cathedrals), or a mastery of a tool, a method, a technique, or a behavioural competence. A facet can also define a level of competence in a given scale, such as EQF. In version v0.8, two representations co-exist: a generic representation with facets (each composed of a type and a value) and the representation adopted in the first version of carto4CH, where only four facets are represented, each by a class and its corresponding property: Discipline, Area, StudiedObject, and Tool.

### Document (PDF)
Access the progressive design document of the HeCo ontology

:::caution
Sorry, the document is in french and not yet translated.
We will do this work when we'll have interactions with 4CH project team.
In the meantime, see french version...
* [Ontologie HeCo 1.0](/documents/PresentationHeCoV1.0.pdf)
:::

## Users
It will be used as a support :
* **for the Carto4CH project partners:** to collectively evolve the cartography modeling, over time, according to the needs
* **for the Carto4CH project developers:** who will use it as a data model to program their access and implement interfaces

## Publication
We wish to publish this ontology :
* first to the restricted circle of project partners
* if it is adopted for the project, it could be published and communicated more widely in France
* if it is used by several French actors, it will be proposed to the 4CH project

The 4CH project is also working on one or several ontologies, it will certainly be necessary to work on the alignment of all these ontologies, so that they work together.

## Method
The method used in this project is agile, iterative and collaborative.

## Monitoring
Since April 2022, we have been monitoring existing ontologies and repositories at the global, European and French levels.
We will explain in the document below what we have retained in each of these projects.

To this survey, we have added the missing parts, specific to our project.

## Implementation
Then, we started to implement in the [SemApps](/docs/technical-documentation/presentation-semapps) tool the concepts and relations present in this ontology, to realize the limitations, the omissions, the new needs.

Regular adaptations of the software and the ontology will help us to find the balance in time.

A preliminary interface for entering competences is developed to evaluate the ontology design choices. The partners will be asked to use this interface during the skills workshops.

## Competences workshops
Collaborative [workshops](../workshops) will be proposed to the partners on a regular basis to collectively reflect on the evolution of this ontology and to give their opinion on the input interface.

## Putting into production
Once this evaluation phase is stabilized, production instances can be deployed.

In this last phase of the project, the ontology's evolutions will be minimal (because they will have a greater impact in production)

---
title: Inverse relationship
sidebar_position: 1
---

# Inverse relationship 

```
┌────────────────────────────────────────────┐
│              Inverse relationship          |
│--------------------------------------------│
│   ┌─────────┐                ┌─────────┐   │
│   │    A    │      ====>     |    B    |   │
│   └─────────┘                └─────────┘   │
│   ┌─────────┐   inverse of   ┌─────────┐   │
│   │    B    │      ====>     |    A    |   │
│   └─────────┘                └─────────┘   │
└────────────────────────────────────────────┘
```

## Introduction
This section explains the notion of ‘inverse relationship’ as it is currently used in Archipelago.

## What is an inverse relationship?
In semantic web, we use triplets to represent information: ‘Subject > Predicate > Object’.
The inverse relationship will therefore be the opposite information ‘Object > Predicate > Subject’.
We will therefore need to find inverse predicates for the vast majority of relations in the [Heco ontology](/docs/technical-documentation/ontology).

## Example

```
┌────────────────────────────────────────────┐
│                  Example                   |
│--------------------------------------------│
│   ┌─────────┐                ┌─────────┐   │
│   │ Person 1│ hasCompetence  |    C1   |   │
│   └─────────┘                └─────────┘   │
│   ┌─────────┐                ┌─────────┐   │
│   │    C1   │ isCompetenceOf | Person 1|   │
│   └─────────┘                └─────────┘   │
└────────────────────────────────────────────┘
```
To describe the relationship ‘a person has a skill’ in the **Heco ontology**, you also need to describe that a skill is linked to a person.

In the interface, this will display : 
* The list of a person's skills
* but also the list of people who have this skill

## How do you specify it in the ontology?
To do this, we use the relationship borrowed from the [OWL language](https://fr.wikipedia.org/wiki/Web_Ontology_Language): ‘owl:inverseOf’.
We start by specifying that the **hasCompetence** property links the **Person** class to the **Competence** class, and that this property also has an inverse relationship **isCompetenceOf**.

This is written as follows: 
```
:hasCompetence rdf:type owl:ObjectProperty ;
               owl:inverseOf :isCompetenceOf ;
               rdfs:domain :Person ;
               rdfs:range :Competence ;
```

In the other direction, we need to specify that the **isCompetenceOf** property links the **Competence** class to the **Person** class, and that this property also has an inverse relationship **hasCompetence**. 

```
:isCompetenceOf rdf:type owl:ObjectProperty ;
                owl:inverseOf :hasCompetence ;
                rdfs:domain :Competence ;
                rdfs:range :Person ;
```

## How inverse relations work in SemApps

For the moment, SemApps uses the [Inference] service (https://semapps.org/docs/middleware/inference) to extract inverse relations from the ontology.

When a triplet is registered, it checks its list of inverse relations, and if the class is concerned, it automatically generates the associated triplet in the database.

In the same way, it must remove the inverse triples when a request is made to delete an item of information.
---
title: LDP
sidebar_position: 2
---

# LDP (Linked Data Platform) 

```
┌────────────────────────────────────────────┐
│        Linked Data Platform / Protocol     |
│--------------------------------------------│
│       LDP Server             Application   |
│   ┌───────────────┐                        │
│   │  ┌─────────┐  │                        │
│   │  |  LDP    │  │                        │
│   │  | service │  │   HTTP   ┌─────────┐   │
│   │  └─────────┘  │  <====>  |  App    │   │
│   │      v ^      │   GET    └─────────┘   │
│   │  ┌─────────┐  │   POST                 │
│   │  | RDF data│  │   PATCH                │
│   │  └─────────┘  │   PUT                  |
│   └───────────────┘   DELETE               │
└────────────────────────────────────────────┘
```

## Introduction
This section explains the concept of the **Linked Data Platform (LDP)** used by the SemApps toolbox.

You can already read the following page: https://en.wikipedia.org/wiki/Linked_Data_Platform

## In a nutshell
This is a standard way of accessing RDF data on a server (linked data).

A server is called an ‘LDP server’, or ‘LDP platform’, when it can be queried and updated using only [HTTP REST](https://en.wikipedia.org/wiki/REST) commands.

## Example
For example, **Archipelago** is an LDP server, as it exposes its data via an [LDP service](https://semapps.org/docs/middleware/ldp/resource).

The easiest way to observe this is to make a GET request using your browser to the ‘organisation’ LDP container on the **Carto4ch-a** server.

https://data-a.carto4ch.huma-num.fr/organizations

You will then get an immediate response containing a list of all the organisations.

```
{
  "@context": [
    "https://www.w3.org/ns/activitystreams",
    "https://data-a.carto4ch.huma-num.fr/.well-known/context.jsonld"
  ],
  "id": "https://data-a.carto4ch.huma-num.fr/organizations",
  "type": [
    "ldp:Container",
    "ldp:BasicContainer"
  ],
  "ldp:contains": [
    {
      "id": "https://data-a.carto4ch.huma-num.fr/organizations/ministere-de-la-culture",
      "type": "heco:Organization",
      "dc:created": "2022-04-22T20:50:34.132Z",
      "dc:creator": "https://data-a.carto4ch.huma-num.fr/users/yannickduthe",
      "dc:modified": "2022-04-25T08:20:28.9Z",
      "pair:hasType": "https://data-a.carto4ch.huma-num.fr/types/institution",
      "pair:homePage": "www.culture.gouv.fr/",
      "pair:label": "Ministère de la culture",
      "heco:hasTopic": "https://data-a.carto4ch.huma-num.fr/themes/culture"
    },
```

## W3C specifications
Technicians can read the specifications at : 
https://www.w3.org/TR/ldp/
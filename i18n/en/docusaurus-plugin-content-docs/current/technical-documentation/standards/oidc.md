---
title: OIDC
sidebar_position: 2
---

# OIDC 

```
┌────────────────────────────────────────────┐
│           SSO / OIDC / SOLID-OIDC          |
│--------------------------------------------│
│       Application            Server OIDC   |
│   ┌───────────────┐                        │
│   │  ┌─────────┐  │                        │
│   │  |  Login  │  │          ┌─────────┐   │
│   │  └─────────┘  │  <====>  |  OIDC   │   │
│   │  ┌─────────┐  │          └─────────┘   │
│   │  |  pass   │  │                        │
│   │  └─────────┘  │                        |
│   └───────────────┘                        │
└────────────────────────────────────────────┘
```

## Introduction
This section explains the OIDC protocol used by SemApps for [**authentication**](/docs/technical-documentation/authentication).

https://en.wikipedia.org/wiki/OpenID#OpenID_Connect_(OIDC)

## In a nutshell
The OIDC protocol is one of the most widely used authentication protocols (CAS also exists).

It enables a set of applications to manage users' personal data and application access authorisations in a single place. This is also known as [**SSO** (Single Sign On)](https://en.wikipedia.org/wiki/Single_sign-on)).

## Use in SemApps
The Virtual Assembly is a partner in an OIDC server project using **Keycloak** software. This is a way of managing authentication in the Carto4CH project.

More information on the page [**authentication**](/docs/technical-documentation/authentication)

:::caution
Status : To be improved
:::
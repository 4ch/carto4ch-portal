---
title: SOLID
sidebar_position: 3
---

# SOLID

## Introduction
This presentation explains the SOLID standard (from W3C) that will be used in this project.

For the [Carto4ch demonstrator](/docs/applications/demonstrator), we will use the [SemApps](/docs/technical-documentation/presentation-semapps) software, which implements this standard.

## Go to the SOLID presentation

For the moment, as this presentation is still young and can be modified, we use a collaborative PAD to update it more easily.
* [Presentation link](https://pad.lescommuns.org/p/SlideSolid-en)
* [Source link](https://pad.lescommuns.org/SlideSolid-en)

(If you would like to contribute content, please contact us).

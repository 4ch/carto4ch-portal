---
title: SemApps
sidebar_position: 3
pagination_next: null
---

# SemApps Tutorial

## Introduction

The SemApps tutorial is an application written in React JS that queries data from one or more SemApps servers.

## Status

:::caution
This tutorial is in its early stages, please feel free to contact us if you have any special requirements.
:::

## Access to the tutorial

* Functional application : https://semapps-tutos.vercel.app/
* Github : https://github.com/fluidlog/semapps-tutos

## Documentation

For the moment, the documentation is on a collaborative PAD, it will be deposited here when it is stabilized:
* https://pad.lescommuns.org/semapps-tutos

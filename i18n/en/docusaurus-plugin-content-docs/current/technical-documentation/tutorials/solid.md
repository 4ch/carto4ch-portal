---
title: SOLID
sidebar_position: 2
pagination_prev: null
---

# SOLID Tutorial

## Introduction

The SOLID tutorial is an application written in React JS that queries data on one or more SOLID PODs.

We advise you to first clone the project code and run it locally on your machine to follow this tutorial step by step.

The documentation is there to guide you in understanding the code.

## Status

:::tip Status
This tutorial is considered **finished** (even if it has possible improvements, it is only a tutorial, not a production application).
:::

## Summary
This tutorial contains :
* a first tutorial listing simply the content of a turtle file present in a POD Solid
* a second one that displays a form allowing to list choose a uri and to display the users of different POD Solid
* a third one that displays and modifies the meaning of the terms of a glossary. These terms are stored in SKOS format in a POD Solid
* A fourth one that allows you to add new terms or to delete them

The [Carto4CH Glossary](/docs/project-documentation/glossary) uses the list of terms present in Tutorials 3 and 4.

Do not hesitate to contact us if you have any questions.

## Documentation

For the moment, the documentation is on a collaborative PAD, it will be deposited here when it is stabilized.
* Documentation: https://pad.lescommuns.org/Solid-Tutos

## Access to the Tutorial

* Working app: https://4ch.gitpages.huma-num.fr/solid-tutos (publication of tuto4)
* Code: https://gitlab.huma-num.fr/4ch/solid-tutos

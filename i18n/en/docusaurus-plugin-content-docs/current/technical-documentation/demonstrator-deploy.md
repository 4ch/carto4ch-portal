---
title: Deployment of the demonstrator
sidebar_position: 6
---

# Deployment of the demonstrator Carto4CH

## Introduction

This documentation explains how the demonstrator applications are **hosted** and **published**.

## Technical requirements

* Internet access
* A hardware configuration with at least 16 GB RAM
* Install [Docker](https://docs.docker.com/) and [docker-compose](https://docs.docker.com/compose/)
* Install [Make](https://www.gnu.org/software/make/manual/make.html)

## Reminder of the general architecture

|Reminder of architecture|Documented building blocks|
|----|----|
|![](/img/demonstrator-architecture-schema.png)|The **demonstrator** is a functional demonstration cartography.<br/>Its aim is to provide an example of what is expected from the Carto4CH project.<br/><br/>This documentation explains how the demonstrator is hosted and how each of its components works:<br/><ul><li>**The two middlewares** of the demonstrator (Instance A & Instance B)</li><li>**The 3 applications** to cartography the data in each instance</li><li>**The mirror instance**</li><li>The **Network visualisation**</li></ul>|

## Docker deployment

|Original projects|Git clone + AddOn|
|----|----|
|![](/img/demonstrator-docker.png)|Each docker project on **Server A or B** performs a **git clone** of the **Archipelago-carto4CH** (middleware and frontend) or **My-competence** projects.<br/><br/>It then retrieves specific files from the **AddOn** directory to customise the colours, title or interoperability parameters.|

## Deployed bricks

On the [Gitlab Huma-Num du projet Carto 4CH > Carto 4CH](https://gitlab.huma-num.fr/4ch/carto4ch), you will find all the docker configurations needed to run the demonstrator on the Huma-Num server.

The Docker configuration consists of 11 docker containers.

|Infrastructure|Containers|
|----|----|
|![](/img/demonstrator-hosting.png)|**Support containers** <br/><br/>- **Traefik** (network management, ports, https, certificates)<br/>- **Jena/Fuseki** (Triplestore)<br/>- **Redis** (cache)<br/><br/>**Applicatifs containers**<br/><br/>**Instance A** <br/>- Middleware A (data-a)<br/>- BO A<br/>- MY A <br/>**Instance B**<br/>- Middleware B (data-b)<br/>- BO Serveur B<br/>- MY Serveur B<br/><br/>**Miroir instance** (C)<br/><br/>**Carto 4CH portal**|

All these containers can be launched with a simple command : ```make start-prod```

This command calls the file [docker-compose-prod.yaml](https://gitlab.huma-num.fr/4ch/carto4ch/-/blob/main/docker-compose-prod.yaml?ref_type=heads), which launches the 11 containers.

The server logs can be viewed using the command: ```make log-prod```.

## How application containers work

### Principle of operation
Each application container starts by making a clone of one of the Carto 4CH projects (Archipelago or My-competences), then overwrites certain files with a specific configuration for the instance.

### A/B/C middleware
The 3 middlewares (data-a, data-b and data-c) clone the [Archipelago Carto4CH > middleware](https://gitlab.huma-num.fr/4ch/archipelago-carto4ch/-/tree/main/middleware?ref_type=heads), and override the files in the **AddOn** directory.

In the case of mirroring, the **services/mirror.service.js** file is added.

### A & B Back-office
The 2 back-offices (bo-a and bo-b) clone the [Archipelago Carto4CH > frontend](https://gitlab.huma-num.fr/4ch/archipelago-carto4ch/-/tree/main/frontend?ref_type=heads), and overload their files by retrieving those in the **AddOn** directory: 
* "Server A" title
* colours, 
* interoperability configuration: we are going to allow Server A to see Server B's data and vice versa.

### My-competences A & B
The 2 My-competences interfaces (my-a and my-b) clone the [My-competences](https://gitlab.huma-num.fr/4ch/my-competences), and overload their files by retrieving those in the **AddOn** directory: 
* title 
* colours
* interoperability configuration

## Visualisations

The demonstrator offers 3 visualisations: 
* A view of middleware A data
* A view of middleware B data
* A view of data from middleware C (mirror) for general viewing.

To find out more, read [Network visualisation > demonstrator](/docs/technical-documentation/visualisation#functioning-for-the-demonstrator)
---
title: Deploying a Carto4CH server
sidebar_position: 5
---

# Deploying a Carto4CH server 

## Introduction
Here we explain how to **personalize and deploy a production Carto4CH server**.

|Schema|Presentation|
|----|----|
|![](/img/infra-deploy.png)|The [Git project "Carto4CH-deploy"](https://gitlab.huma-num.fr/4ch/carto4ch-deploy) allow a partner to install his own server Carto4CH. <br/><br/>I has 5 docker images <br/><ul><li>Traefik (https)</li><li>Jena fuseki (Triplestore)</li><li>SeMApps middleware</li><li>Back-office</li><li>My competence</li></ul>|

## Statut

:::info
The deployment package is now operational.

Don't hesitate to contact us if you have any special requirements.
:::

## How to deploy ?
### Prerequisites
* Internet connexion on the server :)
* A hardware configuration with at least 4GB RAM
* Have installed [Docker](https://docs.docker.com/) and [docker-compose](https://docs.docker.com/compose/)
* Have installed [Make](https://www.gnu.org/software/make/manual/make.html)

### Clone the projet
Clone the project on your server:

```git clone git@gitlab.huma-num.fr:4ch/carto4ch-deploy.git```

### Setting up your instance
Enter the project:

```cd carto4ch-deploy```

#### Domain name in traefik
Update the root **docker-compose.yaml** file with your reserved domain name for your instance.

```
  data:
    build:
...
    expose:
      - "3000"
    labels:
      - "traefik.enable=true"
      - "traefik.http.routers.data.rule=Host(`data.carto4ch.<domain-name>`)"
...
```

#### Back-office settings (bo)
Next, update the instance name, logos and colors by modifying the **AddOn** in the **bo** directory.

These AddOn override certain files in the [Basic Git project](https://gitlab.huma-num.fr/4ch/archipelago-carto4ch).

First, the **.env.local** file.

```
SEMAPPS_INSTANCE_NAME=Back-office (Server A) // Update your instance name
REACT_APP_MIDDLEWARE_URL=https://data.carto4ch.huma-num.fr/ // Update your domain name
PORT=4000 // We recommend keeping the ports specified in the documentation
REACT_APP_LANG=en // Choose your language
```

The **public** directory contains the images : 
* favicon.ico
* logo192.png
* logo512.png
Can be replaced by images of your choice

The **public/index.html** file contains the instance name in several places

```
    <meta
      name="description"
      content="Back-office [Server A]"
    />
...
    <title>Back-office [Server A]</title>
```

The **src/App.js** file contains: 
* banner and font colors

```
theme.palette.primary.main = "#AE4796"
```

* the instance name (react-admin)

```
const App = () => (
  <Admin
    disableTelemetry
    history={history}
    title="Server A"
```

The **src/config/dataServer.js** file if you wish to connect to another server or manage a mirror (see interoperability documentation).

#### Setting up My competence (my)
As with the Back-Office, you can update the instance name, logos and colors by modifying the **AddOn** in the **my** directory.

These AddOn override certain files in the [Basic Git] project (https://gitlab.huma-num.fr/4ch/my-competences).

### Deployment

Start installation:

```make start-prod```

You can also do it in two steps:

```make build-prod``` followed by ```make start-prod```.

To check that all has gone well, you can consult the logs with the command :

```make log-prod```

### Access

Once you've deployed your server (and configured your DNS), you can access : 
* https://data.carto4ch.your-domain-name // To access the middleware and browse LDP data.
* https://bo.carto4ch.your-domain-name // To access the back office
* https://my.carto4ch.your-domain-name // To access My-competence

### Authentication
If you have any questions about authentication (new SSO server or common server configuration), please contact us.
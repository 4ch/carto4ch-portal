---
title: Presentation
sidebar_position: 2
pagination_prev: null
---

# Presentation

## Introduction
This presentation describes the proposed technical architecture.

:::caution
To be translated in english...
:::

## Go to the general presentation

For the moment, as this presentation is still young and may be modified, we use a collaborative PAD to update it more easily.
* [Presentation link](https://pad.lescommuns.org/p/SlideInteroperabiliteSemapps)
* [Source link](https://pad.lescommuns.org/SlideInteroperabiliteSemapps)

(If you would like to contribute content, please contact us).

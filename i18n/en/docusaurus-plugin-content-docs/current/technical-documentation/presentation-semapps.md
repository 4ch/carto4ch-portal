---
title: SemApps
sidebar_position: 3
---

# SemApps
```
┌────────────────────────────┐      ┌───────────────────────────┐
│     SemApps Server         |      │      User Interface       |
│----------------------------│      │---------------------------│
│ ┌────────────────────────┐ │      │ ┌───────────────────────┐ │
│ │  Middleware (Node JS)  │ │      │ │     React-Admin       │ │
│ │------------------------│ │      │ │-----------------------│ │
│ │ ┌────────────────────┐ │ │<-----│ │ ┌───────────────────┐ │ │
│ │ │ Triplestore (Java) │ │ │      │ │ │ SemApps Components│ │ │
│ │ └────────────────────┘ │ │      │ │ └───────────────────┘ │ │
│ └────────────────────────┘ │      │ └───────────────────────┘ │
└────────────────────────────┘      └───────────────────────────┘
```
## Introduction
SemApps is an open-source toolkit for building semantic applications.

SemApps is very much inspired by the W3C proposals: [SOLID](/docs/technical-documentation/standards/presentation-solid).

SemApps was developed by members of the [Virtual Assembly](https://virtual-assembly.org).

For a better understanding of the "toolbox" concept, you can consult the [software architecture diagram here](/docs/schemas/architecture).

## Presentation (non-technical) of SemApps

This presentation will help you to understand SemApps.

**Non-technical** means that we can use technical terms, but only if they are explained.

It goes back over the notion of the **distributed web**, the **standards** used (semantic web, LDP, SPARQL, WAC, Activity Pub), the various **user interfaces** possible and the **modularity** that enables SemApps to build semantic applications.

It can be considered as **a prerequisite** for reading this page.

:::info Access
For the moment, this presentation is only in french version, but we are working on a english version
* [Link in presentation mode](https://pad.lescommuns.org/SlideSemapps-en)
* [Link to source](https://pad.lescommuns.org/SlideSemapps-en)
:::

:::caution
For the moment, as this presentation is still in its infancy and may need to be modified, we are using a **collaborative PAD** to update it more easily.
:::

## SemApps website
|||
|----|----|
|![](/img/semapps-org.png)|https://semapps.org|

This site presents : 
* the **toolbox**, 
* the [SemApps team](https://semapps.org/team) that contributes to the project, 
* the **projects completed**, 
* **the documentation** for all the **reusable components** (see details further down this page...).

## Reminder: Freedom of choice of technologies for Carto4CH

![](/img/liberte-choix-techno.png)

As a reminder, what we are proposing in Carto4CH project, is only a demonstrator, a technical base chosen for the example.

We have chosen to use **SemApps**, but a partner can choose other software bricks, as long as they share **semantic web standards**, and **the HeCo ontology**.

With this demonstrator, we just want to show that it is possible to create interoperable applications.

**Example:**
A partner can rewrite interfaces using a language other than React JS (e.g. Angular JS). 
They can also develop their own semantic data server using a language other than Node JS (e.g. Django), as long as it is based on the W3C proposals [SOLID](/docs/technical-documentation/standards/presentation-solid).


## Technologies used for the demonstrator
|schema of the demonstrator|Technical architecture|
|----|----|
|![](/img/demonstrator-architecture-schema.png)|**User interfaces** <br/>- Front-Office & My-competences : [React JS](https://fr.reactjs.org/) / [React-Admin](https://marmelab.com/react-admin/)<br/>- Network cartography : [D3js](https://d3js.org/)<br/><br/>**Backend**<br/>[Node JS](https://nodejs.org/en/) / [Moleculer](https://moleculer.services/)<br/><br/>**Protocoles**<br/>[LDP](https://www.w3.org/TR/ldp/)<br/>[SPARQL 1.1](https://www.w3.org/TR/sparql11-protocol/)<br/>[WAC](https://www.w3.org/wiki/WebAccessControl)<br/>[Activity Pub](https://www.w3.org/TR/activitypub/)|

These technical choices were made by the [Virtual Assembly](https://virtual-assembly.org) after trying out other technologies (JAVA / PHP) in the first versions of SemApps.


## SemApps technical architecture

### Server part

|Schéma|The server part is made up of two elements|
|----|----|
|![](/img/partie-serveur.png)|<br/>- The **database** (triplestore): which stores the semantic triplets.<br/>- The **middleware**: which receives requests from the user interface, retrieves the elements requested from the database and sends them back to the interface.

The server part is implemented in Javascript, mainly using two libraries: 
* [Node JS](https://nodejs.org/en/), which is the very popular technology for creating Javascript servers
* [Moleculer](https://moleculer.services/), which is a sort of scheduler that orchestrates all the 'services'.

### User interface
![](/img/interfaces-utilisatrices-semapps.png)

The Carto4CH demonstrator user interface is based on the **SemApps** toolbox, which itself depends on a technical foundation (framework) called [**React-Admin**](https://marmelab.com/react-admin/). This technical foundation is maintained by the company [**Marmelabs**](https://marmelab.com/fr/). It uses the JavaScript library [**React**](https://fr.legacy.reactjs.org/).

This library can be used to create an interface in a very short space of time, enabling you to : 
* **Create** data
* **Display** and list data
* **Modify** data
* **Delete** data

(see [**CRUD**](https://fr.wikipedia.org/wiki/CRUD))

We use it for **back-office** and **My-competence**.

## Reusable components
![](/img/composants-reutilisables.png)

### Introduction
The SemApps toolkit is based on reusable "atomic" components.

There are two types of component: 
* Components **for middleware**, for example to read from the database, to import data, to format data, etc. See [Semapps.org > Middleware](https://semapps.org/docs/middleware)
* those **for user interfaces**, e.g. menus, titles, lists, inputs, selection boxes, etc. See [Semapps.org > Frontend](https://semapps.org/docs/frontend)

This allows each component to be independently maintained, rather than a large block managed by a single organisation. The basic components can be enhanced as required and new ones proposed.

Each component has its own documentation, with code examples.

### Three cases for the frontend (React)
There are three possible scenarios: 
* either we use components from React JS
* or we use components from the React-Admin library, which are often derived from React JS, but supplemented by the Marmelab team to simplify their use.
* or we overload these components to meet the needs of SemApps.

### Access to components
The components are available on the **NPM repository**, and you can find their code on the [Virtual Assembly Github](https://github.com/assemblee-virtuelle/semapps): 
* For [frontend components](https://github.com/assemblee-virtuelle/semapps/tree/master/src/frontend/packages)
* For [middleware components](https://github.com/assemblee-virtuelle/semapps/tree/master/src/middleware/packages)

### Access to component documentation
To access the technical documentation for reusable components, visit https://semapps.org## Archipelago

## Archipelago

|||
|-|-|
|![](/img/archipelago.png)| The first SemApps implementation was the [Archpelago] project (https://github.com/assemblee-virtuelle/archipelago). Its objective is to map the archipelagos of the transition, by being installed on each archipelago.

This project contains : 
* SemApps middleware
* An interface based on React-Admin
* The **P.A.I.R** ontology

It is on this basis that we have built the **back-office** of the Carto4CH demonstrator.

## The P.A.I.R ontology

|||
|-|-|
|![](/img/ontologie-pair.jpg)|The [Virtual Assembly](https://virtual-assembly.org) has for several years been offering an ontology for mapping ecosystems of **P**Projects, **A**ctors, **I**ideas, **R**Resources, and also events.|

More information on the [Virtual Assembly Blog](https://www.virtual-assembly.org/ontologie-pair/)

For Carto4CH, we initially used this ontology, and gradually replaced it with **[HeCo ontology](https://portal.carto4ch.huma-num.fr/en/docs/technical-documentation/ontology)**, as SemApps is compatible with any Semantic Web ontology.

## Installing Archipelago locally

### Technical specifications for running Archipelago
* Internet access
* A hardware configuration with at least 4 GB of RAM
* Install [Node JS](https://nodejs.org/en)

:::caution
There seems to be an error when launching the frontend with version Node JS 18 (it works in version 16.19)
:::

### Readme Gitlab
To simply install Archipelago locally on your computer, follow the steps specified in the [Archipelago-Carto4CH Github](https://gitlab.huma-num.fr/4ch/archipelago-carto4ch).

**In a nutshell** :
Three steps: 
* Launch the database
* Launch the middleware
* Then finally the user interface

## Archipelago deployment

|||
|-|-|
|![](/img/deploiement-carto4ch.png)|The Archipelago deployment is quite simple, cause it use **Docker** and **Make**. Once Docker has been installed on a server, you can launch a working server in a single line. It will launch **traefik** (network, ports, https, certificates), **the triplestore**, **middleware** and **frontend**.|

## Using SemApps for the Carto 4CH demonstrator

We used SemApps bricks for the demonstrator.

To understand the demonstrator code, see the [demonstrator developer documentation](/docs/technical-documentation/demonstrator-manual)

## Internationalisation
To manage internationalisation (EN/FR), we based ourselves on the [React-Admin] documentation (https://marmelab.com/react-admin/Translation.html)

**In summary:**
* Update the ``.env.local`` file with ``REACT_APP_LANG=en``.
* Set up the **i18nProvider** (see official RA doc) 
* And fill in the files : 
    * frontend/src/config/messages (containing EN/FR translations)
    * Or the "translations" sections in the **index.js** files.
* Finally, use the **useTranslate** hook to use the translations in the desired code.

## To find out more about SemApps
The Virtual Assembly maintains a **training documentation** on its Github.
* See: [SemApps training](https://github.com/assemblee-virtuelle/semapps/wiki/Formation-SemApps-%28en-fran%C3%A7ais%29)
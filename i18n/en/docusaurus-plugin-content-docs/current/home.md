---
title: Home
sidebar_position: 0
---

# Home

## Introduction

We present below **the European 4CH project**, then **the Carto4CH initiative**, and finally **the leaders of this initiative**.

## European 4CH Project (CCCCH)
The 4CH project is a European study on the potential to create a **European Center of Competence on the Conservation of Cultural Heritage**. This study will last 3 years (2021-2023).
About twenty teams from different countries are involved in this project, including one from France, which is working on **a map of actors and competences in France** in this field.
* [Official website](https://4ch-project.eu)
* [Project page on the MSH Val de Loire website (in French)](https://www.msh-vdl.fr/projet/4ch-project/)

## Cartography of actors and competences (Carto4CH)
The objective of the Carto4CH initiative is to offer the 4CH community a **distributed semantic cartography** of actors and skills in French cultural heritage at first, which could be generalized at the European level in a second time.
Thus, each partner will have its own environment, keep its data, and choose to publish its actors and skills in the general mapping.
Each server uses a common technical base, containing standards and a **shared ontology**.
It is this base that we present on this portal.
* More information in the [Presentation of Carto4CH](/en/docs/project-documentation/presentation-generale)

## Carto4CH project leaders

|[Université de Tours](https://www.univ-tours.fr/)|[LIFAT](https://lifat.univ-tours.fr/)|[CNRS](https://www.cnrs.fr/fr)|[CITERES](http://citeres.univ-tours.fr/)|[MSH Val de Loire](https://www.msh-vdl.fr/)|
| -------- | -------- | ------ | ------ | ------ |
|![](/img/logo-UniversitedeTours.png)|![](/img/lifat.jpg)|![](/img/CNRS-Logo-fond-blanc.jpg)|![](/img/logoCITERES.png)|![](/img/logo_msh.jpg)|

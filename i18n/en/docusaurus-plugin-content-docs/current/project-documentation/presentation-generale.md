---
title: Presentation
sidebar_position: 1
pagination_prev: null
pagination_next: null
---

# Presentation

## Introduction
This presentation describes more precisely the Carto4CH project, but without going into the technical side.

## Download the presentation in PPT format (validated)

[Download the general presentation in PDF version 1.4](./4CH-Actors-and-competences-cartography-v1.4.pdf)

## Or access the general presentation online

:::caution
For the moment, since this presentation is still young and may be modified, we use a collaborative PAD to update it more easily.
The PAD can therefore be ahead of the above presentation in PDF format.
:::

* [Presentation link](https://pad.lescommuns.org/p/SlideCarto4CH-en)
* [Source link](https://pad.lescommuns.org/SlideCarto4CH-en)

(If you would like to contribute content, please contact us).

---
title: III. Attentes du terrain
sidebar_position: 3
---

# III. Les attentes du terrain

## Des questions récurrentes lors des entretiens
Pendant nos entretiens, nous avons posé certaines questions récurrentes :
* Qu'attendriez-vous de cette cartographie ?
* A quoi correspondent pour vous des compétences ?
* Comment l'imaginez-vous concrètement ?
* Connaissez-vous des référentiels ou des projets existants dans ce domaine ?
* De quelle manière souhaitez-vous participer au projet ?

Nous avons recueilli les réponses dans des documents partagés, puis les avons synthétisées dans ce livre blanc.

## Synthèse des entretiens
Globalement, les retours ont été **enthousiastes** sur le projet et il en ressort un besoin partagé d'une telle cartographie.
La majorité souhaite **être tenue au courant**, certain.e.s souhaitent **participer concrètement**, et une minorité travaillent déjà sur un sujet proche.
D'autres encore aimeraient participer, mais savent qu'il va être complexe de trouver de la disponibilité pour ce projet dans les mois qui viennent.

Voici quelques visions, attentes, idées, remarques, conseils, vécus qui ressortent des entretiens.

### "Le mode distribué semble essentiel"
Nous avons écouté avec attention des exemples vécus dans le passé de problématiques dues à des projets centralisés.
Lorsqu'une base de donnée a été mise en place et appartient "politiquement" à une organisation, cette dernière peut avoir du mal à accepter les demandes d'amélioration, les corrections ou l'ajout de nouvelles données.
Il peut exister des tensions entre certaines  institutions qui expliquent qu'elles ont du mal à partager un même serveur et à ouvrir leurs données respectives.
Certain.e.s ont déjà proposé des architectures distribuées par le passé, mais pas encore en utilisant des standards communs. En effet, on peut trouver des portails qui vont chercher leurs données dans diverses bases de données, mais en utilisant des transformations lourdes à maintenir.

### "Besoin de travailler ensemble"
Proposer des groupes de travail croisés entre différents domaines, sur la mutualisation des données peut s'avérer une bonne idée.
Le patrimoine a besoin de croiser les secteurs pour apporter de la richesse et de l'innovation.
La cartographie permettra de relier des personnes qui ne se connaissent pas, et de croiser des compétences sur des domaines différents pour des projets communs.

### Besoin d’une gestion du cycle de vie de la donnée / de la donnée dans le temps
Par exemple, lorsqu'on souhaitera représenter l'activité d'une personne par rapport à un projet, il faudra préciser la date de début et la date de fin. Idem pour un métier qui sera effectué au sein d'une organisation.
Il sera intéressant de prévoir des interfaces permettant de visualiser ces temporalités de différentes manières (frise chronologique, calendrier, diagrammes...)
Dans le temps, les libellés utilisées pour définir des compétences, des outils, des méthodes, des organisations, des objets patrimoniaux, peuvent changer dans le temps. Leurs significations peuvent changer, il faudra donc prévoir d'en avoir plusieurs sur des périodes différentes.

### Volonté commune d'utiliser des standards
Exemple dans le domaine de la 3D, où aucun nouveau standard n'est vraiment sorti, or les anciens standards ne tiennent pas dans la durée, à cause des nouveaux outils… et donc les formats se succèdent, et les réflexions communes ont du mal à aboutir.

### Intérêt de porter l'architecture dans d'autres projets européens
Certaines personnes interrogées ayant une expérience européenne, elles pensent que si ce projet fonctionne dans ce domaine, il serait portable dans d’autres projets européens (par exemple 3D).

### Passer par les acteurs pour trouver les compétences ?
Certain.e.s pensent que les acteurs sont le premier niveau intéressant de cette cartographie, et que les compétences, les métiers, les outils graviteront autour des acteurs à un second niveau.
Nous pourrons imaginer des interfaces permettant à l'utilisateur de choisir son axe de recherche. Soit il recherche un acteur pour trouver une compétence, soit il souhaite trouver tous les acteurs ayant une même compétence.

### Difficulté de trouver la granularité
Question : comment organiser dans la cartographie différentes granularités de données (par exemple : institutionnelle / terrain) ?
Réponse : A l'heure actuelle, nous ne le savons pas, et nous souhaitons justement intégrer dans l'expérimentation des éléments de différentes granularité (institution, consortium, musée, gestionnaire d'un monument, projet, laboratoire de recherche...), de manière autonome, pour ensuite voir les ponts que nous pourrons faire, en ajoutant ce qu'il faut dans l'ontologie pour que des liens se tissent entre ces informations, par nature très hétérogènes.

### Définir la cible de la cartographie
Question : Cette cartographie sera à destination de qui ? Des chercheurs ? Des amateurs ? Des institutions ? Des professionnels du patrimoine ?
Réponse : Ce sont les fournisseurs de données qui choisiront leur cible. Si des organisations souhaitent mettre à disposition des données, alors elles pourront le faire et choisir quelles applications pourront les utiliser. C'est le principe de base de [SOLID](https://pad.lescommuns.org/SlideSolid). Il en ira de même pour les personnes (chercheurs, membres d'institutions, d'associations, etc.), en accord avec l'organisation qui publiera leurs données (serveur de données). Nous souhaitons pour cela créer progressivement une gouvernance partagée par tous ceux qui possèdent un serveur de données, afin que le commun soit "piloté" par ceux qui publient des données. Il faudra peut-être aussi des personnes extérieures, neutres pour décider si tel ou tel partenaire est légitime de  proposer ses données, ou si certaines données entrent dans le périmètre du projet (besoin d'écrire une charte...). Mais à ce niveau là, tout sera à construire au fil du temps.

### Quel type de visualisation ?
Souhaite-t-on une cartographie géographique ? Un représentation en réseau ?
Encore une fois, la réponse est que tous les types de représentation seront possibles une fois que les données seront clairement sémantisées. Peut-être aussi que les besoins de visualisations amèneront à adapter l'ontologie, donc nous sommes à l'écoute de ces besoins aussi.

### Constat : aucun réseau d'acteurs et/ou d'organisations
En effet, il n'existe pas à l'heure actuelle de réseau de tous les acteurs dans le domaine du patrimoine.
Faudra-t-il intégrer des organisations du patrimoine, même si elles ne participent pas toutes à fournir des acteurs et des compétences ?

### Éviter des actions redondantes et proposer des réponses cohérentes
Que ce soit au niveau national ou plus tard au niveau européen, plus le réseau des acteurs et de leurs compétences sera identifié, plus les liens se feront et moins les projets seront redondants. Cela permettra d'avoir des réponses plus cohérentes sur certaines problématiques communes, grâce à la confrontation des spécialistes entre eux.

### Trouver les relais nationaux
Il semble important de trouver des organisations qui pourront relayer ce projet, afin qu'il soit connu des principales organisation du patrimoine français. La gouvernance partagée de la cartographie en dépend.

### Comment trouver des compétences ?
C'est une des questions que nous posons à chaque entretien, et les réponses sont diverses :
  - Le bouche à oreille
  - Se rapprocher des universités
  - Demander à des spécialistes reconnus dans certains domaines
  - Sur le web, via un moteur de recherche
  - Sur le web social, par exemple sur linkedIn, des personnes peuvent déclarer des compétences et les faire valider par des pairs, ou déclarer des certificats d'étude
  - Idem sur des sites de compétences comme par exemple [le site des expertes](https://expertes.fr/)...

### Compétences rares
Certaines compétences en France sont rares, par exemple, il y a peu de restaurateurs.

### Homonymie de compétences
Un bon exemple de compétence qui, pour un même nom, possède deux significations que le contexte permet de distinguer : **restaurateur** (en œuvre d'art ou en cuisine...)

### Compétences croisées
Difficultés parfois à trouver des compétences croisées dans plusieurs domaines. Par exemple, il y a des spécialistes des dorures, et des spécialistes des tableaux, mais peu de spécialistes sur l'utilisation des dorures dans les peintures. Il faut donc faire se rencontrer des spécialistes entre eux pour faire émerger une nouvelle compétence mixte.

### Cartographier les lieux
Dans le patrimoine, certaines personnes ou organisation ont des connaissances poussées dans un secteur géographique précis, ou à propos d'une ville en particulier, ou encore pour une langue. Il faudra donc associer les compétences à des lieux ou à des régions du globe.

### Deux axes de recherche, par personne ou par organisation...
Dans certains cas, les compétences recherchées sont précises, isolées, non liées à une organisation mais plutôt à une personne ayant des expériences. Dans certaines organisations, les compétences sont attachées à la personne, et suivent la personne lorsqu'elle quitte l'organisation.
Mais dans d'autres cas, une organisation va être spécialisée dans un domaine et on va pouvoir s'appuyer sur elle pour trouver régulièrement les spécialistes d'un domaine.

### Des listes de compétences existantes, mais non accessibles
Dans de nombreux projets, des analyses de compétence ont déjà été réalisées, mais sont stockées dans des documents bureautiques, non accessibles à des applications externes sans l'aide de l'humain (ou sont la propriété de l'entreprise pour laquelle a été fait le projet). Des regroupements de compétences ont été réfléchis, en réunion, à l'intérieur des projets, mais ces derniers ne sont pas publiés ni confrontés à d'autres environnements extérieurs au projet.
Dans Carto4CH, les données seront accessibles **en open data ++** ([5 étoiles](https://5stardata.info/fr)), dans un format standard pour être facilement réutilisables par des sites externes.

### Des référentiels existants, mais non maintenus
Certain.e.s ont déjà connu des projets de référencement de compétence qui ont échoués par le passé. Utilisés à leur sortie, ils pêchent souvent par le fait qu'ils ne sont pas entretenus dans le temps.
Il faut donc que les processus de mise à jour de la cartographie soient bien ancrés dans ceux des partenaires qui participeront. Qu'ils entrent dans un cycle de vie de la donnée maintenu dans le temps.
Il faut aussi que les partenaires aient un intérêt constant à mettre à jour ces données.
Par exemple, si les compétences dans LinkedIn sont assez bien maintenues, c'est lié à de nombreux facteurs : l'utilisation massive du site, des fonctionnalités utiles, le fait que ce soit aussi un réseau social, une place de recrutement, qui peut générer des affaires, etc.
Pour notre cartographie, nous misons sur le côté "interopérable" pour qu'elle puisse être facilement mise en relation avec les autres briques du SI des partenaires et ainsi leur servir en plus de servir à d'autres publics.
Certains proposent d'obliger les personnes à remplir leur "profil" si elles veulent participer à un projet, cela se fait couramment de la part des éditeurs scientifiques par exemple, lorsqu'un chercheur ou une chercheuse veut soumettre un article. Mais c'est une tendance qui multiplie les saisies pour les personnes (qui ne mettent guère à jour ces multiples déclarations).  
parenthèse de Béa : je n'ai pas compris cette partie "en conditionnant cette action dans d'autres projets périphériques où ces compétences sont nécessaires."

### Compétences ou métier ?
Exemple: un conservateur n'aura pas les mêmes actions / responsabilités dans un musée ou au Louvre...
Faut-il pour autant opposer les deux ? Les métiers ne seraient-ils pas une partie des compétences ?

### Comment filtrer les longues listes des référentiels de compétences existants ?
Certains projets ont besoin uniquement d'une petite quantité de métiers ou de compétences qu'ils ont déjà identifiés, qu'ils rencontrent tous les jours. Si nous pointons sur un référentiel de compétences très général, il y aura un travail de filtre à réaliser pour n'en conserver que certaines.
Peut-être aussi que d'autres ne seront même pas dans la liste. Il faudra donc pouvoir donner la possibilité aux utilisateurs d'ajouter des compétences dans les listes/référentiels, avec un moteur de désambiguïsation.
S'inspirer de ce que permet Wikidata pour les saisies ?

### Pouvoir découvrir des acteurs que l'on ne connaît pas
Lorsqu'on souhaite trouver des spécialistes d'un domaine, on part souvent de notre carnet d'adresses, de ceux et celles qu'on connaît déjà. Grâce à la cartographie, nous pourrons laisser la liberté à des personnes "inconnues" du réseau d'ajouter leurs compétences, et cela permettra au réseau de s'enrichir "par le bas". Pour cela, il faudra encore une fois désambiguïser les termes utilisés, pour ne pas bloquer la saisie, ou apporter du bruit dans la cartographie. Cette partie demande à être précisée : qui fournira le serveur hébergeant ces données-là et selon quelle gouvernance de ces données.

### Comment gérer les sollicitations futures des acteurs ?
Il est possible que suite à la publication des compétences, certaines personnes gagnent en popularité et deviennent très/trop sollicitées. Il faudra donc gérer collectivement une charte de bonnes pratiques, proposer différentes manières dont on souhaite être mis en relation avec les acteurs proposés dans la cartographie, et inversement ces acteurs pourront indiquer à quel type d'applications (et donc de public) ils permettent d'exposer les données les concernant.

### Attention au mot "expert.e"
En effet, on peut se dire "expert.e" d'un domaine, mais cela reste subjectif si ça n'est pas assez précis et contextualisé.
Comme explique le [Rapport de France Stratégie sur la notion d’expertise](https://www.strategie.gouv.fr/publications/lexpertise-face-une-crise-de-confiance)… les experts n’existent pas, mais il y a des **“situations d’expertise”**, qui peuvent changer dans le temps.
Déjà, dans le temps cela peut varier, et de nouveaux experts, dans le même domaine peuvent apparaître et modifier la crédibilité de cette expertise.
Parfois, des personnes peuvent se dire expertes alors qu'elles ont fait appel à un prestataire pour effectuer techniquement le projet. Dans ce cas, il faudrait trouver un intitulé expliquant que ces personnes sont montées en compétence dans la gestion d'un projet, par l'intermédiaire de quelqu'un d'autre, et trouver les compétences précises qu'elles ont développées dans le suivi de cette prestation.
Pour contextualiser, il nous faudra relier les compétences à des faits concrets, comme des formations, des publications, des fonctions exercées (métiers), des outils, des projets. Tout cela viendra "valider" la compétence et lui ajoutera du crédit.
Nous nous contenterons donc de mettre en lien des faits. Par exemple, unetelle a publié l'article XXX, untel a participé au projet XXX, de telle date à telle date, ou encore unetelle a validé un diplôme. Ce sont des faits sur lesquels il est plus difficile poser des jugements de valeur.

### Trop d'expert.e.s tue l'expert.e...
On peut aussi imaginer que dans certains domaines, il y ait trop d'experts. Par exemple, si on propose le métier de "chef de projet", de nombreuses personnes peuvent le revendiquer. Du coup, lorsqu'on en cherche un.e, on tombe sur une longue liste, qu'il faut pouvoir trier en fonction d'autres paramètres (formation, expérience, domaine, projets...).

### Relation avec le terrain
Il semble important de différencier des compétences "potentielles" par rapport à celles qui sont concrètement utilisées sur le terrain, dans des projets vivants, récents. Une compétence a d'autant plus de valeur lorsqu'elle est utilisée dans la société.

### Un réseau social des compétences ?
Certain.e.s imaginent que la cartographie puisse ressembler à un **linkedIn du patrimoine**, avec une mise en relief des compétences par rapport à l'aspect réseau social. C'est une bonne idée de se permettre l'inter-validation des compétences. Encore une fois, ça permet de valoriser la compétence.
Nous ne savons pas encore quelle forme prendra notre cartographie, mais elle s'inspirera de mécanismes de ce genre.

### Utilisation de technologies de traitement automatique des données et des connaissances (big data / IA) ?
En effet, une fois que plusieurs serveurs de données seront mis en réseau dans la cartographie, nous aurons certainement besoin de technologies plus puissantes pour rechercher, interroger ou pour présenter intelligemment les données.
Possibilité de mémoriser les recherches des utilisateurs, pour proposer des recommandations liées à un secteur spécifique.

### Réutiliser les mots clés connus du domaine
En effet, certains colloques, ou journaux scientifiques, gardent une stabilité dans la liste de mots clé, cela peut aider pour repérer des mot-clés.
Il est fort probable que nous partions de référentiels existants pour la majorité des métadonnées utilisées dans la cartographie. Par exemple, nous utiliserons des référentiels extérieurs de compétences, de métiers, de secteurs, d'objets du patrimoine ou d'outils. En revanche, ce sera l'agrégation de tous ces éléments avec les acteurs qui viendra constituer notre cartographie.

### Penser en amont aux identifiants
Générer ou utiliser des identifiants sera l'un des défis techniques de cette cartographie. Il existe déjà beaucoup d'identifiants (pour les acteurs, et pour d'autres concepts), et il faut les utiliser.
Inversement, il faudra que les identifiants que nous générerons soient pérennes dans le temps et publiés selon les [principes FAIR](https://fr.wikipedia.org/wiki/Fair_data). Nous aurons donc besoin de bien échanger avec nos partenaires sur ce sujet, pour faire les bons choix, car c'est toujours complexe de les changer plus tard.

### Attention aux aspects juridiques
Attention à prendre en compte l’aspect juridique, le code du patrimoine… Trouver le bon compromis.
Dans ce cadre, l'apport du projet 4CH, peut justement être de trouver des solutions à l'échelle européenne, pour des problèmes qu'il serait difficile de résoudre à l'échelle locale. RGPD, licences, etc. On peut s’appuyer sur des compétences de certains pays… Pour faciliter les travaux au niveau terrain.

### Attention à la traduction de certains référentiels en français
Par exemple, dans ESCO, certaines classifications ne correspondent pas à ce que nous attendons. Là encore, il y a besoin de rassembler certains spécialistes de différents domaines pour valider les classifications qui seront retenues.

### S'appuyer sur les référentiels RH ?
La notion de compétence est surtout développée et maintenue par les environnements de gestion des ressources humaines. Il y a par exemple le code ROME. On peut y trouver des choses intéressantes, mais pas forcément au format attendu.

### Difficulté pour certains partenaires d'avoir accès à un serveur
Par exemple, aux archives nationales (service à compétence nationale), il faut passer par le service SNUM pour avoir un serveur, et ça peut prendre du temps. C'est différent à la BnF qui est un établissement public.

### Attention à la représentation des minorités et à la mixité
D'une manière générale, et ce tout au long du projet, il faudra vérifier que les minorités et la mixité soient bien représentées. Que ce soit au niveau des personnes ou des organisations qui pourront contribuer à la cartographie ou bien s'en servir.

---
title: Mockups
sidebar_position: 5
pagination_next: null
---

# Mockups

![](/img/mockups-baner.png)

## Introduction
For the Carto4CH project, we developed several applications.

Before developing each application, we created a mock-up, allowing us to check if the whole team agreed on the expected functionalities. 

To develop these mock-ups, we used the software [Figma](https://figma.com). It has the advantage of being able to "simulate" the actions of the users.

We will put on this page the list of the models used, with for each one:
* a cuteness picture
* its title
* its description 
* a link to the Figma project, 
* a link to its execution (simulation),
* the date of the last update
* a link to the application once developed.

These mock-ups can also be used by technical teams who would like to produce their own applications, using the same functionalities.

## Mockups list

| Picture | Title | Description | Project | Simulation | Last update | Application |
| -------- | ----- | -------- | ------- | ------- | ------- | ------- |
| ![](/img/cuteness-mockup-my-competences.png) | My-competences | Mockup simulating My-competences application. | [Projet](https://www.figma.com/file/oG1AsFQIYymkre7q6d1DOg/Maquette-My-competence?t=N349UaJ82gxFxemD-0) | [Simulation](https://www.figma.com/proto/oG1AsFQIYymkre7q6d1DOg/Maquette-My-competence?scaling=min-zoom&page-id=0%3A1&starting-point-node-id=1%3A2) | 20/01/2023 | [Application](https://my-a.carto4ch.huma-num.fr/) |
| ![](/img/cuteness-mockup-competences.png) | Competence entry | Mockup simulating the way of editing cmpetences in the back-office. | [Projet](https://www.figma.com/file/jeB2DSHDpXgEo8CIdGApwJ/Maquette-choix-des-comp%C3%A9tences?t=GcsgEBqOOb0vtVC2-0) | [Simulation](https://www.figma.com/proto/jeB2DSHDpXgEo8CIdGApwJ/Maquette-choix-des-comp%C3%A9tences?node-id=1%3A2&starting-point-node-id=1%3A2) | 02/06/2022 | [Application](https://semapps-a.carto4ch.huma-num.fr/) |
| ![](/img/cuteness-mockup-portal.png) | Portail Carto4ch | Mockup of the portal | [Projet](https://www.figma.com/file/8sAcBID9EwBzZZ0fh0fDYE/Maquette-du-portail-Carto-4CH?t=N349UaJ82gxFxemD-0) | [Simulation](https://www.figma.com/proto/8sAcBID9EwBzZZ0fh0fDYE/Maquette-du-portail-Carto-4CH?scaling=min-zoom&page-id=0%3A1&starting-point-node-id=10%3A61&node-id=10%3A61) | 02/12/2022 | [Application](http://portal.carto4ch.huma-num.fr/) |


(If you wish to participate in the update of these mockups, please contact us).

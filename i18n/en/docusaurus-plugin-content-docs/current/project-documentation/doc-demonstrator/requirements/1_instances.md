---
title: Carto4CH instances
sidebar_position: 1
---

# Carto4CH instances 

## General
:::info
**An "instance "** = a data server for a partner organisation, together with its various specific interfaces for managing the instance's data.
:::

In the demonstrator, for example, we simulated two instances: A and B.

In the reality of the project in production, there could be many instances, belonging to many organisations.

The diagram of the demonstrator shows this clearly:

![](/img/schema-demonstrator-A-B.png)

In the diagram above, you can see that each instance has : 
* its own **data** (A on one side and B on the other)
* several **user applications** (My-competence, Back-office or Carto).

Each instance is technically and politically autonomous, but this does not prevent it from being linked to the others via the mapping interface. This is the advantage of using standards for sharing information between servers.

## Customisation of each instance
In production, the administrators of future instances will be able to customise them with : 
* a colour
* a logo
* a title
* specific parameters (for example, its links with another instance in terms of interoperability)

to give it a unique look.
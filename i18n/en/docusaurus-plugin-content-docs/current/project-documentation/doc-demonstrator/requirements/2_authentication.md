---
title: Authentication
sidebar_position: 2
---

# Authentication

![](/img/authentification-menu-my-en.png)

## General
In this section we will explain the concepts of "user profile", "connection", "authentication" and "permissions".
These concepts are valid in both applications, the only difference being that users currently only have access to their profile in the back office.

## Connection choices
**By default**, you are not logged in. Before you can access the application, you need to create a user account.

| In the top right-hand menu | Screen images |
| -------- | -------- |
| **On the back office** If you wish to create or modify data, click on the profile icon in the top right-hand corner, where you can either register, log in or log out, or view and manage your personal data. |![](/img/authentification-menu-bo-en.png)|
| **On My-competence** The application asks you to log in directly. However, it does not allow you to manage your profile. This function is only available in the back office.|![](/img/authentification-menu-my-en.png)|

## Authentication

| Authentication Server | Screen images |
| -------- | -------- |
|**On the back office** If this is your first time, choose **"Register "**, to register by creating your user on the authentication server. If you have already created your account on the authentication server, and you have clicked on "Connect", **follow the explanation screen**.|![](/img/authentification-sso-bo-en.png)|
|**On My-competence** If it's your first time, the application will send you an invitation to create an account.|![](/img/authentification-sso-my-en.png)|

## Registration
In both applications, if you are registering for the first time, you will be taken to an interface where you can register (orange button...).

This exemple is a OIDC french server (using [Keycloak software](https://www.keycloak.org/)).

![](/img/auth-inscription-sso.png)

:::warning
Please remember the e-mail address and password you use.
:::

| Connected | Screen images |
| -------- | -------- |
|**On the backOffice** Once you have logged in, you will be taken to the home page or to your Carto4CH profile. You then have the option **to view and edit your profile**.|![](/img/authentification-menu-bo-en.png)|
|**MyCompetence** does not currently manage the user's profile||

## User profil managment

:::danger
**Warning:**
As mentioned above, this section is only available in the **back-office**. There is no user profile in My-competence at the moment.
:::

On the back office, if you click on **"View my profile "**, you will be taken to an interface showing the few details provided during authentication:

![](/img/auth-user-profil-en.png)

You have access to 3 buttons to the right of your surname/first name.

| Button | Screen images |
| -------- | -------- |
| The **List** button takes you back to the list of people. |![](/img/auth-list-person-en.png)|
| The **Edit** button is used to switch to add/modify your personal data. The **Show** button returns you to the profile screen. |![](/img/auth-edit-profil-en.png)|
| The **Permission** button allows you to manage your data access rights.|![](/img/auth-permission-en.png)|

## Permissions managment

Applications make it possible to manage access rights to information in a fine-tuned way.
Currently, the first rule is that **whoever creates an object has full rights to it**. The others only have read-only rights.

For exemple, here the permissions of an organization : 

![](/img/auth-permissions-users-en.png)

We can then add rights to this resource to other people.

![](/img/auth-add-user-permission-en.png)

**For example:** Yannick creates the "FSP" object, but makes a mistake in the definition. Olivier only has read-only rights to it, and asks Yannick for modification rights.

![](/img/auth-permission-by-user-en.png)

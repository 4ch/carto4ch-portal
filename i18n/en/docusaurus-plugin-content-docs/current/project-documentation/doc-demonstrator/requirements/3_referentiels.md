---
title: Vocabularies
sidebar_position: 3
---

# Vocabularies

![](/img/referentiels-banner-en.png)

## Introduction
When editing forms, you will have to select data from lists.

If you don't find what you're looking for in these lists, you can add new values. 

This is what we call "repositories" in this project.

The way you do this differs depending on whether you are using the **back-office**, or **My-competences**.

## Managing a vocabulary in the back office
### Via the menu
In the **Back-office**, repositories can be enhanced via the menu.
```
Menu > Vocabularies > discipline(s)
                    > sector(s)
                    > Studied object(s)
                    > Tool(s)
                    > Occupation(s)
```

The list of objects you create here will correspond to the list displayed in the competences.

![](/img/referentiels-competence-en.png)

### Via the input field choice lists
At the end of each list, you can "Create" a new value in the list.

![](/img/referentiels-competence-creer-en.png)

If you click on the 'Create' line, a popup window appears, giving you the option of adding a value and selecting it.

![](/img/referentiels-competence-creer-popup-en.png)

Enter a new value

![](/img/referentiels-competence-creer-popup-edit-en.png)

![](/img/referentiels-competence-creer-popup-edit-new-en.png)

And by pressing the "Add" button, this creates the new value (here in the "Discipline(s)" vocabulary)

![](/img/referentiels-competence-creer-popup-edit-new-input-en.png)

You can then find this data in the list of disciplines using the menu.

## Managing vocabularies in My-competences

### Introduction
Please note **these vocabularies are shared**. This data can be used by other people.

For example: 
* when creating or editing a job, you can add a new organisation or a new occupation.
* when creating or editing a competence, you can add a discipline. 

### Via the choice lists in the input fields

In My-competences, you can only add new values by clicking on "Create" at the bottom of the choice lists.
This simplifies data entry for the user, which is the main objective of this application compared with the back office.
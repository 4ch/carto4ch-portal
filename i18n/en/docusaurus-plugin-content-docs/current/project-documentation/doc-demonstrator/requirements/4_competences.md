---
title: Competences & qualities
sidebar_position: 4
---

# Competences & qualities

:::info
See also [Requirements about vocabularies](/docs/project-documentation/doc-demonstrator/requirements/referentiels)
:::

## Introduction

We propose to describe a competence in terms of different facets, and the four "qualities", or "types of facet", that we propose have been chosen for the professional context of cultural heritage. 

A competence is described by at least one facet, which comes under one of the following four qualities:
* The quality **"Discipline"**, or academic field, is useful for indicating the knowledge linked to the competence we wish to describe.
* The **"Sector"** quality, or sector of activity, or field of application, is used to specify the context in which the competence is applied. This framework is a form of complementary knowledge.
* The quality **"Studied object"**, can be used to be even more precise about the field of knowledge of the competence.
* The **"Tool"** quality is used to indicate any technique, method or tool of which the competency denotes mastery.

Furthermore, skills are linked to a context. For example, if you have worked as an IT project manager in the heritage sector, this is different from having trained in IT in the heritage sector. The Discipline quality with the value IT will be useful for both skills. The same skill can be described by zero or more diciplines, zero or more sectors of application, zero or more objects of study, zero or more tools.

The table below shows two competences as examples:

| Competences | Qualities | Exemple |
| -------- | -------- | -------- |
| Competence C1 | Discipline D1, Discipline D2 | Project manager, Informatique
| Competence C1 | Sector S1 | Museums
| Competence C1 | Studied object OE1 | Gilding in the 12th century
| Competence C1 | Tool T1, tool T2 | Office Suite, Wordpress
| Competence C2 | Discipline D2, Discipline D3 | Informatique, Database
| Competence C2 | Sector S1, Sector S2 | Museums, Archives
| Competence C2 | No studied Object | 
| Competence C2 | Tool T2, tool T3, tool T4 | Wordpress, PHP, PostgreSQL

![](/img/competences-graphviz.png)

## List of qualities & definitions

Definitions are extrated from [Ontology Heco document](https://portal.carto4ch.huma-num.fr/docs/technical-documentation/ontology)

| Qualités (FR) | Quality (EN) | Définitions|
| -------- | -------- | -------- |
| Discipline | Discipline | For all academic and learned disciplines. These disciplines can be more or less specific, and are traditionally organised into hierarchies. **Example:** "Computer science" is more general than "Semantic Web". |
| Secteur | Area | Comes from the need to distinguish heritage sectors. We see this as a domain of application (sector of activity). **Example:** "Computer science" is more general than "Semantic Web". |
| Objet d'étude | Studied Object | Is an object of study, more or less precise. But it is not a single real entity, like a particular painting, this notion is out of scope for the moment. **Example:** the Middle Ages, or ceramics produced in geographical area X in period Y...
| Outil | Tool | Is to indicate mastery of any technical, methodological or conceptual tool. **Example:** Protégé, PostrgreSQL, 3D Scanner, Data Management Plan...
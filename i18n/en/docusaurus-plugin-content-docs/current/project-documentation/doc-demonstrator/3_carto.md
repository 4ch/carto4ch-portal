---
title: Carto-competences user manual
sidebar_position: 4
---

# Documentation of the carto-competences

## Introduction

Carto-competences is a read-only interface, displaying competences in the form of a **network graph**.

## Access to user documentation

To understand this interface, go to the [**Visualisation page**](/docs/technical-documentation/visualisation).
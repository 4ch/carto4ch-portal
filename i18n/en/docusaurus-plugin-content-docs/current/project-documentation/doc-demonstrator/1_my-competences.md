---
title: My-competences user manual
sidebar_position: 2
---

# My-competences user manual

![](/img/my-banner-en.png)

## Introduction

My-competence is a user interface allowing people to enter their competences and only see their competences (unlike the back office, in which everyone sees everything).

## Version

This documentation carries that of the demonstrator: **V1.0**

## Prerequisites
To make it easier for you to read what follows, together we will review certain concepts (instances, authentication, repositories, etc.).

To do this, please read the [User manual prerequisites](/docs/category/requirements-category-label-2)

## Job management

### Presentation
It is possible to manage (create, edit and delete) **jobs**. By “jobs” we mean professional experience.

These professional experiences have given you competences that you will be able to use later.

### Creation of jobs
At first, the job list is empty.
Click the “Create” button to the right of the word “jobs”.

You can then complete the form for the job of your choice.

![](/img/my-job-create-en.png)

you can specify:

* the **title** (what you would write on your CV...)
* the **short description**
* the **type of contract** (CDD, CDI, Independent, etc.)
* the **employer**
* the **occupation**
* the **competence** that this job has given rise to
* the **description**
* the **starting date**
* the **end date**
* A list of themes that can help us filter later in the cartography.

For example :

![](/img/my-job-create-example-en.png)

### Job cards
Once completed, new “job card” appear in the global My-competences list.

![](/img/my-job-list-en.png)

When you click on it, it displays the content of the post.

![](/img/my-job-show-en.png)

### Editing a job
You can then return to "edit" mode or return to the list via the buttons at the top right.

### Deletion of a job
Deleting a job is possible by using edit mode and clicking on the "Delete" button at the bottom right.

## Commitments management

### Introduction
It is possible to manage (create, edit and delete) commitments. 

By "commitments" we mean participation in associations or various projects without specific financial remuneration, and without professional contract. 
Projects in which one participates as part of a paid job can also be declared as contributions, without there being a direct link with the job (if specified, the dates may show this link).

These commitments have given you competences that you will be able to use later.

### Creating a commitment
At first, the commitment list is empty.
Click the “Create” button to the right of the word "commitment".

You can then complete the form for the commitment of your choice.

![](/img/my-commitment-create-en.png)

you can specify:
* the **title** of the commitment (as you would on a CV)
* the **short description**
* a **description**
* the **project** to which you contributed by carrying out this activity (there may be several)
* the **competences** that this activity revealed in you
* the **starting date**
* the **end date**
* A list of **themes** that can help us filter later in the cartography.

For example :

![](/img/my-commitment-create-example-en.png)

### Commitment card
Once completed, the new “commitment card” appear in the global My-competences list.

![](/img/my-commitment-list-en.png)

When you click on it, it displays the content of the commitment.

![](/img/my-commitment-show-en.png)

### Commitment édition
You can then return to "edit" mode or return to the list via the buttons at the top right.

### Deleting a commitment
Deleting a commitment is possible by using edit mode and clicking on the "Delete" button at the bottom right.

## Training management

### Introduction
It is possible to manage (create, edit and delete) **training courses**.
By “training”, we mean lessons that you have followed:
* during your studies
* during your professional experience
* during your commitment
* by yourself

These lessons have given you competences that you will be able to use.

### Training creation
At first, the training list is empty.
Click on the “Create” button to the right of the word “training(s)”.

You can then complete the form for the training of your choice.

![](/img/my-training-create-en.png)

You can specify:
* the **title** of your training
* a **short description**
* a **description** of the training content
* the **organization** where you trained
* the **competences** acquired during this training
* the **starting date**
* the **end date**
* A list of **themes** that can help us filter later in the mapping.

For example :

![](/img/my-training-create-example-en.png)

### Training cards
Once completed, the new “training card” appear in the global My-competences list.

![](/img/my-training-list-en.png)

When you click on it, it displays the content of the training.

![](/img/my-training-show-en.png)

### Editing a training
You can then return to "edit" mode or return to the list via the buttons at the top right.

### Deleting a training
Deleting a training is possible by using edit mode and clicking on the "Delete" button at the bottom right.

## Competences management

### Introduction
Finally, following the completion of your CV, we will be able to move on to entering the competences.
These competences fall within the scope of cultural heritage.
We therefore proposed to structure these competences according to "qualities" linked to the Heritage environment.

:::info
First of all, read the [Prerequisites on competences and qualities](/docs/project-documentation/doc-demonstrator/requirements/competences)
:::

### Competence creation
At the start, the skills list is empty.
Click the “Create” button to the right of the word “Skills.”

You can then complete the form for the skill of your choice.

![](/img/my-competence-create-en.png)

You can specify:
* a **description** (to provide context for this skill)
* zero, one or more **disciplines**
* zero, one or more **sectors**
* zero, one or more **objects of study**
* zero, one or more **technical tools**
* **jobs** having provided this competence
* **commitment** having provided this competence
* **training** having provided this competence

For example :

![](/img/my-competence-create-example-en.png)

### Competence card
Once completed, the new "competence card" appear in the global My-competences list.

![](/img/my-competence-list-en.png)

When you click on it, it displays the content of the training.

![](/img/my-competence-show-en.png)

### Editing a competence
You can then return to "edit" mode or return to the list via the buttons at the top right.

### Removing a competence
Deleting a training course is possible by using edit mode and clicking on the "Delete" button at the bottom right.

## CV viewing

:::info
Once your data has been entered, you can use the back office application to view your CV (and fill in your personal data...).
:::

## Conclusion

Thank you for following this manual, we hope everything went well and that you are satisfied with this My-competences application.

The data collected is now structured in the database in accordance with the HeCo ontology.
They can therefore be queried by a cartography and visualization tool, or by any new specific query interface, in addition to the Back-office and My-competences.

For any comments, suggestions, ideas, [contact us](/docs/contacts)
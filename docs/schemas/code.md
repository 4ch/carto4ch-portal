---
title: Code
sidebar_position: 5
---

# Code

## Introduction

Les schémas ci-dessous sont à destination des développeurs souhaitant reprendre le code du démonstrateur. Ils expliquent des fonctionnements du code du démonstrateur.

## Accès aux Schémas

| Libellé| Image |
| -------- | -------- |
| Ce schéma explique le fonctionnement des inputs "auto-complete" [Schéma améliorable collectivement sous Figma](https://www.figma.com/file/quEMQ6L26UgKl9g1jUIwWJ/Fonctionnement-des-inputs-(auto-complete)?type=design&node-id=102-119&mode=design&t=6b7q2mhZ8RQChDNb-0)|![](/img/inputs.png)|
| Ce schéma (adressé aux développeurs) explique comment sont articulés les fichiers de code entre SemApps et Archipelago [Schéma améliorable collectivement sous Figma](https://www.figma.com/file/Y6qT1hcf9CciSUl1voeTPX/archipelago-et-yarn-links)|![](/img/schema-archipelago.png)|
| Relations réifiées pour les compétences (abandonné) |![](/img/reified-relations-competences.png)|
| Relations réifiées pour les rôles (abandonné) |![](/img/reified-relations-roles.png)|

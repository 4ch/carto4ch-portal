---
title: Mode distribué
sidebar_position: 4
pagination_prev: null
---

# Mode distribué

## Introduction

Les schémas ci-dessous expliquent ce qu'on entend par **"mode distribué"**.
* Les données sont stockées sur des serveurs décentralisés
* La cartographie générale (ou "Web application") va les rechercher dans ces serveurs pour les afficher

## Accès aux Schémas

| Document de travail | Image |
| -------- | -------- |
| Ici un schéma très simple pour ne pas se perdre dans les détails. [Schéma améliorable collectivement sous Figma](https://www.figma.com/file/PbX8O3WIMqahZgu6rV3UCZ/Distribu%C3%A9-couleurs-Carto4CH?node-id=102%3A119)|![](/img/schema-distribue-carte.png)|
| Ici, un schéma un peu plus précis [schéma améliorable collectivement sous Figma](https://www.figma.com/file/aPrHSKEigcKfcrVX1nXycy/4CH---Carto-distribu%C3%A9e-simplifi%C3%A9e?type=design&node-id=3-2&t=6sjNN1vqycYJCozp-0). |![](/img/schema-distribue-simplifie.png)|
| Pour voir une "vue globale" de l'architecture du projet Carto4CH, voir ce [schéma améliorable collectivement sous Figma](https://www.figma.com/file/K8eaErNiTpfZOn0LvfzQbq/Untitled?node-id=3%3A2). |![](/img/schema-distribue.png)|
| Pour mieux comprendre les différents modes d'hébergement de données, les notions de "décentralisation" ou de "distribution", voir ce [schéma améliorable collectivement sous Figma](https://www.figma.com/file/Z81KZxM6xYsVQLeZnZxVCB/Organisation-de-l'h%C3%A9bergement-et-des-donn%C3%A9es-Semapps?node-id=0%3A1). |![](/img/Organisation-hebergement-donnees.png)|
| Pour comprendre le mécanisme d'interconnexion de données dans SemApps. Voir le [schéma améliorable collectivement sous Figma](https://www.figma.com/file/3f74se3w8LaDmnulEMNQIx/Interconnexion-des-donn%C3%A9es-SemApps). |![](/img/Interco-msh-masa.png)|
| Pour comprendre le mécanisme de miroir dans SemApps, permettant une recherche fédérée, voir ce [schéma améliorable collectivement sous Figma](https://www.figma.com/file/BO2p508bSmQU1nqQSXj2eC/Recherche-f%C3%A9d%C3%A9r%C3%A9e---Mirroring-SemApps). |![](/img/mirroring-msh-masa.png)|

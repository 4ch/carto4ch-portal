---
title: Infrastructure
sidebar_position: 3
---

# Infrastructure

## Introduction

Les schémas ci-dessous expliqueront comment sont hébergés les serveurs déployés pour le projet.

## Accès aux Schémas

| Document de travail| Image |
| -------- | -------- |
| Ici un schéma détaillant l'infrastructure docker chez un partenaire. [Schéma améliorable collectivement sous Figma](https://www.figma.com/file/8wGJep8oXW86iPwoHQvupZ/Architecture-de-d%C3%A9ploiement-d'un-serveur-Carto4CH?type=design&node-id=1-2&mode=design&t=99ZfYuRT280Unbj0-0)|![](/img/infra-deploy.png)|
| Ici un schéma détaillant l'infrastructure suite au déménagement de l'instance A sur le serveur Huma-Num. [Schéma améliorable collectivement sous Figma](https://www.figma.com/file/gTg62KjoVldTm1gIzqiWLa/Architecture-du-Serveur-HN-carto4ch)|![](/img/Infra-server-carto4ch.png)|
| Ici un schéma détaillant l'infrastructure présente en 2022 sur 2 serveurs. [Schéma améliorable collectivement sous Figma](https://www.figma.com/file/CnOjxQhAKCBN3fOFPXm1Lj/D%C3%A9ploiement-des-serveurs-Carto4CH?node-id=0%3A1)|![](/img/infra-2-servers.png)|

---
title: Ontologie
sidebar_position: 2
---

# Ontologie HeCo

## Introduction

Les schémas ci-dessous représentent l'évolution de l'[ontologie HeCo](../technical-documentation/ontology).

## Schémas

| Version| Image (clic droit pour visualiser)|
| -------- | -------- |
| Ontologie HeCo version 1.0|![](/img/OntologyHeco-v1.0.svg)|
| Ontologie HeCo version 0.9|![](/img/OntologyHeco-v0.9.png)|
| Ontologie HeCo version 0.8|![](/img/OntologyHeco-v0.8.png)|
| Ontologie HeCo version 0.7|![](/img/OntologyHeco-v0.7.png)|
| Ontologie HeCo version 0.6|![](/img/OntologyHeco-v0.6.png)|
| Ontologie HeCo version 0.5|![](/img/OntologyHeco-v0.5.png)|
| Ontologie HeCo version 0.4|![](/img/OntologyHeco-v0.4.png)|
| Ontologie HeCo version 0.3|![](/img/OntologyHeco-v0.3.png)|
| Ontologie HeCo version 0.2|![](/img/OntologyHeco-v0.2.png)|
| Ontologie HeCo version 0.1|![](/img/OntologyHeco-v0.1.png)|

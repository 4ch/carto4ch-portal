---
title: Architecture
sidebar_position: 1
---

# Architecture

## Introduction

Les schémas ci-dessous sont des schémas d'architecture à plusieurs niveau.

## Accès aux Schémas

| Libellé| Image |
| -------- | -------- |
| Ce schéma explique l'architecture technique du démonstrateur [Schéma améliorable collectivement sous Figma](https://www.figma.com/file/HSG85jS1yXtw3FjPJN37KX/Demonstrator---en---choix-technologiques?type=design&node-id=102-119&mode=design&t=6b7q2mhZ8RQChDNb-0)|![](/img/demonstrator-architecture-schema.png)|
| Ce schéma (adressé à tous) explique la notion de "boite à outils" SemApps [Schéma améliorable collectivement sous Figma](https://www.figma.com/file/KLqHKZYak1YKREBQm2N7oS/Comprendre-la-boite-%C3%A0-outils-SemApps-pour-4CH)|![](/img/toolbox-semapps.png)|

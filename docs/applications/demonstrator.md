---
title: Démonstrateur
sidebar_position: 2
---

# Démonstrateur

## Introduction

|Schéma|Présentation|
|----|----|
|![](/img/schema-demonstrator-A-B.png)|Le **démonstrateur** est une cartographie fonctionnelle de démonstration.<br/>Son objectif est de donner un exemple de ce qui est attendu par le projet Carto4CH.<br/><br/>Il respecte le socle technique proposé (SOLID, ontologie...), et contient :<br/><ul><li>Deux serveurs de données de démonstration (Server A et Server B)</li><li>Des applications web permettant de cartographier les données de ces deux serveurs</li><li>Des données de démonstration</li></ul>|

## Accès

:::info
**Accès à l'instance A**
* My-competences de l'instance A : https://my-a.carto4ch.huma-num.fr
* Back-office de l'instance A : https://bo-a.carto4ch.huma-num.fr
* Visualisation en réseau de l'instance A : https://carto4ch-a.flod.io
* Data de l'instance A : https://data-a.carto4ch.huma-num.fr

**Accès à l'instance B**
* My-competences de l'instance B : https://my-b.carto4ch.huma-num.fr
* Back-office de l'instance B : https://bo-b.carto4ch.huma-num.fr
* Visualisation en réseau de l'instance B : https://carto4ch-b.flod.io
* Data de l'instance B : https://data-b.carto4ch.huma-num.fr

**Accès à l'instance C**
* Visualisation en réseau de l'instance C : https://carto4ch-c.flod.io
* Datade l'instance C : https://data-c.carto4ch.huma-num.fr

**Les codes sources** des projets sont dispo :
* sur le Gitlab d'huma-Num (docker + Archipelago) : https://gitlab.huma-num.fr/4ch/
:::

## Statut

* ontology : HeCo v1.0
* Back-office Server A : OK
* Back-office Server B : OK
* My-Server A : OK
* My-Server B : OK
* Visualisation-Server A : OK
* Visualisation-Server B : OK
* Cartographie générale : OK

:::caution Attention
Pour le back-office, nous utilisons le projet [Archipelago](https://github.com/assemblee-virtuelle/archipelago), basé sur [SemApps](/docs/technical-documentation/presentation-semapps).
:::

N'hésitez pas à nous contacter si vous avez des attentes particulières.

## Documentation du démonstrateur

La documentation relative à ce démonstrateur se trouve : 
* [ici, pour la documentation utilisat.eur.rice](/docs/category/doc-demonstrator-category-label-2)
* [ici, pour la documentation technique (code)](/docs/technical-documentation/demonstrator-manual)
 
---
title: Présentation
sidebar_position: 1
pagination_prev: null
pagination_next: null
---

# Présentation

## Introduction
Cette présentation décrit plus précisément le **projet Carto4CH**, mais sans entrer dans le côté technique.

## Télécharger la présentation au format PPT (validée)

[Télécharger la présentation générale en version 1.4 en PDF](./presentation-carto4ch-v1.4.pdf)

## Ou accéder à la présentation générale en ligne

:::caution
Pour le moment, vu que cette présentation est encore jeune et peut être amenée à être modifiée, nous utilisons un PAD collaboratif pour la mettre à jour plus facilement.
Le PAD peut donc être en avance sur la présentation ci-dessus en format PDF.
:::

* [Lien vers la source de la présentation](https://pad.lescommuns.org/SlideCarto4CH)
* [Lien vers la présentation](https://pad.lescommuns.org/p/SlideCarto4CH)

(Si vous souhaitez participer au contenu, merci de nous contacter).

---
title: Maquettes
sidebar_position: 4
pagination_next: null
---

# Maquettes

![](/img/mockups-baner.png)

## Introduction
Pour le projet Carto4CH, nous avons développé plusieurs applications.

Avant de développer chaque application, nous avons créé une maquette, permettant de vérifier si toute l'équipe était d'accord sur les fonctionnalités attendues. 

Pour mettre au point ces maquettes, nous avons utilisé le logiciel [Figma](https://figma.com). Il a l'avantage de pouvoir "simuler" les actions des utilisateurs.

Nous mettrons sur cette page la liste des maquettes utilisées, avec pour chacune :
* une mignature
* son titre
* sa description 
* un lien vers le projet Figma, 
* un lien vers son exécution (simulation),
* la date de dernière mise à jour
* un lien vers l'application une fois développée.

Ces maquettes peuvent aussi servir aux équipes techniques qui souhaiteraient eux aussi produire eux-mêmes leurs propres applications, utilisant les mêmes fonctionnalités.

## Liste des maquettes

| Mignature | Titre | Description | Projet | Simulation | Dernière MAJ | Application |
| -------- | ----- | -------- | ------- | ------- | ------- | ------- |
| ![](/img/cuteness-mockup-my-competences.png) | My-competences | Maquette simulant l'application My-competences. | [Projet](https://www.figma.com/file/oG1AsFQIYymkre7q6d1DOg/Maquette-My-competence?t=N349UaJ82gxFxemD-0) | [Simulation](https://www.figma.com/proto/oG1AsFQIYymkre7q6d1DOg/Maquette-My-competence?scaling=min-zoom&page-id=0%3A1&starting-point-node-id=1%3A2) | 20/01/2023 | [Application](https://my-a.carto4ch.huma-num.fr/) |
| ![](/img/cuteness-mockup-competences.png) | Saisie des compétences | Maquette simulant la saisie des compétence dans le back-office. | [Projet](https://www.figma.com/file/jeB2DSHDpXgEo8CIdGApwJ/Maquette-choix-des-comp%C3%A9tences?t=GcsgEBqOOb0vtVC2-0) | [Simulation](https://www.figma.com/proto/jeB2DSHDpXgEo8CIdGApwJ/Maquette-choix-des-comp%C3%A9tences?node-id=1%3A2&starting-point-node-id=1%3A2) | 02/06/2022 | [Application](https://semapps-a.carto4ch.huma-num.fr/) |
| ![](/img/cuteness-mockup-portal.png) | Portail Carto4ch | Maquette du portail | [Projet](https://www.figma.com/file/8sAcBID9EwBzZZ0fh0fDYE/Maquette-du-portail-Carto-4CH?t=N349UaJ82gxFxemD-0) | [Simulation](https://www.figma.com/proto/8sAcBID9EwBzZZ0fh0fDYE/Maquette-du-portail-Carto-4CH?scaling=min-zoom&page-id=0%3A1&starting-point-node-id=10%3A61&node-id=10%3A61) | 02/12/2022 | [Application](http://portal.carto4ch.huma-num.fr/) |


(Si vous souhaitez participer à la mise à jour de ces maquettes, merci de nous contacter).

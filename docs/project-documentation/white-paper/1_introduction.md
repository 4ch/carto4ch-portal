---
title: I. Introduction
sidebar_position: 2
---

# I. Introduction
:::info Signification
**4CH** = CCCCH = **C**ompetence **C**entre for the **C**onservation of **C**ultural **H**eritage
:::

Le [projet 4CH](https://www.4ch-project.eu/) est **une étude européenne sur la potentialité de créer un Centre de Compétence européen sur la Conservation du patrimoine Culturel**.
Cette étude va durer 3 ans (2021-2023).
Une vingtaine d'équipes de différents pays interviennent dans ce projet, dont nous pour la France : nous sommes des spécialistes du Web sémantique relevant de l'[université de Tours](https://www.univ-tours.fr/) et du CNRS, plus précisément du [LIFAT](https://lifat.univ-tours.fr/) et de [CITERES-LAT](http://citeres.univ-tours.fr/spip.php?rubrique31), et qui collaborons au sein du [Consortium MASA](https://masa.hypotheses.org/) et de la [MSH Val de Loire](https://www.msh-vdl.fr/) dans son Lab [HumaNum](https://www.huma-num.fr/). Nous proposons de développer **une cartographie des acteurs et des compétences en France dans le domaine du patrimoine culturel**.

Ce livre blanc sera l'occasion de vous partager nos réflexions, menées depuis avril 2022, suite à une veille sur internet, et à une série d'entretiens avec des partenaires dans le domaine du patrimoine culturel. Nous expliquerons aussi la philosophie de l'architecture technique utilisée, sans entrer dans les détails techniques, qui sont déjà expliqués dans notre [documentation technique](/docs/category/technical-documentation-category-label).

Ce livre blanc sera **versionné**, ce qui nous permettra de mettre en production petit à petit les différentes sections qui le composent. Le [tableau des versions ci-dessus](/docs/project-documentation/white-paper/presentation#versions) ainsi que le [Blog du portail](https://portal.carto4ch.huma-num.fr/blog) précisera l'historique de l'évolution de ce document.

Entre mi-avril et juillet 2022, nous avons effectué une **vingtaine d'entretiens avec une trentaine de "contacts partenaires"** de diverses organisations liées au patrimoine, Ministère de la culture, BnF, Archives de France, consortiums d'HumaNum, responsables de projets comme ESPADON ou Notre Dame de Paris, universités, etc. Nous continuerons à mener des entretiens dans les mois à venir mais de façon plus ponctuelle. La vocation de ces entretiens est d'informer et de récolter des retours sur cette initiative, qui a vu sa source au sein du projet européen 4CH, mais qui, nous l'espérons, vivra au delà et de manière autonome, le plus longtemps possible !
Nous en profitons pour remercier toutes celles et ceux avec qui nous avons pu échanger et nous espérons que leurs retours seront traduits dans ce document.

Si ce n'est pas le cas, il est fait pour évoluer et vous pouvez à tout moment [nous contacter](https://portal.carto4ch.huma-num.fr/docs/contacts) pour nous partager vos remarques et demandes d'amélioration.

---
title: II. Attentes 4CH
sidebar_position: 3
---

# II. Attentes du projet 4CH
Pendant les entretiens, nous avons cherché à répondre à ces question essentielles :
* Qu'entend-on par cartographie ?
* Pourquoi faire une cartographie des compétences ?
* Comment va-t-elle se matérialiser ?

Une première série de réponses vient de ceux qui l'ont initié et du projet 4CH, mais elle doit surtout être confrontée à ce que le terrain attend d'un tel projet (voir II).

## Un besoin de cartographier le savoir...
L'europe a été touchée par des catastrophes (naturelles, climatiques, humaines, guerres...) qui ont pesé fort dans la conservation du patrimoine culturel européen. On peut citer récemment l'incendie de Notre Dame de Paris par exemple, qui a amené les acteurs du patrimoine à réfléchir à des moyens d'**anticiper**, de **sécuriser**, et de **réagir vite**. Pour ce faire, il est nécessaire d'avoir des outils, une mémoire de l'existant, mais surtout des humains possédant des savoir-faire, des connaissances et des compétences pouvant être activées le plus efficacement en cas de besoin. Il faut aussi transmettre cette envie de vouloir conserver notre patrimoine, et y mettre les moyens en temps et en monnaie, pour y parvenir.
Et de ce côté, nous avons pu constater quelques lacunes, pour **trouver les compétences au bon moment, pour répondre à un besoin urgent**. Même sans urgence, à chaque montage de projet, que ce soit pour une étude, une médiation culturelle, une préservation ou conservation, etc. il est également nécessaire de trouver les partenaires adéquats en fonction de leurs compétences. Il faut pouvoir mettre en relation un **objectif lié à un objet patrimonial**, ou problème à traiter, avec des acteurs capables de le traiter.

Or, lorsqu'on souhaite donner accès à un espace informationnel, quel-qu’il soit, il faut d'abord le **cartographier**. C'est à dire déterminer ses contours, son périmètre, son contenu, sa raison d'être, dire ce qu'il est et ce qu'il n'est pas. Ce n'est qu'une fois que ce travail est fait qu'on peut donner accès au contenu, via un moteur de recherche, ou des cartographies diverses (géographiques, en réseau, tableaux de bords, graphiques...)

Dans le cas présent dans le projet Carto4CH, l'espace informationnel est constitué de représentations informatiques d'identités humaines, d'organisations, de secteurs du patrimoine, de projets, de métiers, de compétences, de techniques et d'outils, de domaines scientifiques, mais aussi (peut-être) de lieux, de monuments, de matières premières, de matériaux, de textures, de couleurs, de différents types d’œuvres d'art, etc.
Un travail de représentation [ontologique](https://fr.wikipedia.org/wiki/Ontologie_(informatique)) de ces éléments et de leurs relations sera nécessaire pour les différencier toutes et les rendre perceptibles et interrogeables par une interface humain-machine.

## Une utilisation du numérique encore perfectible...
[La "perception"](https://fr.wikipedia.org/wiki/Perception) est en général réservée au sensible, à l'humain. En effet, nous serions quelques années plus tôt (XXème sciècle), pour cartographier, nous aurions utilisé le partage d'information oral et écrit entre les humains. Mais depuis environ 50 ans, nous avons un outil très puissant pour faire cela, l'informatique. Cette précision peut être risible à notre époque ultra-digitalisée, mais le **paradoxe est là**, lorsqu'un problème se produit, beaucoup d'entre nous recherchent d'abord dans leur mémoire personnelle, ou dans leurs carnets papier, prennent leur téléphone pour appeler un expert, qui lui, connaît d'autres experts, et les équipes se forment ainsi pour répondre à un besoin urgent.

Donc oui, nous avons l'outil informatique, mais à l'heure actuelle, nous ne savons pas encore l’utiliser à bon escient pour répondre à certains besoins.

Le projet Carto4CH souhaite trouver le juste milieu dans l'utilisation de l'outil informatique. Ni trop, ni pas assez. Plutôt que de chercher à trouver des résultats dans une multitude de données (comme le fait le big data), nous allons plutôt partir sur une construction de résultats basés sur de nouvelles **données structurées**. Nous avons la chance de pouvoir le faire car les données de ce projet semblent assez précises et définies, ce qui n'est pas le cas de tous les projets.

## Besoin d'ouverture et de partage
Nous sommes capables de cartographier presque toutes les routes du globe, mais pas encore tous les savoirs des humains qui y vivent.
En effet, dans le cas des savoirs, le traitement des informations est plus complexe. Il est très lié à l'humain, il évolue dans le temps, et est encore maintenant source de pouvoir. Souvent caché, ou vendu, le savoir entre dans le jeu de la compétition économique, des secrets industriels, etc.
Depuis quelques années, l'open-source, l'open-data, l'open-science, ou encore **le logiciel libre**, montrent une nouvelle voie, tout en permettant à chacun de proposer des services rentables économiquement.
Le projet 4CH se place dans cette éthique de partage, en utilisant des logiciels open-source, en favorisant les gouvernances collectives, en ouvrant les données au maximum, et en utilisant l'informatique pour numériser la connaissance et s'en servir de mémoire collective.

Même si cette cartographie Carto4CH ne s'occupera "que" des acteurs et des compétences, nous utiliserons des logiciels open-source, en licence libre, utilisant des formats de donnée standardisés, correctement documentés, pour faciliter une confiance et une adhésion au niveau français, puis nous l'espérons européen.

## Une base de connaissances partagée dans un cloud
Le projet 4CH a le souhait de créer **une base de connaissance européenne** permettant de partager les données du patrimoine au sein d'une infrastructure technique ouverte aux différents partenaires, et en structurant le plus possible les données dès le début.
Il construit un **"cloud 4CH"** ouvert, permettant aux partenaires d'y publier leurs services.

Le fait que nous proposions une cartographie **partagée** et **distribuée**, dans laquelle chaque partenaire conserve ses données au sein d'une gouvernance partagée, entre tout à fait dans ces réflexions et apporte une solution aux limitations que montrent les systèmes centralisés et propriétaires actuels.
Nous utiliserons une architecture **extensible** ("scalable") à l'échelle européenne. En effet, plutôt que d'avoir un gros serveur centralisé qui peut être surchargé en cas d'une augmentation rapide de données et de leurs utilisations, nous nous baserons plutôt sur un ensemble de "petits" serveurs interconnectés. Certains pourront être dans le cloud 4CH.

Côté **infrastructure**, nous ferons donc notre possible pour être compatibles avec le cloud 4CH, afin de proposer différents lieux d'hébergement des serveurs (cloud 4CH, Huma-Num, autres...)

## Respect des données personnelles
Le projet 4CH souhaite aussi respecter le [RGPD](https://www.cnil.fr/fr/rgpd-de-quoi-parle-t-on). Que chaque partenaire doit pouvoir récupérer ses données à tout moment, les mettre à jour et les supprimer.

Le projet Carto4CH, de par son ADN "distribuée", souhaite aussi être en règle avec ces principes. Des travaux auront lieu à ce sujet qui donneront des recommandations aux partenaires qui publieront leurs données.
Ainsi, chacun gardera la maîtrise, le pouvoir et les responsabilités liés à ses données.

## Utiliser les ontologies et terminologies existantes
Le projet 4CH invite les partenaires à utiliser des ontologies existantes et reconnues dans le domaine du patrimoine culturel (par exemple : CIDOC CRM ou EDM Europeana) pour sémantiser leurs données.

Côté Carto4CH, nous souhaitons publier une ontologie relative aux acteurs et aux compétences, utilisant au mieux les schémas existants. Ainsi, les données présentes dans les serveurs de chaque partenaire seront interopérables et réutilisables dans d'autres environnements (même en dehors de notre cartographie). Nous souhaitons également  réutiliser des thésauri existants, sur des métiers, des secteurs du patrimoine, etc., ainsi que des listes d'autorité fournissant des identifiants, par exemple pour les organisations et les personnes.

## Une temporalité assumée
Le projet 4CH est une étude préalable à la création d'un centre de compétence européen. Elle se positionne dans un temps assez long et lent, pour que chaque pays et organisation puisse prendre le temps de se positionner, et que des interactions entre les pays et équipes apparaissent.

Du côté Carto4CH,  nous sommes dans la même volonté d'avancer posément, un pas après l'autre, en prenant le temps nécessaire pour impliquer nos partenaires, accompagner les équipes techniques, communiquer régulièrement, etc.
Peut-être que cette cartographie ne contiendra que 3 ou 4 partenaires dans la première année. Mais nous souhaitons que le socle technique partagé soit suffisamment réfléchi pour qu'il perdure dans le temps, et assez modulaire pour laisser place aux nouvelles idées et initiatives tout au long du projet.
Nous souhaitons donc proposer des standards déjà réfléchis par des communautés établies, et proposer des logiciels potentiellement indépendants des langages de programmation ou des syntaxes de données.
**Tant que nous partageons tous les mêmes standards, nous restons interopérables**.

## Pas d'attente précise concernant l'interface
Côté 4CH, nous n'avons pas pour le moment de conseil ou de recommandation au niveau de l'interface Humain-machine.

Carto4CH reste donc libre de proposer différentes interfaces, voir la possibilité de proposer à des partenaires de faire leur propre interface ou d'intégrer les données de la cartographie dans leurs logiciels métier...

## Quel est le livrable à fournir ?
Le projet 4CH ne demande pas de livrable particulier, il souhaite que l'équipe française apporte ses compétences en web sémantique et relaye 4CH auprès de la communauté CH France le temps du projet.

Le projet Carto4CH est donc plus dans un mode "expérimental". Nous avons la chance d'avoir une grande liberté d'action, qui va nous permettre de proposer une architecture novatrice et ambitieuse répondant à des problèmes souvent identifiés dans les systèmes centralisés actuels. En effet des cartographies d'acteurs et de compétences en France dans le domaine du patrimoine culturel ont déjà été menées, sans qu'elles ne soient ni à jour ni utilisées actuellement.

Notre souhait est que cette cartographie reste vivante et utilisée, et si possible que ses composants (architecture, ontologie, thésauri, et même gouvernance) eux-mêmes soient réutilisables dans d'autres contextes.

# En conclusion
Avec le projet Carto4CH, nous proposons une architecture compatible avec la vision et l'éthique du projet 4CH : Ouverte, distribuée, modulaire, et standardisée.

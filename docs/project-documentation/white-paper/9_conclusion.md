---
title: Conclusion
sidebar_position: 9
pagination_next: null
---

# Conclusion
Même s'il n'est pas encore terminé, nous espérons que cette première version de livre blanc vous a apporté une synthèse de nos échanges et vous a donné envie de suivre ce projet, voir d'y participer.
C'est une première version, il est évolutif et tiendra compte de vos retours.
N'hésitez donc pas à nous envoyer vos retours par e-mail en utilisant [l'adresse e-mail de contact](https://portal.carto4ch.huma-num.fr/docs/contacts).

---
title: 0. Présentation
sidebar_position: 1
pagination_prev: null
---

# 0. Présentation

## Versions
Nous sommes en **version 0.1** de ce livre blanc.
A chaque publication, nous essayerons de mettre à jour le tableau ci-dessous.
Le blog fera aussi office d'historique.

| Version | Date |Description|
| -------- | ----- | -------- |
| 0.1 | 25/07/2022 | Première version du livre blanc Carto 4CH, contenant une synthèse des entretiens avec les partenaires (§II. et §III.)|
| 0.x | ?? | Prise en compte des retours des partenaires |
| 1.0 | ?? | La veille sur l'existant (§IV.) |
| 1.x | ?? | Prise en compte des retours des partenaires |

## Téléchargement en PDF
Si vous préférez télécharger cette version de livre blanc, voici la version en cours au format PDF :
* [Livre blanc - Version en cours - au format PDF](./livre-blanc-carto4ch.pdf)

## Qu'est-ce qu'un livre blanc ?
Il me semble important de nous aligner sur ce que nous attendons de ce livre blanc, en partant de la définition officielle [Wikipedia](https://fr.wikipedia.org/wiki/Livre_blanc).

> *Un livre blanc est un type de littérature grise (terme générique, désigne les documents produits par l’administration, l’industrie, l’enseignement supérieur et la recherche, les services, les ONG, les associations, etc., qui n’entrent pas dans les circuits habituels d’édition et de distribution) prenant la forme d'un rapport ou guide destiné à présenter des informations concises sur un sujet complexe tout en présentant les principes de l'auteur sur le sujet. Il a généralement pour objectif de faciliter ou d'orienter la prise de décision du lecteur sur le sujet, et est utilisé aussi bien au niveau institutionnel que commercial.
> Son usage officiel dans le domaine politique a évolué depuis son apparition dans les années 1920. Né du besoin d'exprimer les intentions d'un gouvernement dans un contexte précis, il peut servir à établir une mise au point de portée générale ou à rechercher un consensus dans un cadre spécifique. Il permet aussi à des institutions privées ou publiques à but non lucratif comme les ONG de publier un message officiel sous forme d'état des lieux sur un domaine d'intérêt public. Les livres blancs trouvent aujourd'hui un nouvel emploi dans un contexte non officiel, tel celui qui a trait aux activités économiques. La communication d'entreprise, par l'intermédiaire du marketing, des relations publiques et d'internet, tire parti de l'efficacité de ce nouvel outil de développement commercial interactif.*

Dans notre cas, nous n'avons **pas d'intérêts commerciaux**. Le projet est financé pour l'instant sur les fonds européens du projet 4CH, et nous ne savons pas à l'heure actuelle comment il vivra par la suite.
On pourrait dire qu'il a une orientation **politique**, car il invite le lecteur à se questionner sur l'architecture informatique souhaitable à notre époque, et à faire un choix.
Nous souhaitons que ce livre blanc aide le lecteur à **comprendre la philosophie du commun** que nous sommes en train de créer, avec l'espérance que nous serons progressivement de plus en plus à l'utiliser et à le faire vivre.

Il viendra compléter la littérature présente dans le [portail Carto4CH](https://portal.carto4ch.huma-num.fr/).
Nous y mettrons potentiellement des axes de réflexion qui n'ont pas été retenus, car cela vous aidera à comprendre nos choix. Nous essayerons de ne pas dupliquer trop l'information par rapport à ce qui existe dans le portail, dans les présentations et dans les documentations techniques. Nous pourrons y glisser quelques détails, anecdotes, approximations (à but de vulgarisation) que nous évoquons oralement lors de nos entretiens, mais que nous n'avons pas écrit dans nos présentations.

Ce livre blanc pourra continuer de vivre en dehors du projet 4CH, voir même en dehors du domaine du patrimoine culturel, car son architecture informatique distribuée pourra inspirer d'autres projets.

Nous ne souhaitons pas citer de personnes, nous citerons uniquement des organisations ou des projets.

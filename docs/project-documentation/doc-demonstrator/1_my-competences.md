---
title: Doc. de My-competences
sidebar_position: 2
---

# Manuel utilisateur de My-competences

![](/img/my-banner.png)

## Introduction

My-competence est une interface utilisat.eur.rice permettant aux personnes de renseigner leurs compétences et de ne voir que leur compétences (contrairement au back-office, dans lequel tout le monde voit tout).

## Version

Cette documentation porte celle du démonstrateur : **V1.0**

## Pré-requis
Pour vous faciliter la lecture de ce qui suit, nous allons ensemble passer en revue certains concepts (instances, authentification, référentiels, etc.).

Pour cela, merci de lire la rubrique [Pré-requis du manuel utilisateur](/docs/category/requirements-category-label-2)

## Gestion des postes

### Présentation
Il est possible de gérer (créer, éditer et supprimer) des **postes**. Nous entendons par "postes", les expériences professionnelles.

Ces expériences professionnelles vous ont apportées des compétences que vous allez pouvoir renseigner ensuite.

### Création des postes
Au début, la liste des postes est vide.
Cliquez sur le bouton "Créer" à droite du mot "postes".

Vous pouvez alors renseigner le formulaire du poste de votre choix.

![](/img/my-job-create.png)

vous pourrez préciser : 

* le **titre** (ce que vous écririez sur votre CV...)
* la **courte description**
* le **type de contrat** (CDD, CDI, Indépendant...)
* l'employeur
* la profession
* les compétences que ce poste a fait naitre
* la **description**
* la date de début 
* la date de fin
* Une liste de thèmes qui pourra nous aider à filtrer plus tard dans la cartographie.

Par exemple : 

![](/img/my-job-create-example.png)

### Carte d'un poste
Une fois renseignées, les "cartes" des objets apparaissent dans la liste globale de My-competences.

![](/img/my-job-list.png)

Lorsque vous cliquez dessus, cela affiche le contenu du poste.

![](/img/my-job-show.png)

### Edition d'un poste
On peut alors repasser en mode "edition" ou revenir à la liste via les boutons en haut à droite.

### Suppression d'un poste
La suppression d'un poste est possible en utilisant le mode édition et en cliquant sur le bouton "Supprimer" en bas à droite.

## Gestion des contributions

### Introduction
Il est possible de gérer (créer, éditer et supprimer) des contributions. Nous entendons par "contributions", les participations à des associations ou à des projets divers sans rémunération financière spécifique, et sans contrat professionnel.  Les projets auxquels on participe dans le cadre d'un poste rémunéré peuvent également être déclarés comme contributions, sans qu'il n'y ait de lien direct avec le poste (si elles sont précisées, les dates pourront montrer ce lien).

Ces contributions vous ont apporté des compétences que vous allez pouvoir renseigner ensuite.

### Création d'une contribution
Au début, la liste des contributions est vide.
Cliquez sur le bouton "Créer" à droite du mot "contributions".

Vous pouvez alors renseigner le formulaire de la contribution de votre choix.

![](/img/my-commitment-create.png)

vous pourrez préciser : 
* le **titre** de la contribution (comme vous le feriez sur un CV)
* la **courte description**
* le **projet** auquel vous avez contribué en effectuant cette activité (il peut y en avoir plusieurs)
* les **compétences** que cette activité ont révélés en vous
* une **description**
* la **date de début**
* la **date de fin**
* Une liste de **thèmes** qui pourra nous aider à filtrer plus tard dans la cartographie.

Par exemple : 

![](/img/my-commitment-create-example.png)

### Carte contribution
Une fois renseignées, les "cartes" des contributions apparaissent dans la liste globale de My-competences.

![](/img/my-commitment-list.png)

Lorsque vous cliquez dessus, cela affiche le contenu de la contribution.

![](/img/my-commitment-show.png)

### Edition d'une contribution
On peut alors repasser en mode "edition" ou revenir à la liste via les boutons en haut à droite.

### Suppression d'une contribution
La suppression d'une contribution est possible en utilisant le mode édition et en cliquant sur le bouton "Supprimer" en bas à droite.

## Gestion des formations

### Introduction
Il est possible de gérer (créer, éditer et supprimer) des **formations**.
Nous entendons par "formations", des enseignements que vous avez suivis : 
* pendant vos études
* pendant votre expérience professionnelle
* lors de vos contributions
* par vous même

Ces enseignements vous ont apporté des compétences que vous allez pouvoir renseigner.

### Création d'une formation
Au début, la liste des formations est vide.
Cliquez sur le bouton "Créer" à droite du mot "formation(s)".

Vous pouvez alors renseigner le formulaire de la formation de votre choix.

![](/img/my-training-create.png)

Vous pourrez préciser : 
* le **titre** de votre formation
* une **courte description**
* l'**organisation** chez qui vous vous êtes formé
* les **compétences** acquises pendant cette formation
* une **description** du contenu de la formation
* la **date de début**
* la **date de fin**
* Une liste de **thèmes** qui pourra nous aider à filtrer plus tard dans la cartographie.

Par exemple : 

![](/img/my-training-create-example.png)

### Carte formation
Une fois renseignées, les "cartes" des formations apparaissent dans la liste globale de My-competences.

![](/img/my-training-list.png)

Lorsque vous cliquez dessus, cela affiche le contenu de la formation.

![](/img/my-training-show.png)

### Edition d'une formation
On peut alors repasser en mode "edition" ou revenir à la liste via les boutons en haut à droite.

### Suppression d'une formation
La suppression d'une formation est possible en utilisant le mode édition et en cliquant sur le bouton "Supprimer" en bas à droite.

## Gestion des compétences

### Introduction
Enfin, suite au remplissage de votre CV, nous allons pouvoir passer à la saisie des compétences.
Ces compétences entrent dans le périmètre du patrimoine culturel.
Nous avons donc proposé de structurer ces compétences en fonction de "qualités" liées à l'environnement du Patrimoine.

:::info
Avant tout, lire les [Pré-requis sur les compétences et les qualités](/docs/project-documentation/doc-demonstrator/requirements/competences)
:::

### Création d'une compétence
Au début, la liste des compétence est vide.
Cliquez sur le bouton "Créer" à droite du mot "Compétences".

Vous pouvez alors renseigner le formulaire de la compétence de votre choix.

![](/img/my-competence-create.png)

Vous pourrez préciser : 
* une **description** (pour apporter un context à cette compétence)
* une (ou plusieurs) **disciplines**
* un (ou plusieurs) **secteurs**
* un (ou plusieurs) **objets d'étude**
* un (ou plusieurs) **outils techniques**
* Des **postes** ayant apportés cette compétence
* Des **contributions** ayant apportés cette compétence
* Des **formations** ayant apportés cette compétence

Par exemple : 
![](/img/my-competence-create-example.png)

### Carte compétence
Une fois renseignées, les "cartes" des compétences apparaissent dans la liste globale de My-competences.

![](/img/my-competence-list.png)

Lorsque vous cliquez dessus, cela affiche le contenu de la compétence.

![](/img/my-competence-show.png)

### Edition d'une compétence
On peut alors repasser en mode "edition" ou revenir à la liste via les boutons en haut à droite.

### Suppression d'une compétence
La suppression d'une formation est possible en utilisant le mode édition et en cliquant sur le bouton "Supprimer" en bas à droite.

## Visualisation du CV

:::info
Une fois vos données entrées, vous pouvez utiliser l'application du back-office, pour visualiser votre CV (et remplir vos données personnelles...).
:::

## Conclusion

Merci d’avoir suivi ce manuel, nous espèrons que tout s’est bien passé et que vous êtes satisfait de cette application My-competences.

Les données récoltées sont à présent structurées dans la base conformément à l'ontologie HeCo.
Elles pourront donc être interrogées par un outil de cartographie et de visualisation, ou par toute nouvelle interface d'interrogation spécifique, en plus du Back-office et de My-competences.

Pour toutes remarques, suggestions, idées, [contactez-nous](/docs/contacts)
---
title: Doc. du Back-office
sidebar_position: 3
---

# Documentation du Back-office

![](/img/bo-banner.png)

## Introduction

Le back-office est une application permettant d'administrer un serveur Carto4CH.

Contrairement à My-Competences, qui ne permet de gérer que le postes, les contributions, les formations et les compétences d'une personne, le back-office permet d'administrer en plus : 
* les **organisations**
* les **profils** des utilisateurs 
* les **projets**
* les **référentiels** (qualités et professions)

Ce que nous appelons l'interface "back-office" est issue d'un projet appelé [Archipelago](https://github.com/assemblee-virtuelle/archipelago) créé et maintenu par  l'[Assemblée Virtuelle](https://virtual-assembly.org) et basé sur [SemApps](https://semapps.org), contenant une interface permettant d'éditer des concepts comme des organisations, des personnes, et dans notre cas, **des compétences**. 
C'est un projet open-source, libre, sémantique et distribué, basé sur les standards du [W3C](https://fr.wikipedia.org/wiki/World_Wide_Web_Consortium).

:::caution Attention
Cette documentation s'adresse à la fois aux administrateurs de futures instances, qu'aux utilisateurs voulant enregistrer leurs compétences, soit dans cet espace de test qu'est le démonstrateur, soit dans une instance spécifique de production. 

Sachant que pour les utilisateurs, l'interface la plus simple est plutôt [My-competences](/docs/project-documentation/doc-demonstrator/my-competences).

Nous avons souhaité créer la possibilité de plusieurs usages, et laissé la liberté à l'administrateur d'une instance, de s'organiser comme il le souhaite en fonction des deux interfaces.
:::

## Accès à l'application

L'URL de l'application est précisée dans ce portail : [Applications > Démonstrateur](/docs/applications/demonstrator)

## Pré-requis
Pour vous faciliter la lecture de ce qui suit, nous allons ensemble passer en revue certains concepts (instances, authentification, référentiels, etc.).

Pour cela, merci de lire la rubrique [Pré-requis du manuel utilisateur](/docs/category/requirements-category-label-2)

## Les types et les statuts
### Généralités
Pour plusieurs ressources (organisations, projets, etc.), il est nécessaire de savoir gérer des **types** et des **statuts**.

**Par exemple :**
Une organisation peut être de type entreprise, association, école, etc.

Elle peut aussi avoir un statut "public" ou "privé".

#### Aux utilisateurs
Ces types sont ajoutés par votre administrateur. Si vous ne trouvez pas le type qui vous convient, merci de le contacter.

#### Aux administrateurs
Nous avons choisi de regrouper tous les statuts de différentes ressources et tous les types de différentes ressources.

##### Gérer les statuts
Pour ajouter un statut, allez dans le menu > Config > Statuts, et cliquez sur le bouton "créer" en haut à droite.

Il est important de choisir la bonne classe, liée à la ressource pour laquelle vous souhaitez ajouter un statut.
![](/img/bo-status-create.png)

Cela ajoutera un nouveau statut à la liste.

![](/img/bo-status-show.png)

##### Gérer les types
Pour ajouter un type, allez dans le menu > Config > Types, et cliquez sur le bouton "créer" en haut à droite.

Il est important de choisir la bonne classe, liée à la ressource pour laquelle vous souhaitez ajouter un type.

![](/img/bo-type-create.png)

Cela ajoutera un nouveau type à la liste.

![](/img/bo-type-show.png)

### Filtrage des ressources
Que ce soit pour les organisations ou pour une autre ressource, à partir du moment où ils apparaissent en liste, nous pouvons y ajouter un mécanisme de filtrage à droite en fonction d'une des propriétés de cet ressource.

Par exemple, pour la liste des organisations, il est possible de filtrer, soit par le type, soit par le thème (on aurait pu aussi ajouter le statut...).

En cliquant par exemple sur le type "UMR (3)", la liste se filtre pour ne laisser que les UMR.

![](/img/bo-list-filter.png)

Pour **supprimer le filtre**, il faut de nouveau cliquer sur le même type. 
Un icon avec une croix dans un cercle apparait à droite du terme filtrant. Cela permet à l'utilisateur de savoir si un filtre est activé ou pas.

Ces filtres ne s'activent que sur l'interface de l'utilisateur dans son navigateur (et pas pour l'ensemble des personnes connectées).

## Les organisations

### Description

Le terme « Organisation » peut être : 
* une **association** (comme l’Assemblée Virtuelle), 
* un **établissement public** (enseignement supérieur et recherche comme les universités, ou uniquement recherche comme le CNRS), 
* un **ministère**, 
* une **entité interne** à un établissement public (comme une unité de recherche MAP, LIFAT, MSH, CITERES, C2RMF, une équipe d’une unité de recherche comme CITERES-LAT), 
* un **service** comme SNUM, 
* une **fondation** comme FSP
* une **entreprise** (comme SPARNA)
* une **université**
* une **école privée**

Celles-ci vont être ajouté par les administrateur de chaque instance, ou par vous si vous en avez le besoin (par exemple, un organisme de formation).

![](/img/bo-organization-list.png)

### Création d'une organisation
Pour ajouter une organisation, cliquez sur le bouton **Créer** à droite.

Vous pouvez alors remplir un formulaire contenant :
* un premier onglet de données concernant l'organisation : 
  * le **nom** de l'organisation
  * une **courte description**, 
  * une **description**, 
  * un **type**, 

![](/img/bo-organization-create.png)

* un deuxième onglet de relations avec d’autres concepts
  * les **postes**
  * les **formations**
  * les **thèmes**

![](/img/bo-organization-create-relations.png)

### Affichage de la fiche de l'organisation

![](/img/bo-organisation-show.png)

### Edition d'une organisation
On peut alors repasser en mode "edition" ou revenir à la liste via les boutons en haut à droite.

### Suppression d'une organisation
La suppression d'une organisation est possible en utilisant le mode édition et en cliquant sur le bouton "Supprimer" en bas à droite.

## Les personnes / utilisateurices
### Généralités
Les personnes présentes dans ce "trombinoscope" correspondent aux utilisateurices qui se sont créé un profil sur l'instance.

![](/img/bo-list-person.png)

En cliquant sur chaque personne, vous avez accès en lecture seule à leur CV.

![](/img/bo-person-show.png)

Chaque utilisateurice peut à tout moment mettre à jour son CV en modifiant son profil.

### Edition du profil
L'utilisateur a créé un profil lors de son [authentification](/docs/project-documentation/doc-demonstrator/requirements/authentication) sur l'application.

Suite à cela, il peut revenir à tout moment pour mettre à jour son profil.
Pour cela, vous pouvez : 
* cliquer sur le bouton "Modifier" lors de l'affichage de votre profil (CV)
* cliquer dans le menu en haut à droite : "Editer mon profil"

![](/img/bo-menu-edit-profil.png)

Vous pouvez mettre à jour les **données** vous concernant

![](/img/bo-person-edit-data.png)

Mais aussi les relations avec des postes, contributions, formations et compétences.

![](/img/bo-person-edit-relations.png)

Ce sont tous ces éléments qui formeront votre CV.

### Suppression d'un profil

:::caution Attention
**Attention :** Merci de prévenir votre administrateur avant de supprimer votre profil, car cela peut avoir des conséquences.
:::

La suppression d'un profil est possible en utilisant le mode édition et en cliquant sur le bouton "Supprimer" en bas à droite.

## Les projets

### Description
Vous pouvez ajouter des projets qui seront ensuite utilisés potentiellement par des contributions (voir ci-dessous).

![](/img/bo-project-list.png)

### Création d'un projet
Pour ajouter un projet, cliquez sur le bouton **Créer** à droite.

Vous pouvez alors remplir un formulaire contenant :
* un premier onglet de données concernant l'organisation : 
  * le **nom** de l'organisation
  * une **courte description**, 
  * une **description**, 
  * un **statut**, 
  * un **type**,
  * un **site web** 

![](/img/bo-project-create-data.png)

* un deuxième onglet de relations avec d’autres concepts
  * les **contributions** liées à ce projet
  * les **thèmes** en relation avec ce projet

![](/img/bo-project-create-relation.png)

### Edition d'un projet
On peut alors repasser en mode "edition" ou revenir à la liste via les boutons en haut à droite.

### Suppression d'un projet
La suppression d'un projet est possible en utilisant le mode édition et en cliquant sur le bouton "Supprimer" en bas à droite.

## Ordre de création des objets
:::info
1) Qualités 
2) Professions
3) Reste...
:::

Il est important de comprendre l'organisation entre les ressources avant de créer votre CV.

Les postes peuvent utiliser les professions, et les compétences détaillées sont dépendantes des qualités. Il est donc important de commencer par créer les qualités, puis les professions en premier.

voir : [Compréhension des compétences et des qualités](/docs/project-documentation/doc-demonstrator/requirements/competences)

L'administrateur de votre instance va donc d'abord créé les premières qualités de base (pour éviter des listes vides), puis les professions de base, et enfin, les utilisateurs ajouteront leurs postes, formations, et leurs contributions.

## Création des qualités

Les qualités servent à préciser le détail d'une compétences.
L'administrateur ou l'utilisateur sera amené à créer des qualités.

Voyons ici l'exemple de création pour une **"discipline"**.

Cliquez sur le bouton **Créer** à droite du mot "discipline".

Vous arrivez sur un formulaire vous expliquant que si vous entrez quelques lettres, le champ va interroger le référentiel [Wikidata](https://www.wikidata.org/wiki/Wikidata:Main_Page) et vous propose une liste qui peut vous aider.

![](/img/bo-discipline-propositions.png)

Deux choix s'offrent à vous : 
* **soit vous trouvez le terme** que vous cherchez dans la liste, dans ce cas, sélectionnez-le.
* **soit vous ne le trouvez pas**, et dans ce cas, vous pouvez ajouter votre propre terme. Une fois que vous avez terminé d'écrire les lettres de votre terme, cliquez sur la ligne commençant par un "+" comme ci-dessous : 

![](/img/bo-discipline-create.png)

Si vous cliquez autre part, le champ se vide et vous pouvez recommencer.

:::caution Attention
**Attention :** L'aide à la saisie manque de réactivité, il faut parfois faire preuve de patience. Surtout si votre réseau est lent.
:::

Une fois créés, elles apparaissent dans la liste : 

![](/img/bo-discipline-show.png)

Ce sera la même choses pour les secteurs, les objets d'étude et les outils.

## Les professions (métiers)

### Présentation
Les premières professions seront créées par l'administrateur, et peu à peu, la liste sera complété par les utilisateurs.

S'il y a des doublons ou des erreurs, merci d'en informer votre administrateur.

Pour créer une profession, qui ensuite pourra être reliée à un poste, nous pouvons être aidé par les professions du [référentiel européen ESCO](https://esco.ec.europa.eu/fr/classification/occupation_main).

### Création
Pour ajouter une profession, cliquez sur le bouton **Créer** à droite du mot "Professions".

![](/img/bo-occupation-propositions.png)

Si vous ne trouvez pas votre profession dans la liste proposée, vous pourrez ajouter votre propre profession.

![](/img/bo-occupation-list.png)

## Les postes
### Généralités
Vous allez pouvoir ajouter les **expériences professionnelles** (= postes) que nous avez exercé dans votre parcours.

![](/img/bo-job-list.png)

### Création d'un poste
Pour créer un poste, cliquez sur le bouton "Créer" à droite du mot "Postes".
Vous allez pouvoir remplir un formulaire contenant : 
* un premier onglet de données concernant le poste :
  * un **titre**
  * une **courte description**
  * un **type de contrat** (CDI, CDD...)
  * une **description**
  * une **date de début**
  * une **date de fin**

![](/img/bo-job-create-data.png)

* un deuxième onglet contenant les relations avec d'autres ressources : 
  * des **organisations** pour lesquelles vous avez travaillé pendant ce poste
  * des **professions** pour lesquelles vous avez été employé
  * des **compétences** développées pendant ce poste
  * des **thèmes** relatifs à ce poste

![](/img/bo-job-create-relation.png)

### Edition d'un poste
On peut alors repasser en mode "edition" ou revenir à la liste via les boutons en haut à droite.

### Suppression d'un poste
La suppression d'un poste est possible en utilisant le mode édition et en cliquant sur le bouton "Supprimer" en bas à droite.

## Les contributions
### Généralités
Vous allez pouvoir ajouter les **participations/contributions** que nous avez effectué dans votre parcours.

![](/img/bo-commitment-list.png)

### Création d'une contribution
Pour créer un poste, cliquez sur le bouton "Créer" à droite du mot "Contributions".
Vous allez pouvoir remplir un formulaire contenant : 
* un premier onglet de données concernant le poste :
  * un **titre**
  * une **courte description**
  * une **description**
  * une **date de début**
  * une **date de fin**

![](/img/bo-commitment-create-data.png)

* un deuxième onglet contenant les relations avec d'autres ressources : 
  * des **projets** pour lesquelles vous avez agit via cette contribution
  * des **compétences** développées pendant cette contribution
  * des **thèmes** relatifs à cette contribution

![](/img/bo-commitment-create-relation.png)

### Edition d'une contribution
On peut alors repasser en mode "edition" ou revenir à la liste via les boutons en haut à droite.

### Suppression d'une contribution
La suppression d'une contribution est possible en utilisant le mode édition et en cliquant sur le bouton "Supprimer" en bas à droite.

## Les formations

### Présentation
Ajouter une formation est aussi simple que les postes.

![](/img/bo-training-list.png)

### Création
Pour créer une formation, cliquez sur le bouton "Créer" à droite du mot "Formations".

Vous allez pouvoir remplir un formulaire contenant : 
* un premier onglet de données concernant la formation :
  * un **titre**
  * une **courte description**
  * une **description**
  * une **date de début**
  * une **date de fin**

![](/img/bo-training-create-data.png)

* un deuxième onglet contenant les relations avec d'autres ressources : 
  * des **organisations** chez qui vous avez effectué cette formation
  * des **compétences** développées pendant cette formation
  * des **thèmes** relatifs à cette formation

![](/img/bo-training-create-relation.png)

### Edition d'une formation
On peut alors repasser en mode "edition" ou revenir à la liste via les boutons en haut à droite.

### Suppression d'une formation
La suppression d'une formation est possible en utilisant le mode édition et en cliquant sur le bouton "Supprimer" en bas à droite.

## Les compétences

### Présentation
Vous allez pouvoir ajouter des compétences. 
Une "compétence" est structurée en un ensemble de [qualités](/docs/project-documentation/doc-demonstrator/requirements/competences). Chaque compétence peut être reliée optionellement aux postes, contributions et formations.

Ces compétences peuvent avoir été acquise dans un contexte particulier (un emploi, une formation ou une contribution).

![](/img/bo-competence-list.png)

### Création d'une compétence
Pour créer une compétence, cliquez sur le bouton "Créer" à droite du mot "Compétences".

:::caution Attention
Le titre sera généré automatiquement à partir de votre identifiant et d'un incrément.
:::

Vous allez pouvoir remplir un formulaire contenant : 
* un premier onglet de données concernant la compétence :
  * une **description** du context de la compétence
  * des **disciplines**
  * des **secteurs**
  * des **des objects d'étude**
  * des **des outils**

:::info
Pour plus d'information sur les qualités, lire [pré-requis sur les compétences et les qualités](/docs/project-documentation/doc-demonstrator/requirements/competences)
:::

![](/img/bo-competence-create-data.png)

* un deuxième onglet contenant les relations avec d'autres ressources : 
  * des **postes** qui ont générés cette compétence
  * des **contributions** qui ont générés cette compétence
  * des **formations** qui ont générés cette compétence

![](/img/bo-competence-create-relation.png)

Pour plus d'information sur les qualités choisies, vous pouvez vous référer au [document de l'ontologie HeCo](/docs/technical-documentation/ontology)

### Edition d'une compétence
On peut alors repasser en mode "edition" ou revenir à la liste via les boutons en haut à droite.

### Suppression d'une compétence
La suppression d'une compétence est possible en utilisant le mode édition et en cliquant sur le bouton "Supprimer" en bas à droite.

## Associations des objets créés au profil

La particularité du back-office est que le lien entre votre profil et tout ce que vous avez ajouté est à ajouter manuellement. Ce lien est automatique dans My-competences. Nous tenons à ne rien automatiser dans le BO, car il est réservé plus aux adminitrateurs.

Pour cela, rendez-vous dans votre profil utilisateur, dans l'onglet "CV" : 

![](/img/profil-cv.png)

## Visualisation du "CV patrimonial"

Une fois toutes les associations effectuées, vous pouvez observer votre CV dans votre profil.
Ce CV sera alors public.

![](/img/profil-cv-show.png)

## Conclusion

Merci d'avoir suivi ce manuel, j'espère que tout s'est bien passé et que vous êtes satisfait de ce back-office.

Les données récoltées sont à présent structurées dans la base conformément à l'ontologie HeCo.
Elles pourront donc être interrogées plus simplement par l'outil de cartographie et de visualisation.

Pour toutes remarques, suggestions, idées, [contactez-nous](/docs/contacts)

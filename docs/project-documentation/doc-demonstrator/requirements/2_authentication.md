---
title: Authentification
sidebar_position: 2
---

# Authentification

![](/img/authentification.png)

## Généralités
Nous allons expliquer dans cette section les notions de "profil utilisateur", de "connexion", "d'authentification" ou de "permissions".
Ces notions sont valables dans les deux applications, la seule différence est qu'actuellement, l'utilisateur n'a accès à son profil que dans le back-office.

## Choix de connexion
**Par défaut**, vous n'êtes pas connecté. Avant d'accéder à l'application, il faut vous créer un compte utilisteur.

| Dans le menu en haut à droite | Image-écran |
| -------- | -------- |
| **Sur le back-office** Si vous souhaitez créer ou modifier des données, cliquez en haut à droite sur l'icon de profil, qui vous permet, soit de vous inscrire, soit de vous connecter ou de vous déconnecter, soit de voir et de gérer vos données personnelles |![](/img/authentification-menu-bo.png)|
| **Sur My-competence** L'application vous demande directement de vous connecter. Par contre, elle ne permet pas de gérer votre profil, cette fonctionnalité se trouve uniquement dans le back-office.|![](/img/authentification-menu-my.png)|

## Authentification

| Serveur d'authentification | Image-écran |
| -------- | -------- |
|**Sur le back-Office** Si c'est la première fois, choisissez **"S'inscrire"**, afin de vous inscrire en créant votre utilisateur sur le serveur d'authentification. Si vous avez déjà créé votre compte sur le serveur d'authentification, et que vous avez cliqué sur "Se connecter", **suivez l'écran d'explication**. |![](/img/authentification-sso-bo.png)|
|**Sur My-competence** Si c'est la première fois, l'application vous envoie directement sur l'invitation à vous créer un compte.|![](/img/authentification-sso-my.png)|

## Inscription
Dans les deux applications, si c'est la première fois, vous arrivez sur une interface vous permettant de vous enregistrer (bouton orange...)

![](/img/auth-inscription-sso.png)

:::warning
Merci de bien retenir le mail et le mot de passe utilisé.
:::

| Connecté | Image-écran |
| -------- | -------- |
|**Sur le backOffice** Une fois connecté, vous arrivez sur l'accueil ou sur votre profil Carto4CH. Vous avez alors la possibilité **de voir, d'éditer votre profil**.|![](/img/authentification-menu-bo.png)|
|**MyCompetence** ne gère pas le profil de l'utilisateur actuellement||

## Gestion du profil utilisateurice

:::danger
**Attention :**
Comme précisé plus haut, cette partie n'est disponible que dans le **back-office**. Il n'y a pas de profil utilisateur dans My-compétence pour le moment.
:::

Sur le back-office, si vous cliquez sur **"Voir mon profil"**, vous arrivez sur une interface montrant les quelques données fournies lors de l'authentification : 

![](/img/auth-user-profil.png)

Vous avez accès à 3 boutons à droite de votre nom/prénom.

| Bouton | Image-écran |
| -------- | -------- |
| Le bouton **Liste** vous renvoit vers la liste des personnes. |![](/img/auth-list-person.png)|
| Le bouton **Editer** permet de passer en mode ajout/modification de vos données personnelles. Un bouton **Afficher** permet alors de revenir sur l'écran de profil. |![](/img/auth-edit-profil.png)|
| Le bouton **Permission** permet de gérer vos droits d'accès aux données.|![](/img/auth-permission.png)|

## Gestion des permissions

Les applications permettent de gérer les droits d'accès aux informations de manière fine.
Actuellement, la première règle est que **celui qui créer un objet possède tous les droits dessus**. Les autres ont uniquement les droits en lecture seule. 

![](/img/auth-permissions-users.png)

Il peut ensuite ajouter des droits sur cet objet à d'autres personnes.

![](/img/auth-add-user-permission.png)

**Par exemple :** Yannick créer l'objet "FSP", mais se trompe dans la définition.
Beatrice n'a que les droits de lecture seule dessus, et demande les droits en modification à Yannick.

![](/img/auth-permission-by-user.png)

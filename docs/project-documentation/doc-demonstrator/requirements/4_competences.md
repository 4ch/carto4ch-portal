---
title: Compétences et qualités
sidebar_position: 4
---

# Compétences et qualités

:::info
Voir aussi [Pré-requis sur les référentiels](/docs/project-documentation/doc-demonstrator/requirements/referentiels)
:::

## Introduction

Nous proposons de décrire une compétence au travers de différentes facettes et les quatre "qualités", ou sortes de facettes, que nous proposons ont été choisies pour le cadre professionnel du patrimoine culturel. 

Une compétence est décrite par au moins une facette, relevant de l'une des quatre qualités suivantes :
* La qualité **"Discipline"**, ou domaine académique, est utile pour indiquer les connaissances liées à la compétence que nous voulons décrire.
* La qualité **"Secteur"**, ou secteur d'activité, ou encore domaine d'application, permet de préciser le cadre dans lequel la compétence est appliquée. Ce cadre est une forme de connaissance complémentaire.
* La qualité **"Objet d'étude"**, permet d'être éventuellement plus précis encore sur le domaine de connaissances de la compétence.
* La qualité **"Outil"**, permet d'indiquer toute technique, méthode ou outillage dont la compétence dénote la maîtrise.

Par ailleurs les compétences sont liées à un contexte, par exemple, si vous êtes intervenu en tant que chef de projet informatique dans le secteur du patrimoine, c'est différent du fait d'avoir suivi une formation en informatique dans le secteur du patrimoine. La qualité Discipline avec pour valeur informatique sera utile dans les deux compétences. Une même compétence peut être décrite par zéro ou plusieurs diciplines, zéro ou plusieurs secteurs d'application, zéro ou plusieurs objets d'étude, zéro ou plusieurs outils.

Le tableau ci-dessous montre comme exemples deux compétences : 

| Compétences | Qualités | Exemple
| -------- | -------- | -------- |
| Compétence C1 | Discipline D1, Discipline D2 | Chef de projet, Informatique
| Compétence C1 | Secteur S1 | Musées
| Compétence C1 | Objet d'étude OE1 | Dorures au XII ème sciècle
| Compétence C1 | Outil T1, outil T2 | Suite Office, Wordpress
| Compétence C2 | Discipline D2, Discipline D3 | Informatique, Bases de données
| Compétence C2 | Secteur S1, Secteur S2 | Musées, Archives
| Compétence C2 | Pas d'object d'étude | 
| Compétence C2 | Outil T2, outil T3, outil T4 | Wordpress, PHP, PostgreSQL

![](/img/competences-graphviz.png)

## Liste des qualités et définitions

Les définitions sont extraites du [document décrivant l'ontologie HeCo](https://portal.carto4ch.huma-num.fr/docs/technical-documentation/ontology)

| Qualités (FR) | Quality (EN) | Définitions|
| -------- | -------- | -------- |
| Discipline | Discipline | Pour tout ce qui est disciplines académiques et savantes. Ces disciplines peuvent être plus ou moins spécifiques, elles sont traditionnellement organisées en hiérarchies. **Exemple :** « Informatique » est plus général que « Web sémantique ». |
| Secteur | Area | Provient du besoin de distinguer les secteurs du patrimoine. Nous voyons cela comme un domaine d’application (secteur d’activité). **Exemple :** « Informatique » est plus général que « Web sémantique ». |
| Objet d'étude | Studied Object | Est un objet d’étude, plus ou moins précis. Mais ce n’est pas une entité réelle unique, comme un tableau particulier, cette notion est hors champ pour l’instant. **Exemple :** le Moyen-âge, ou les céramiques produites dans la zone géographique X à la période Y…
| Outil | Tool | Est pour indiquer une maîtrise de tout ce qui est outil technique, méthodologique ou conceptuel. **Exemple :** Protégé, PostrgreSQL, Scaner 3D, Plan de Gestion de Données...
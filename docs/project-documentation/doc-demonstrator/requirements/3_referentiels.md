---
title: Référentiels
sidebar_position: 3
---

# Référentiels

![](/img/referentiels-banner.png)

## Introduction
Lors de l'édition des formulaires, vous allez devoir sélectionner des données dans des listes.

Si vous ne trouvez pas votre bonheur dans ces listes, vous pouvez ajouter de nouvelles valeurs. 

C'est ce que nous appelons dans ce projet des "référentiels".

La manière de faire diffère si on utilise le **back-office**, ou **My-competences**.

## Gestion d'un référentiel dans le Back-office
### Via le menu
Dans le **Back-office**, on peut enrichir les référentiels depuis le menu.
```
Menu > Référentiels > disciplin(e)
                    > secteur(s)
                    > Objet(s) d'étude
                    > Outil(s)
                    > Profession(s)
```

La liste des objets que vous allez créer ici correspondra à la liste qui sera affichée dans les compétences.

![](/img/referentiels-competence.png)

### Via les listes de choix des champs de saisie
A la fin de chacune des listes, se trouve la possibilité de "Créer" une nouvelle valeur dans la liste.

![](/img/referentiels-competence-creer.png)

Si vous cliquez sur la ligne "Créer", une popup s'affiche et vous donne la possibilité d'ajouter une valeur et de la sélectionner.

![](/img/referentiels-competence-creer-popup.png)

On entre une nouvelle valeur

![](/img/referentiels-competence-creer-popup-edit.png)

![](/img/referentiels-competence-creer-popup-edit-new.png)

Et en appuyant sur le bouton "Ajouter", cela créer la nouvelle valeur (ici dans le référentiel "Discipline(s)")

![](/img/referentiels-competence-creer-popup-edit-new-input.png)

Vous pourrez ensuite retrouver cette donnée dans la liste des discipline en passant par le menu.

## Gestion des référentiels dans My-competences

### Introduction
Attention : **ces référentiels sont partagés**. Ces données pourront servir pour d'autres personnes.

Par exemple : 
* lors de la création ou de l'édition d'un poste, vous allez pouvoir ajouter une nouvelle organisation, ou une nouvelle profession.
* lors de la création ou de l'édition d'une compétence, vous allez pouvoir ajouter une discipline 

### Via les listes de choix des champs de saisie

Dans My-competences, il est uniquement possible d'ajouter de nouvelles valeurs en cliquant sur "Créer" en bas des listes de choix.
Cela simplifie la saisie pour l'utilisateur, ce qui est l'objectif principal de cette application par rapport au back-office.


---
title: Les instances Carto 4CH
sidebar_position: 1
---

# Les instances Carto4CH

## Généralités
:::info
**"Une instance"** = un serveur de données pour une organisation partenaire, ainsi que ses différentes interfaces spécifiques permettant de gérer les données de l'instance.
:::

Dans le démonstrateur, par exemple, nous avons simulé deux instances : A et B.

Dans la réalité du projet en production, il pourra y avoir de nombreuses instances, appartenant à de nombreuses organisations.

Le Schéma du démonstrateur le montre bien : 

![](/img/schema-demonstrator-A-B.png)

Dans le schéma ci-dessus, vous pouvez constater que chaque instance possède : 
* ses propres **données** (A d'un côté et B de l'autre)
* plusieurs **applications utilisateurices** (My-competence, Back-office ou Carto).

Chaque instance est autonome, techniquement et politiquement, ce qui ne l'empêche pas d'être relié aux autres via l'interface de cartographie. C'est l'avantage d'utiliser des standards de partage d'information entre les serveurs.

## Customisation de chaque instance
En production, les administrateurs de futurs instances pourront la customiser avec : 
* une couleur
* un logo
* un titre
* un paramétrage spécifique (par exemple, ses liens avec une autre instance au niveau de l'interopérabilité)

qui lui donnera sa spécificité.


---
title: Doc. de l'interface carto-competences
sidebar_position: 4
---

# Documentation de l'interface carto-competences

## Introduction

Carto-competences est une interface en lecture seule, affichant les compétences sous forme de **graph** (en réseau).

## Accès à la documentation utilisat.eur.rice

Pour comprendre le fonctionnement de cette interface, rendez-vous sur la page de la [**Visualisation**](/docs/technical-documentation/visualisation)
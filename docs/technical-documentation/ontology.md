---
title: Ontologie
sidebar_position: 4
---

# Ontologie HeCo

## Introduction

Nous décrirons dans cette page l'ontologie Carto4CH. 
Le livrable sera un ensemble de pièces, versionné :
* Le [**format brut (OWL/TTL)**](#format-brut-owlttl) contenant l'ontologie en syntaxe [turtle](https://fr.wikipedia.org/wiki/Turtle_(syntaxe))
* Le [**format WIDOCO (HTML)**](#format-widoco-html) de l'ontologie sous forme de page HTML
* Un [**schéma (SVG)**](#schéma-svg) de l'ontologie
* Un [**résumé (TXT)**](#résumé-txt) les apports de la dernière version
* Un [**document (PDF)**](#document-pdf) contenant l'historique des réflexions depuis la création de l'ontologie

[[Extrait de wikipedia](https://fr.wikipedia.org/wiki/Ontologie_(informatique))]
En informatique et en science de l'information, **une ontologie est** un modèle de données contenant des concepts et relations permettant de modéliser un ensemble de connaissances dans un domaine donné.

Dans notre cas, l'**ensemble de connaissances** à modéliser est représenté par des acteurs et leurs compétences dans le domaine du patrimoine culturel.

Nous avons donné à cette ontologie le nom de **HeCo** (pour **He**ritage **Co**mpetence).

L'objectif de cette ontologie est de formaliser comment nous souhaitons organiser les informations au sein de notre cartographie.
Elle permet aussi de délimiter clairement **le périmètre** du projet. Nous limiterons la cartographie aux concepts et aux relations présents dans cette ontologie.

## Statut

:::info
Publiée en version 1.0.
:::

## Versions

| Version | Date |Description|Commentaires|
| -------- | ----- | -------- | ------- |
| 1.0 | 01/09/2023 | Ajout de toutes les relations inverses utilisées | Version stable qui peut être utilisée dans un cycle de production et de maintenance.
| 0.9 | 21/08/2023 | Mise à jour de quelques relations | Lien entre les contributions et les projets.
| 0.8 | 07/03/2023 | Représentation des deux modes possibles pour les facettes de compétences | Cette version montre une certaine stabilité, notre objectif est que le démonstrateur soit le plus aligné possible avec cette version.
| 0.7 | 27/01/2023 | Précisions sur les "postes", "formations", "contribution", et sur les qualités | Du côté du démonstrateur, cette version accompagne l'abandon des relations réifiées
| 0.6 | 25/11/2022 | Simplification par représentation des facettes comme des "qualities" | Cette version est liée à l'utilisation des relations réifiées.
| 0.5 | 07/11/2022 | Prise en compte des formations |Cette version est la première à être publiée aux partenaires |
| 0.1 à 0.4 | 01/09/2022 | Première version | Versions de travail en interne, pour commencer l'implémentation dans SemApps |

## Livrable

### Format brut (OWL/TTL)

Le document **heco.owl** a été édité avec l'outil [Protege](https://protege.stanford.edu/).
Nous le stockons sur le projet [Gitlab Carto4CH](https://gitlab.huma-num.fr/4ch), afin qu'il puisse être édité collectivement.

* [Lien vers le fichier OWL sur le Gitlab](https://gitlab.huma-num.fr/4ch/heco/-/raw/main/heco.owl)

### Format WIDOCO (HTML)
Le logiciel WIDOCO (WIzard for DOCumenting Ontologies), permet de fournir une interface HTML lisible plus facilement par un humain.

* [Lien vers le format WIDOCO (HTML)](pathname:///widoco/index-fr.html)

### Schéma (SVG)
* Retrouvez les schémas versionnés de l'ontologie [dans la rubrique Schémas > Ontologie](/docs/schemas/ontology)

### Résumé (TXT)

Les acteurs restent modélisés comme dans CIDOC CRM. Trois relations sont définies entre les personnes et les groupes, lesquels représentent des projets, des associations, des organisations, etc. Ces trois relations peuvent être caractérisées par une date de début et de fin et par les compétences acquises : la première, Job, représente une relation d’employé à employeur et est caractérisée par un type d’emploi (JobType) et l’intitulé de l’emploi (Occupation). La deuxième, Commitment, représente une relation d’engagement dans un projet, une association, ou autre type de groupe. La troisième, Training, représente une relation de formation par un groupe, elle peut être caractérisée par un diplôme, certificat ou autre.

Une personne peut avoir déclaré zéro ou plusieurs compétences. Une compétence est définie par plusieurs facettes, une facette pouvant spécifier une connaissance (une discipline scientifique comme l'archéologie, un secteur d'activité comme le secteur des archives, ou un objet d'étude comme le travail de la pierre par les bâtisseurs de cathédrales), ou une maîtrise d'outil, de méthode, de technique, ou encore une compétence comportementale. Une facette peut aussi définir un niveau de compétence dans une échelle donnée, comme EQF. Dans la version v0.8, deux représentations co-existent : une représentation générique avec les facettes (chacune composée d’un type et d’une valeur) et la représentation adoptée dans les codes de la première version de carto4CH, où seulement quatre facettes sont représentées et chacune par une classe et la propriété correspondante : Discipline, Area, StudiedObject, et Tool.

### Document (PDF)
Accéder au document de conception progressive de l'ontologie HeCo

Document complet (en PDF)
* [Ontologie HeCo 1.0](/documents/PresentationHeCoV1.0.pdf)

## Utilisat.eur.rice.s
Elle servira de support :
* **pour les partenaires du projet Carto4CH :** pour collectivement faire évoluer la modélisation de la cartographie, dans le temps, en fonction des besoins
* **pour les développeu.r.se.s du projet Carto4CH :** qui s'en serviront de modèle de données pour programmer leur accès et implémenter des interfaces

## Publication
Nous souhaitons publier cette ontologie :
* d'abord au cercle restreint des partenaires du projets
* si elle est adoptée pour le projet, elle pourra faire l'objet d'une publication et d'une communication plus large au niveau français
* si elle est utilisée par plusieurs acteurs français, elle sera proposée au projet 4CH

Le projet 4CH travaille aussi sur d'autres ontologies, il faudra certainement travailler l'alignement de toutes ces ontologies, pour qu'elle fonctionnent ensembles.

## Méthode
La méthode utilisée dans ce projet se veut agile, itérative et collaborative.

## Veille
Depuis le mois d'avril 2022, nous avons effectué une veille sur les ontologies et les référentiels existants, au niveau mondial, européen et français.
Nous expliquerons dans le document ci-dessous ce que nous avons retenu dans chacun de ces projets.

A cette veille, nous avons ajouté les pièces manquantes, spécifiques à notre projet.

## Implémentation
Puis, nous avons commencé à implémenter dans l'outil [SemApps](/docs/technical-documentation/presentation-semapps) les concepts et les relations présentes dans cette ontologie, pour nous rendre compte des limitations, des oublis, des nouveaux besoins.

Des adaptations régulières du logiciel et de l'ontologie nous aiderons à trouver l'équilibre dans le temps.

Une interface préliminaire de saisie des compétences est développée pour évaluer les choix de conception de l'ontologie. Les partenaires seront sollicités pour utiliser cette interface lors des ateliers de compétences.

## Ateliers compétences
Des [ateliers collaboratifs](../workshops) seront proposés régulièrement aux partenaires pour réfléchir collectivement à l'évolution de cette ontologie, et donner leur avis sur l'interface de saisie.

## Mise en production
Une fois cette phase d'évaluation stabilisée, des instances de production pourront être déployées.

Dans cette dernière phase du projet, les évolutions de l'ontologie seront minimes (car auront un impact plus important en production)

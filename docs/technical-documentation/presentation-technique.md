---
title: Présentation
sidebar_position: 2
pagination_prev: null
---

# Présentation

## Introduction
Cette présentation décrit l'architecture technique proposée.

## Accéder à la présentation générale

Pour le moment, vu que cette présentation est encore jeune et peut être amenée à être modifiée, nous utilisons un PAD collaboratif pour la mettre à jour plus facilement.
* [Lien en mode présentation](https://pad.lescommuns.org/p/SlideInteroperabiliteSemapps)
* [Lien vers la source](https://pad.lescommuns.org/SlideInteroperabiliteSemapps)

(Si vous souhaitez participer au contenu, merci de nous contacter).

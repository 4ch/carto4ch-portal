---
title: Visualisation en réseau
sidebar_position: 8
---

# Visualisation en réseau 

## Introduction
Nous expliquerons ici comment fonctionne la **visualisation en réseau** utilisée pour Carto 4CH.

|Schéma|Présentation|
|----|----|
|![](/img/visualisation-flodio.png)|La visualisation en réseau permet d'apporter une 3ème manière de parcourir les données et de rechercher des compétences|

## FLODIO
Cette visualisation utilise le logicicel [Flodio](https://flod.io) et le concept du [FLOD](https://pad.lescommuns.org/flodio) pour naviguer dans le graphe des données.

Flodio interroge directement les données conformes à l'ontologie **HeCo** sur une des instances Carto4CH, et utilise la librairie **D3JS** pour afficher les données dans un graphe.

Il est possible de filtrer les données par le menu en haut à gauche, et de rechercher via le moteur de recherche en haut à droite.

## Fonctionnement pour le démonstrateur
Pour le démonstrateur, nous utilisons pour l'instant le service [Vercel](https://vercel.com/yannickduthe-hotmailcom/cartosemapps), qui permet de publier le code Javascript du projet [CartoSemapps](https://gitlab.com/fluidlog/cartosemapps).

Chaque instance A/B/C pointe sur une branche du projet **CartoSemapps**.

## URL d'accès
Voir les liens sur le [Démonstrateur Carto 4CH](/docs/applications/demonstrator)
---
title: Manuel de développement du démonstrateur
sidebar_position: 5
---

# Manuel de développement du démonstrateur

![](/img/demonstrator-manual.png)

## Introduction

Cette documentation est destinée aux développeur.euse.s souhaitant comprendre comment fonctionne le démonstrateur, ou comment implémenter leur propre serveur sur la base de la boite à outil [SemApps](/docs/technical-documentation/presentation-semapps).

Nous expliquerons surtout de quels projets originaux il est constitué, et quels sont les surcharges spécifiques à ce projet.

Cette documentation n'exipliquera pas le fonctionnement interne de chaque projet original, pour cela, voir la [présentation SemApps](/docs/technical-documentation/presentation-semapps).

Pour en savoir plus sur le déploiement de ce démonstrateur, voir la section [Déploiement du démonstrateur](/docs/technical-documentation/demonstrator-deploy)

## Pré-requis techniques

* Comprendre le **web sémantique** et l'[ontologie HeCo](/docs/technical-documentation/ontology)
* Avoir intégré la documentation technique de [SOLID](/docs/technical-documentation/standards/presentation-solid)
* Avoir intégré la documentation technique de [SemApps](/docs/technical-documentation/presentation-semapps)
* Maitriser les protocoles : 
  * [LDP](https://www.w3.org/TR/ldp/)
  * [SPARQL 1.1](https://www.w3.org/TR/sparql11-protocol/)
  * [Activity Pub](https://www.w3.org/TR/activitypub/)

## Rappel de l'architecture générale

|Rappel de l'architecture|Les briques documentées|
|----|----|
|![](/img/demonstrator-architecture-schema.png)|Le **démonstrateur** est une cartographie fonctionnelle de démonstration.<br/>Son objectif est de donner un exemple de ce qui est attendu par le projet Carto4CH.<br/><br/>Nous expliquerons dans cette documentation l'hébergement de ce démonstrateur et le fonctionnement de chacune de ses briques : <br/><ul><li>**Les deux middlewares** de démonstration (Instance A et Instance B)</li><li>**Les 3 applications** permettant de cartographier les données dans chaque instance</li><li>L'**instance miroir**</li><li>Les **visualisation en réseau**</li></ul>|

## Fork des projets originaux

|Projets d'origines|Customisation|
|----|----|
|![](/img/demonstrator-forks.png)|Chaque projet du **démonstrateur** est un fork d'un projet original maintenu par l'Assemblée virtuelle (AV).<br/><br/>- Archipelago, <br/>- Minicourse <br/>- CartoSemapps. <br/><br/>Cela permet au projet Carto4CH d'être doublement maintenu, à la fois par les membres de l'équipe 4CH, et par les membres de l'association AV<br/>|

## Les deux instances de middleware A et B

### Architecture du middleware
Comme décrit dans la [Présentation technique SemApps](/docs/technical-documentation/presentation-semapps), le middleware contient **la base de données**, et **un orchestrateur Javascript** (moleculer) qui organise le lancement de différents services.

### Services moleculer utilisés
Lorsqu'on lance le middleware avec ```yarn run dev```, il lance le script ```moleculer-runner --repl --hot services``` (voir fichier package.json), qui lance les services et les place en attente. La liste des services SemApps est présente dans la [Documentation officielle SemApps](https://semapps.org/docs/middleware). Dans le projet Carto 4CH, nous utilisons : 
* **auth** : gestion de l'authentification
* **backup** : pour effectuer régulièrement un backup de la base
* **core** : qui est le premier service qui ensuite appelle les autres
* **inference** : qui s'occupe des inférences et dans notre cas, des relations inverses
* **sync** : qui s'occupe des synchronisations et des miroirs
* **webacl** : gère les permissions dans la base
* **webid** : gère les profils web ID.

Tous ces services peuvent aussi en instancier d'autres. Par exemple, le **core** lance aussi le service **activity pub** si besoin, il n'y a pas besoin de l'ajouter dans le fichier **packages.json**.

### Les spécificités des middlewares Carto 4CH
#### Peu de surcharges
D'une manière générale, nous n'avons pas beaucoup touché au code du middleware. Nous sommes restés assez fidèles au code [Archipelago d'origine](https://github.com/assemblee-virtuelle/archipelago) à part quelques spécificités liées aux containers et à l'ontologie utilisée (+ contexte).

#### Containers
Nous avons déclaré des containers utiles à la gestion des compétences. Pour cela, nous avons modifié le fichier **config/containers.js**.

#### Ontologie HeCo et context.json
Nous avons aussi ajouté l'URI de l'ontologie HeCo dans la liste du fichier **config/ontologies.json**

Nous avons aussi ajouté un fichier **context.json** contenant la liste des relations (prédicats) utilisés. Pour cela, nous avons dû modifier les fichiers **core.service.js** et **jsonld.service.js** pour préciser le nouveau contexte local à utiliser.

#### Gestion des relations entre les compétences et les P/C/F
Pour les besoins du projet, nous avons modifié le service **LDP**.
Nous avons ajouté un hook dans le fichier **ldp.service.js**, lors du GET, du PUT et du CREATE, pour modifier les prédicats utilisés.

## Les 2 back-office(s)

### Archipelago
Ces 2 frontend (bo-a et bo-b) utilisent [l'interface back-office d'Archipelago](https://github.com/assemblee-virtuelle/archipelago).

Pour plus de détails sur le projet Archipelago, vous pouvez lire [la présentation SemApps](/docs/technical-documentation/presentation-semapps)

### Les spécificités de Archipelago-carto4CH

Beaucoup de surcharges ont été apportées à ce projet.
* La customisation du thème est gérée par la config docker
* Gestion de l'interopérabilité.
* Gestion des postes, formations et contributions
* Gestion des projets
* Gestion des compétences en respectant l'ontologie HeCo
* Gestion des qualités, des professions
* Interrogation des référentiels ESCO et Wikidata
* Formulaires en une page
* Titre automatique pour les compétences
* Relation entre les compétences et les P/C/F

:::caution Attention
Régulièrement, Archipelago-Carto4CH a récupéré les mises à jour : 
* d'Archipelago AV, 
* des composants SemApps, 
* des évolutions de React-Admin (surtout le passage en V4)
:::

## Documentation utilisateurice du démonstrateur

* Voir : [Documentation utilisateurice du démonstrateur](/docs/category/doc-demonstrator-category-label-2)
 
---
title: Intégration dans le cloud 4CH
sidebar_position: 10
---

# Intégration dans le cloud 4CH

```
┌───────────────────────────────────┐
│  Carto4CH    ===>     Cloud 4CH   |
│-----------------------------------│
│                         ___       │
│     ___                /___\      │
│    /   \              /     \     │
│   |     |    ===>    /_______\    |
│    \___/            /         \   |
│                    └───────────┘  |
│                                   |
└───────────────────────────────────┘
```

## Introduction
Nous expliquerons ici comment fonctionne l'**intégration** de Carto4CH dans le cloud de 4CH.

## Compréhension des pré-requis
L'équipe de l'INFN nous a transmis des documents contenant leurs pré-requis techniques pour intégrer le cloud 4CH.

Il en a résulté que nous devions garder notre attention sur les points suivants : 
* Accès aux référentiels extérieurs
* Sauvegarde externe
* Authentification via INDIGO-IAM / Keycloak
* Config NGINX
* Versionning

## Echanges avec l'équipe INFN
Nous avons validé avec l'équipe que la solution SemApps déployée via docker, et compatible OIDC était tout à fait intégrable dans le cloud 4CH.

## Premiers tests
Malheureusement, lors des tests d'[**authentification**](/docs/technical-documentation/authentication), nous n'avons pas réussi à configurer correctement le client OIDC pour pouvoir nous authentifier correctement.

:::caution
Ces tests ont eu lieu mi-décembre 2023, et l'équipe de l'INFN n'a pas pu nous aider avant la fin du projet.
**Statut :** A terminer si possible lorsque le projet reprendra sous forme d'une association.
:::
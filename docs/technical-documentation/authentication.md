---
title: Authentification / SSO
sidebar_position: 9
---

# Fonctionnement de l'authentification / SSO 

```
┌────────────────────────────────────────────┐
│           SSO / OIDC / SOLID-OIDC          |
│--------------------------------------------│
│       Application            Server OIDC   |
│   ┌───────────────┐                        │
│   │  ┌─────────┐  │                        │
│   │  |  Login  │  │          ┌─────────┐   │
│   │  └─────────┘  │  <====>  |  OIDC   │   │
│   │  ┌─────────┐  │          └─────────┘   │
│   │  |  pass   │  │                        │
│   │  └─────────┘  │                        |
│   └───────────────┘                        │
└────────────────────────────────────────────┘
```

## Introduction
Nous expliquerons ici comment fonctionne l'**authentification** dans Carto 4CH, quelles sont les limites actuelles, et les prochaines évolutions.

## Les différents protocoles
### OpenID Connect (OIDC)
Le protocole OIDC est un des protocole les plus utilisé en matière de SSO (Single Sign On).

Voir [**Standards > OIDC**](/docs/technical-documentation/standards/oidc)

### Central Authentication Service (CAS)
Le protocole CAS est un autre protocole d'authentification que l'on peut trouver fréquemment.

https://fr.wikipedia.org/wiki/Central_Authentication_Service

## Des exemples de serveurs d'authentification
### Serveur OIDC des communs
Nous utilisons actuellement le serveur OIDC des communs (https://login.lescommuns.org/). C'est un serveur dont la gouvernance est partagée par plusieurs organisations, sur lequel nous avons installé le logiciel open-source [Keycloak](https://www.keycloak.org/).

SemApps propose ce type d'authentification par défaut, mais nous pouvons aussi paramétrer d'autres protocoles, comme **CAS**.

### Le serveur d'authentification de l'université de Tours
Lorsque nous souhaitons nous connecter à un service numérique de l'université de Tours, nous tombons sur une demande d'authentification.

https://cas.univ-tours.fr/cas/login

Ce serveur utilise le protocole CAS.
Nous avons demandé à l'université si nous pouvions l'utiliser pour Carto4CH, mais a priori, il est réservé aux applications utilisées par l'université, et pas pour des projets de recherche.

### Le serveur d'authentification de 4CH

Le projet 4CH, ou plutôt l'[INFN](https://fr.wikipedia.org/wiki/Istituto_nazionale_di_fisica_nucleare), utilisent un serveur SSO appelé INDIGO-IAM, qui utilise le protocole OIDC.

Nous avons essayé de remplacer le serveur OIDC des communs par celui de l'INFN dans SemApps.

Malheureusement, nous avons rencontré des problèmes techniques qui n'ont pas pu être réglé avant la fin du projet.

## Les améliorations à venir dans SemApps
Voir [**Recommandations > Authentification**](/docs/recommendations/authentication)

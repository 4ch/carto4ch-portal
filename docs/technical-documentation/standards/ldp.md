---
title: LDP
sidebar_position: 2
---

# LDP (Linked Data Platform) 

```
┌────────────────────────────────────────────┐
│        Linked Data Platform / Protocol     |
│--------------------------------------------│
│       LDP Server             Application   |
│   ┌───────────────┐                        │
│   │  ┌─────────┐  │                        │
│   │  |  LDP    │  │                        │
│   │  | service │  │   HTTP   ┌─────────┐   │
│   │  └─────────┘  │  <====>  |  App    │   │
│   │      v ^      │   GET    └─────────┘   │
│   │  ┌─────────┐  │   POST                 │
│   │  | RDF data│  │   PATCH                │
│   │  └─────────┘  │   PUT                  |
│   └───────────────┘   DELETE               │
└────────────────────────────────────────────┘
```

## Introduction
Nous expliquerons ici le concept **Linked Data Platform (LDP)** utilisé par la boite à outils SemApps.

Vous pouvez déjà lire la page suivante : 
https://en.wikipedia.org/wiki/Linked_Data_Platform

## En résumé
C'est une manière normalisée d'accéder à des données RDF sur un serveur (données liées).

Un serveur est appelé "serveur LDP", ou "plateforme LDP", lorsqu'il est possible de l'interroger et de le mettre à jour en utilisant uniquement des commandes [HTTP REST](https://fr.wikipedia.org/wiki/Representational_state_transfer).

## Exemple
Par exemple, **Archipelago** est un serveur LDP, car il expose ses données via [un service LDP](https://semapps.org/docs/middleware/ldp/resource).

Le plus simple pour l'observer, est de faire une requête GET en utilisant votre navigateur sur le conteneur LDP "organization" du serveur **Carto4ch-a**.

https://data-a.carto4ch.huma-num.fr/organizations

Vous obtenez alors une réponse immédiate contenant la liste de toutes les organisations.

```
{
  "@context": [
    "https://www.w3.org/ns/activitystreams",
    "https://data-a.carto4ch.huma-num.fr/.well-known/context.jsonld"
  ],
  "id": "https://data-a.carto4ch.huma-num.fr/organizations",
  "type": [
    "ldp:Container",
    "ldp:BasicContainer"
  ],
  "ldp:contains": [
    {
      "id": "https://data-a.carto4ch.huma-num.fr/organizations/ministere-de-la-culture",
      "type": "heco:Organization",
      "dc:created": "2022-04-22T20:50:34.132Z",
      "dc:creator": "https://data-a.carto4ch.huma-num.fr/users/yannickduthe",
      "dc:modified": "2022-04-25T08:20:28.9Z",
      "pair:hasType": "https://data-a.carto4ch.huma-num.fr/types/institution",
      "pair:homePage": "www.culture.gouv.fr/",
      "pair:label": "Ministère de la culture",
      "heco:hasTopic": "https://data-a.carto4ch.huma-num.fr/themes/culture"
    },
```

## Spécifications du W3C
Les techniciens pourront lire les spécifications à l'adresse : 
https://www.w3.org/TR/ldp/

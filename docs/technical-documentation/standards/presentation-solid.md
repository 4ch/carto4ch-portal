---
title: SOLID
sidebar_position: 3
---

# SOLID

## Introduction
Cette présentation explique le standard SOLID (du W3C) qui sera utilisé dans ce projet.

Pour le [démonstrateur Carto4ch](/docs/applications/demonstrator), nous utiliserons le logiciel [SemApps](/docs/technical-documentation/presentation-semapps), qui implémente ce standard.

## Présentation SOLID

:::info Accès
* [Lien en mode présentation](https://pad.lescommuns.org/p/SlideSolid)
* [Lien vers la source](https://pad.lescommuns.org/SlideSolid)
:::

:::caution
Pour le moment, vu que cette présentation est encore jeune et peut être amenée à être modifiée, nous utilisons un **PAD collaboratif** pour la mettre à jour plus facilement.
:::

(Si vous souhaitez participer au contenu, merci de nous contacter).

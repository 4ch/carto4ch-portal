---
title: OIDC
sidebar_position: 2
---

# OIDC 

```
┌────────────────────────────────────────────┐
│           SSO / OIDC / SOLID-OIDC          |
│--------------------------------------------│
│       Application            Server OIDC   |
│   ┌───────────────┐                        │
│   │  ┌─────────┐  │                        │
│   │  |  Login  │  │          ┌─────────┐   │
│   │  └─────────┘  │  <====>  |  OIDC   │   │
│   │  ┌─────────┐  │          └─────────┘   │
│   │  |  pass   │  │                        │
│   │  └─────────┘  │                        |
│   └───────────────┘                        │
└────────────────────────────────────────────┘
```

## Introduction
Nous expliquerons ici le protocole OIDC utilisé par SemApps pour l'[**authentification**](/docs/technical-documentation/authentication).

https://fr.wikipedia.org/wiki/OpenID_Connect

## En résumé
Le protocole OIDC est un des protocoles d'authentification les plus utilisé (il existe aussi CAS).

Il peut permettre à un ensemble d'applications, de gérer à un seul endroit les données personnelles des utilisateurs et les habilitations d'accès aux applications. C'est ce qu'on appelle aussi le **SSO** (Single Sign On) ou [Authentification Unique](https://fr.wikipedia.org/wiki/Authentification_unique).

## Utilisation dans SemApps
L'Assemblée virtuelle est partenaire d'un projet de serveur OIDC utilisant le logiciel **Keycloak**. C'est un moyen de gérer l'authentification dans le projet Carto4CH.

Plus d'informations sur la page [**authentification**](/docs/technical-documentation/authentication)

:::caution
Statut : A améliorer
:::
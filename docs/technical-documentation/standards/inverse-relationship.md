---
title: Relations inverses
sidebar_position: 1
---

# Relations inverses 

```
┌────────────────────────────────────────────┐
│              Relations inverses            |
│--------------------------------------------│
│   ┌─────────┐                ┌─────────┐   │
│   │    A    │      ====>     |    B    |   │
│   └─────────┘                └─────────┘   │
│   ┌─────────┐   inverse of   ┌─────────┐   │
│   │    B    │      ====>     |    A    |   │
│   └─────────┘                └─────────┘   │
└────────────────────────────────────────────┘
```

## Introduction
Nous expliquerons ici la notion de "relation inverse" telle qu'elle est utilisée actuellement dans Archipelago.

## Qu'est-ce qu'une relation inverse ?
En web sémantique, nous utilisons des triplets pour représenter une information : "Subject > Predicate > Object".
La relation inverse sera donc l'information contraire "Object > Predicate > Subject".
Il faudra donc trouver des prédicats inverses pour une grande majorité des relations de l'[ontologie Heco](/docs/technical-documentation/ontology).

## Exemple

```
┌────────────────────────────────────────────┐
│                  Exemple                   |
│--------------------------------------------│
│   ┌─────────┐                ┌─────────┐   │
│   │ Person 1│ hasCompetence  |    C1   |   │
│   └─────────┘                └─────────┘   │
│   ┌─────────┐                ┌─────────┐   │
│   │    C1   │ isCompetenceOf | Person 1|   │
│   └─────────┘                └─────────┘   │
└────────────────────────────────────────────┘
```
Pour décrire dans l'**ontologie Heco** la relation "une personne a une compétence", il faudra aussi décrire qu'une compétence est liée à une personne.

Cela permettra, dans l'interface, d'afficher : 
* La liste des compétences d'une personnes
* mais aussi la liste des personnes qui possèdent cette compétence

## Comment le préciser dans l'ontologie ?
Pour cela, on utilise la relation empruntée au [language OWL](https://fr.wikipedia.org/wiki/Web_Ontology_Language) : "owl:inverseOf".
Nous commençons par préciser que la propriété **hasCompetence**, permet de relier la classe **Person** à la classe **Competence**, et que cette propriété possède aussi une relation inverse **isCompetenceOf**.

Cela s'écrit ainsi : 

```
:hasCompetence rdf:type owl:ObjectProperty ;
               owl:inverseOf :isCompetenceOf ;
               rdfs:domain :Person ;
               rdfs:range :Competence ;
```

Et dans l'autre sens, nous devrons préciser que que la propriété **isCompetenceOf**, permet de relier la classe **Competence** à la classe **Person**, et que cette propriété possède aussi une relation inverse **hasCompetence**. 
```
:isCompetenceOf rdf:type owl:ObjectProperty ;
                owl:inverseOf :hasCompetence ;
                rdfs:domain :Competence ;
                rdfs:range :Person ;
```

## Fonctionnement des relations inverses dans SemApps

Pour le moment, SemApps utilise le service [Inference](https://semapps.org/docs/middleware/inference) pour extraire les relations inverses de l'ontologie.

Lors de l'enregistrement d'un triplet, il interroge sa liste de relations inverses, et si la classe est concernée, il génère automatiquement le triplet associé dans la base.

De la même manière, il doit retirer les triplets inverses lors d'une demande de suppression d'une information.
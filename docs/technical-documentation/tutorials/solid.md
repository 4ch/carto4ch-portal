---
title: SOLID
sidebar_position: 2
pagination_prev: null
---

# Tutoriel SOLID

## Introduction

Le tutoriel SOLID est une application écrite en React qui interroge des données sur un ou plusieurs POD SOLID.

Nous vous conseillons dans un premier temps de cloner le code du projet et de le lancer en local sur votre machine pour suivre pas à pas ce tutoriel.

La documentation est là pour vous guider dans la compréhension du code.

## Statut

:::tip Statut
Ce tutoriel est considéré comme **terminé** (même s'il a des possibles améliorations, ce n'est qu'un tutoriel, pas une application de production).
:::

## Résumé
Ce tutoriel contient :
* un premier tuto listant simplement le contenu d'un fichier turtle présent dans un POD Solid
* un deuxième qui affiche un formulaire permettant de lister choisir une uri et d'afficher les utilisateurs de différents POD Solid
* un troisième qui affiche et modifie la signification des termes d'un lexique. Ces termes sont stockés au format SKOS dans un POD Solid
* Un quatrième qui permet d'ajouter des nouveaux termes ou de les supprimer

Le [Lexique Carto4CH](/docs/project-documentation/glossary.mdx) utilise d'ailleurs la liste des termes présents dans les Tutoriels 3 et 4.

N'hésitez pas à nous contacter si vous avez des questions.

## Documentation

Pour l'instant, la documentation est stockée sur un PAD collaboratif, elle sera déposée ici lorsqu'elle sera stabilisée.
* Documentation : https://pad.lescommuns.org/Solid-Tutos

## Accès au Tutoriel

* Application fonctionnelle : https://4ch.gitpages.huma-num.fr/solid-tutos (publication du tuto4)
* Code du tutoriel : https://gitlab.huma-num.fr/4ch/solid-tutos

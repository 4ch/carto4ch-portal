---
title: SemApps
sidebar_position: 3
pagination_next: null
---

# Tutoriel SemApps

## Introduction

Le tutoriel SemApps est une application écrite en React qui interroge des données sur un ou plusieurs serveurs SemApps.

## Statut

:::caution Attention
Ce tutoriel est à ses débuts, n'hésitez pas à nous contacter si vous avez des attentes particulières.
:::

## Accès au Tutoriel

* Application fonctionnelle : https://semapps-tutos.vercel.app/
* Github : https://github.com/fluidlog/semapps-tutos

## Documentation

Pour l'instant, la documentation est sur un PAD collaboratif, elle sera déposée ici lorsqu'elle sera stabilisée :
* https://pad.lescommuns.org/semapps-tutos

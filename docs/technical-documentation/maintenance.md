---
title: Maintenance
sidebar_position: 11
---

# Maintenance d'un serveur Carto4ch

## Introduction
Nous expliquerons ici comment maintenir dans le temps : 
* le démonstrateur
* un serveur partenaire Carto4CH.

Principalement, prendre soin de régulièrement compacter les fichiers du triplestore, et d'effectuer des sauvegardes régulières de la base et des images.

## Accès à l'interface Web du triplestore
Afin d'observer ou d'intervenir directement sur les triplets en base, il faut **créer un canal SSH** pour ouvrir un port spécifique sécurisé sur le serveur du démonstrateur.

```
ssh user@carto4ch.huma-num.fr -N -f -L 4040:carto4ch.huma-num.fr:3030
```
(en remplaçant "user" par votre utilisateur)

Ensuite, vous pouvez accéder à l'interface Jena Fuseki via l'url : ```http://localhost:4040/```

![](/img/acces-jena-fuseki-web.png)

Vous pouvez alors découvrir les données, effectuer des requêtes SPARQL et administrer les jeux de données (dataset).

## Compactage

### Problème connu sur Jena Fuseki
La base Jena/fuseki souffre d'obésité. Il faut de temps en temps l'arrêter, et lancer une commande de compatage, pour ne pas que les fichiers de la base prennent trop de place sur le disque.

### Solution, le script de compactage
Pour cela, tout est indiqué dans la [documentation SemApps > Triple Store > Compacting datasets](https://semapps.org/docs/triplestore/compacting-datasets)

Par exemple, pour le démonstrateur, nous avons ajouté (et testé) le script compact.sh suivant : 

```
#!/bin/bash

# Add /usr/local/bin directory where docker-compose is installed
#PATH=/usr/sbin:/usr/bin:/sbin:/bin:/usr/local/bin

cd /home/semapps/carto4ch

# Stop all containers including Fuseki
/usr/bin/docker compose -f docker-compose-prod.yaml stop fuseki

# Run fuseki compact with same data as prod
/usr/bin/docker run --volume="$(pwd)"/data/fuseki:/fuseki --entrypoint=/docker-compact-entrypoint.sh semapps/jena-fuseki-webacl

/usr/bin/docker compose -f docker-compose-prod.yaml up -d fuseki

echo "Cron job finished at" $(date)
```

### Explication du script
* nous arrêtons le container fuseki
* nous lançons le script docker-compact-entrypoint.sh (contenu dans le container fuseki)
* nous redémarrons le container fuseki
* nous écrivons que tout s'est bien passé.

### Lancement du script régulièrement en CRON
Ce script pourra être ajouté à la crontab avec ```crontab -e``` en lançant le compactage, par exemple toutes les nuits (à 3h du matin) : 

```
0 3 * * * /home/semapps/carto4ch/compact.sh >> /home/semapps/carto4ch/cron-compact.log 2>&1
```

Un log attestera que tout s'est bien déroulé.

## Sauvegarde

### Présentation
Pour éviter de perdre les données, soit celles du démonstrateur, soit celles d'un serveur Carto4ch d'un partenaire, il est important de pouvoir mettre en place des sauvegardes régulières de la base.

### Documentation
Le mécanisme de backup utilisé dans SemApps est décrit ici : 
https://semapps.org/docs/middleware/backup

### Tests de sauvegarde externe en SFTP
Dans le cas du démonstrateur, nous n'avons qu'un seul serveur et celui-ci n'est pas en "production", donc nous n'avons pas installé de mécanisme de sauvegarde en production. 

Par contre, nous avons testé le mécanisme afin de pouvoir le décrire ici, pour les partenaires qui souhaiteraient le mettre en place.

Pour cela, nous avons : 
* généré les fichiers de sauvegarde en utilisant le service **Backup** de SemApps
* paramétré un serveur FTP externe (qui ne sera pas décrit ici)
* utilisé un transfert de fichier via [**SFTP**](https://fr.wikipedia.org/wiki/SSH_File_Transfer_Protocol).

### 2 étapes
Nous allons donc expliquer ici : 
* comment générer un backup de la base Jena Fuseki
* comment sauvegarder régulièrement ce backup sur un serveur externe en utilisant les fonctionnalités de SemApps

### Génération du backup
#### Génération manuelle
Il est possible de générer un backup manuellement depuis l'interface web de Jena Fuseki

![](/img/backup-jena-fuseki.png)

#### Génération automatique
Comme décrit dans la [documentation du service backup de SemApps](https://semapps.org/docs/middleware/backup), il est possible d'automatiser la sauvegarde d'un répertoire via le CRON.

Nous avons d'abord ajouté un **utilisateur carto4ch** sur le serveur FTP distant.

Test d'un sftp depuis le démonstrateur sur le serveur distant : 

![](/img/sftp.png)

Il faut savoir que Fuseki stocke ses fichiers de backup dans le container docker, dans le répertoire ```/app/fuseki-backups```

Il est donc important de publier ce répertoire au niveau des volumes docker : 

```
  data-a:
...
    volumes:
...
   - ./data/fuseki/backups:/app/fuseki-backups
```

Ensuite, nous mettons à jour (si besoin) le fichier **backup.service.js** dans le middleware pour lui préciser à quelle heure lancer la création des fichiers de backup. Attention à ne pas laisser la même heure que celle du compactage (vue précédemment).

```
...
    cronJob: {
      time: '00 04 * * *', // Every night at 4am
      timeZone: 'Europe/Paris'
    }
```

Enfin, nous mettons à jour le fichier ```.env.local``` du middleware, en précisant : 
* l'utilisateur ftp (créé sur le serveur FTP distant)
* son mot de passe
* l'url du serveur FTP
* le répertoire où déposer les fichiers
* dans le cas de nos tests, nous ne précisons pas le numéro de port
* le répertoire de backup où se trouvent les fichiers à transférer

```
# Backup server (Optional)
SEMAPPS_BACKUP_SERVER_USER=carto4ch
SEMAPPS_BACKUP_SERVER_PASSWORD=*****
SEMAPPS_BACKUP_SERVER_HOST=url-of-ftp-server
SEMAPPS_BACKUP_SERVER_PATH=/home/carto4ch/backups
SEMAPPS_BACKUP_SERVER_PORT=
SEMAPPS_BACKUP_FUSEKI_DATASETS_PATH=/app/fuseki-backups/
```

:::caution Attention
Ajouter un CRON pour supprimer régulièrement les fichiers dans le répertoire ```/app/fuseki-backups``` (c'est à dire ```data/fuseki/backups```)
:::


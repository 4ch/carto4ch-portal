---
title: Déploiement d'un serveur
sidebar_position: 7
---

# Déploiement d'un serveur Carto4CH

## Introduction
Nous expliquerons ici comment **déployer et personaliser un serveur Carto4CH de production** chez un partenaire du projet 4CH.

## Statut

:::info
Le package de déploiement est opérationnel.

N'hésitez pas à nous contacter si vous souhaitez en installer un.
:::

## Forks + Customisation

|Projets d'origines|Forks + customisation|
|----|----|
|![](/img/carto4ch-server-forks.png)|Chaque sous-projet d'un **serveur Carto4CH** est un fork d'un projet original maintenu par l'Assemblée virtuelle (AV).<br/><br/>- Archipelago, <br/>- Minicourse<br/><br/>Cela permet au projet Carto4CH d'être doublement maintenu, à la fois par les membres de l'équipe 4CH, et par les membres de l'association AV<br/>|

## Déploiement via Docker

|Projets d'origines|Git clone + AddOn|
|----|----|
|![](/img/carto4ch-server-docker.png)|Chaque projet docker d'un **serveur Carto4CH** effectue un **git clone** des projet **Archipelago-Carto-4ch** (middleware et frontend) ou **My-competences**.<br/><br/>Ensuite, il récupère ses fichiers spécifiques dans le répertoire **AddOn** pour customiser les couleurs, le titre ou les paramètres d'interopérabilité|

## Briques déployées
|Schéma|Présentation|
|----|----|
|![](/img/infra-deploy.png)|Le [Projet Git "Carto4CH-deploy"](https://gitlab.huma-num.fr/4ch/carto4ch-deploy) permet à un partenaire d'installer son propre serveur Carto4CH.<br/><br/>Il contient 5 conteneurs docker<br/><ul><li>Traefik (gestion des ports et des certificats (https)</li><li>Jena fuseki (Triplestore)</li><li>Le middleware SemApps</li><li>Le back-office</li><li>My competence</li></ul>|

## Comment installer son serveur ?
### Les pré-requis
* Avoir un accès à internet
* Une configuration matérielle avec au moins 4Go de RAM
* Avoir installé [Docker](https://docs.docker.com/) et [docker-compose](https://docs.docker.com/compose/)
* Avoir installé [Make](https://www.gnu.org/software/make/manual/make.html)

### Clone du projet
Récupérez le projet sur votre serveur :

```git clone git@gitlab.huma-num.fr:4ch/carto4ch-deploy.git```

### Paramétrage de votre instance
Entrez dans le projet :

```cd carto4ch-deploy```

#### Nom de domaine dans traefik
Mettez à jour le fichier **docker-compose.yaml** à la racine avec votre nom de domaine réservé pour votre instance.

```
  data:
    build:
...
    expose:
      - "3000"
    labels:
      - "traefik.enable=true"
      - "traefik.http.routers.data.rule=Host(`data.carto4ch.<nom de domaine>`)"
...
```

#### Paramétrage du back-office (bo)
Mettez ensuite à jour le nom de l'instance, les logos et les couleurs en modifiant les **AddOn** du répertoire **bo**.

Ces AddOn viennent surcharger certains fichiers du projet [Git de base](https://gitlab.huma-num.fr/4ch/archipelago-carto4ch).

Tout d'abord le fichier **.env.local**

```
SEMAPPS_INSTANCE_NAME=Back-office (Serveur A) // Mettez à jour votre nom d'instance
REACT_APP_MIDDLEWARE_URL=https://data.carto4ch.huma-num.fr/ // Mettez à jour votre nom de domaine
PORT=4000 // Nous conseillons de conserver les ports précisés dans la documentation
REACT_APP_LANG=fr // Choisissez la langue
```

Le répertoire **public** contient les images : 
* favicon.ico
* logo192.png
* logo512.png
Peuvent être remplacés par les images de votre choix

Le fichier **public/index.html** contient le nom de l'instance à plusieurs endroits

```
    <meta
      name="description"
      content="Back-office [Server A]"
    />
...
    <title>Back-office [Server A]</title>
```

Le fichier **src/App.js** contient : 
* les couleurs du bandeau et de la police

```
theme.palette.primary.main = "#AE4796"
```

* encore le nom de l'instance (react-admin)

```
const App = () => (
  <Admin
    disableTelemetry
    history={history}
    title="Server A"
```

Le fichier **src/config/dataServer.js** si vous souhaitez vous connecter à un autre serveur ou gérer un miroir (voir documentation sur l'interopérabilité).

#### Paramétrage de My competence (my)
Comme pour le Back-office, vous pouvez mettre à jour le nom de l'instance, les logos et les couleurs en modifiant les **AddOn** du répertoire **my**.

Ces AddOn viennent surcharger certains fichiers du projet [Git de base](https://gitlab.huma-num.fr/4ch/my-competences).

### Déploiement

Lancez l'installation :

```make start-prod```

Vous pouvez aussi le faire en deux fois :

```make build-prod``` suivi de ```make start-prod```

Pour vérifier que tout s'est bien passé, vous pouvez consulter les logs avec la commande :

```make log-prod```

### Accès

Une fois que vous aurez déployé votre serveur (et configuré votre DNS), vous pourrez accéder à : 
* https://data.carto4ch.votre-nom-de-domaine // Pour accéder au middleware et parcourir les données en LDP.
* https://bo.carto4ch.votre-nom-de-domaine // Pour accéder au back-office
* https://my.carto4ch.votre-nom-de-domaine // Pour accéder à My-competence

### Authentification
Pour tout ce qui concerne l'authentification (configuration d'un serveur SSO ou de celui des communs), merci de nous contacter.

### Page d'accueil
Vous pouvez alors configurer une page d'accueil (Menu > Config > Page), en lui donnant le nom "accueil" (ou en modifiant le nom attendu dans le code), et en complétant sa description. Elle apparaitra au lancement de l'application.
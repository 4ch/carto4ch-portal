---
title: SemApps
sidebar_position: 3
---

# SemApps
```
┌────────────────────────────┐      ┌───────────────────────────┐
│    Serveur SemApps         |      │  Interface utilisatrice   |
│----------------------------│      │---------------------------│
│ ┌────────────────────────┐ │      │ ┌───────────────────────┐ │
│ │  Middleware (Node JS)  │ │      │ │     React-Admin       │ │
│ │------------------------│ │      │ │-----------------------│ │
│ │ ┌────────────────────┐ │ │<-----│ │ ┌───────────────────┐ │ │
│ │ │ Triplestore (Java) │ │ │      │ │ │ Composants SemApps│ │ │
│ │ └────────────────────┘ │ │      │ │ └───────────────────┘ │ │
│ └────────────────────────┘ │      │ └───────────────────────┘ │
└────────────────────────────┘      └───────────────────────────┘
```
## Introduction
SemApps est une boite à outils open-source, permettant de réaliser des applications sémantiques.

SemApps s'inspire très fortement des propositions du W3C : [SOLID](/docs/technical-documentation/standards/presentation-solid).

SemApps est développé par des membres de l'[Assemblée Virtuelle](https://virtual-assembly.org).

Pour mieux comprendre la notion de "boite à outils", vous pouvez consulter le [schéma d'architecture logicielle ici](/docs/schemas/architecture).

## Présentation (non technique) de SemApps

Cette présentation permet de rentrer doucement dans la compréhension de SemApps.

**Non technique**, signifie qu'on se permet d'utiliser des termes techniques, mais uniquement s'ils sont explicités.

Elle revient sur la notion de **web distribué**, sur **les standards** utilisés (web sémantique, LDP, SPARQL, WAC, Activity Pub), les différentes **interfaces utilisatrices** possibles et sur la **modularité** qui permet à SemApps de construire des applications sémantiques.

On peut la considérer comme **un pré-requis** à la lecture de cette page.

:::info Accès
* [Lien en mode présentation](https://pad.lescommuns.org/p/SlideSemapps)
* [Lien vers la source](https://pad.lescommuns.org/SlideSemapps)
:::

:::caution
Pour le moment, vu que cette présentation est encore jeune et peut être amenée à être modifiée, nous utilisons un **PAD collaboratif** pour la mettre à jour plus facilement.
:::

## Le site web de SemApps
|||
|----|----|
|![](/img/semapps-org.png)|https://semapps.org

Ce site présente : 
* la **boite à outil**, 
* l'[équipe SemApps](https://semapps.org/team) qui contribue au projet, 
* les **projets réalisés**, 
* **la documentation** de tous les **composants réutilisables** (voir le détail plus bas dans cette page...).

## Rappel : Liberté dans le choix des technologies pour Carto4CH

![](/img/liberte-choix-techno.png)

Pour rappel, ce que nous proposons ici n'est qu'un démonstrateur, un socle technique choisi pour l'exemple.

Nous avons choisi d'utiliser **SemApps**, mais un partenaire peut choisir d'autres briques logicielles, du moment qu'il partage **les standards du web sémantique**, et **l'ontologie HeCo**.

Via ce démonstrateur, nous souhaitons juste montrer qu'il est possible de créer des applications interopérables.

**Exemple :**
Un partenaire peut réécrire des interfaces en utilisant un autre langage que React JS (par exemple Angular JS). 
Il peut aussi développer son propre serveur de données sémantiques en utilisant un autre language que Node JS (par exemple en Django), du moment qu'il se base sur les propositions du W3C [SOLID](/docs/technical-documentation/standards/presentation-solid).

## Technologies utilisées pour le démonstrateur
|Schéma du démonstrateur|Architecture technique|
|----|----|
|![](/img/demonstrator-architecture-schema.png)|**Interfaces utilisatrices** <br/>- Front-Office et My-competences : [React JS](https://fr.reactjs.org/) / [React-Admin](https://marmelab.com/react-admin/)<br/>- Cartographie en réseau : [D3js](https://d3js.org/)<br/><br/>**Partie serveur**<br/>[Node JS](https://nodejs.org/en/) / [Moleculer](https://moleculer.services/)<br/><br/>**Protocoles**<br/>[LDP](https://www.w3.org/TR/ldp/)<br/>[SPARQL 1.1](https://www.w3.org/TR/sparql11-protocol/)<br/>[WAC](https://www.w3.org/wiki/WebAccessControl)<br/>[Activity Pub](https://www.w3.org/TR/activitypub/)|

Ces choix techniques ont été pris par l'[Assemblée virtuelle](https://virtual-assembly.org) après avoir essayé d'autres technologies (JAVA / PHP) dans les premières versions de SemApps.

## Fonctionnement technique de SemApps

### La partie serveur
|Schéma|La partie serveur est composée de deux éléments|
|----|----|
|![](/img/partie-serveur.png)|<br/>- **La base de donnée** (triplestore) : qui stoque les triplets sémantiques<br/>- **Le middleware** : écoute les demandes de l'interface utilisatrice, récupère les éléments demandés en base et les renvoit à l'interface.

La partie serveur est réalisée en Javascript, principalement autour de deux librairies : 
* [Node JS](https://nodejs.org/en/), qui est la technologie la plus utilisée pour créer des serveurs en Javascript
* [Moleculer](https://moleculer.services/), qui est une sorte d'ordonanceur, qui fait le chef d'orchestre entre tous les "services".

### Les interfaces utilisatrices
![](/img/interfaces-utilisatrices-semapps.png)

Les interfaces utilisatrices du démonstrateur Carto4CH est basée sur la boite à outil **SemApps**, qui elle même est dépendante d'un socle technique (framework) appelé [**React-Admin**](https://marmelab.com/react-admin/). Ce socle technique est maintenu par la société (française) [**Marmelabs**](https://marmelab.com/fr/). Il utilise la bibliothèque JavaScript [**React**](https://fr.legacy.reactjs.org/).

#### Le framework React-Admin
![](/img/react-admin-exemple.png)

Cette librairie React permet de réaliser en très peu de temps, une interface permettant de : 
* **Créer** des données
* **Afficher** et lister des données
* **Modifier** des données
* **Supprimer** des données

(voir [**CRUD**](https://fr.wikipedia.org/wiki/CRUD))

Nous l'avons utilisé pour le **back-office** et pour **My-competence**.

## Les composants réutilisables
![](/img/composants-reutilisables.png)

### Introduction
La boite à outils SemApps est basé sur des composants "atomiques" réutilisables.

Il existe deux types de composants : 
* **des composants pour le middleware**, par exemple pour aller lire dans la base de donnée, pour importer des données, ou pour formater les données, etc. Voir [Semapps.org > Middleware](https://semapps.org/docs/middleware)
* **ceux pour les interfaces utilisatrices**, par exemple des menus, des titres, des listes, des inputs, des boites de sélections, etc. Voir [Semapps.org > Frontend](https://semapps.org/docs/frontend)

Cela permet que chaque composant soit maintenu indépendemment, plutôt qu'un gros bloc géré par une seule organisation. On peut à souhait enrichir les composants de base et en proposer de nouveaux.

Chaque composant possède sa documentation, avec des exemples de code.

### Trois cas pour le frontend (React)
Trois cas peuvent se présenter : 
* soit nous utilisons des composants de React JS
* soit nous utilisons des composants de la librairie React-Admin, qui sont souvent issus de React JS, mais complétés par l'équipe de Marmelab pour simplifier leur utilisation
* soit nous surchargeons ces composants pour répondre aux besoins de SemApps

### Accès aux composants
Les composants sont mis à disposition sur le **dépot NPM**, et vous pouvez retrouver leur code sur le [Github de l'assemblée virtuelle](https://github.com/assemblee-virtuelle/semapps) : 
* Pour les [composants frontend](https://github.com/assemblee-virtuelle/semapps/tree/master/src/frontend/packages)
* Pour les [Composants middleware](https://github.com/assemblee-virtuelle/semapps/tree/master/src/middleware/packages)

### Accès à la documentation des composants
Pour avoir accès à la documentation technique des composants réutilisables, rendez-vous sur le site https://semapps.org

## Archipelago

|||
|-|-|
|![](/img/archipelago.png)|La première implémentation de SemApps réalisée, est le projet [Archpelago](https://github.com/assemblee-virtuelle/archipelago). Son objectif est de cartographier les archipelles de la transition, en étant installé sur chaque archipelle.|

Ce projet contient : 
* Le middleware de SemApps
* Une interface basée sur React-Admin
* L'ontologie **P.A.I.R**

C'est sur cette base que nous avons construit **le back-office** du démonstrateur Carto4CH.

## L'ontologie P.A.I.R

|||
|-|-|
|![](/img/ontologie-pair.jpg)|L'[Assemblée Virtuelle](https://virtual-assembly.org) propose depuis plusieurs années une ontologie permettant de cartographier les écosystèmes de **P**rojets, **A**cteurs, **I**dées, **R**essources, mais aussi des évènements.|

Plus d'informations sur le [Blog de l'Assemblée virtuelle](https://www.virtual-assembly.org/ontologie-pair/)

Pour Carto4CH, nous avons au début utilisé cette ontologie, et peu à peu, nous l'avons remplacé par **[l'ontologie HeCo](https://portal.carto4ch.huma-num.fr/en/docs/technical-documentation/ontology)**, car SemApps est compatible avec n'importe quelle ontologie du Web sémantique.

## Installation d'Archipelago en local

### Caractéristiques techniques pour faire tourner Archipelago
* un accès à internet
* Une configuration matérielle avec au moins 4 Go de RAM
* Installer [Node JS](https://nodejs.org/en)

:::caution Attention
Il semble qu'il y ait une erreur lors du lancement du frontend avec la version Node JS 18 (cela fonctionne en version 16.19)
:::

### Procédure sur le Gitlab
Pour installer simplement Archipelago en local de votre ordinateur, suivez les étapes précisées dans le [Github d'Archipelago-Carto4CH](https://gitlab.huma-num.fr/4ch/archipelago-carto4ch).

**En résumé** :
Trois étapes : 
* Lancer la base de donnée
* Lancer le middleware
* Puis enfin l'interface utilisatrice

## Déploiement d'Archipelago

|||
|-|-|
|![](/img/deploiement-carto4ch.png)|Le déploiement d'Archipelago est assez simple, car il utilise **Docker** et **Make**. Une fois Docker installé sur un serveur, en une ligne, on peut donc lancer un serveur fonctionnel.<br/><br/>Il lancera **traefik** (réseau, ports, https, certificats), **le triplestore**, le **middleware** et le **frontend**|

## Utilisation de SemApps pour le démonstrateur Carto 4CH

Nous avons utilisé les briques de SemApps pour le démonstrateur.

Pour comprendre le code du démonstrateur, voir la [documentation développeur du démonstrateur](/docs/technical-documentation/demonstrator-manual)

## Internationalisation
Pour gérer l'internationalisation (EN/FR), nous nous sommes basé sur la documentation [React-Admin](https://marmelab.com/react-admin/Translation.html)

**En résumé :**
* Mettre à jour le fichier ```.env.local``` avec ```REACT_APP_LANG=fr```
* Paramétrer le **i18nProvider** (voir doc RA officielle) 
* Et renseigner les fichiers : 
    * frontend/src/config/messages (contenant les traductions EN/FR)
    * Ou les parties "translations" dans les fichiers **index.js**
* Enfin, utiliser le hook **useTranslate** pour utiliser les traductions dans le code souhaité.

## Pour approfondir SemApps
L'Assemblée Virtuelle maintient une **documentation de formation** sur son Github.
* Voir : [Formation SemApps](https://github.com/assemblee-virtuelle/semapps/wiki/Formation-SemApps-%28en-fran%C3%A7ais%29)

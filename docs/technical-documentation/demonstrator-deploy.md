---
title: Déploiement du démonstrateur
sidebar_position: 6
---

# Déploiement du démonstrateur Carto4CH

## Introduction

Cette documentation explique comment sont **hébergées** et **publiées** les applications du démonstrateur.

## Pré-requis techniques

* un accès à internet
* Une configuration matérielle avec au moins 16 Go de RAM
* Installer [Docker](https://docs.docker.com/) et [docker-compose](https://docs.docker.com/compose/)
* Installer [Make](https://www.gnu.org/software/make/manual/make.html)

## Architecture générale

|Rappel de l'architecture|Les briques documentées|
|----|----|
|![](/img/demonstrator-architecture-schema.png)|Le **démonstrateur** est une cartographie fonctionnelle de démonstration.<br/>Son objectif est de donner un exemple de ce qui est attendu par le projet Carto4CH.<br/><br/>Nous expliquerons dans cette documentation l'hébergement de ce démonstrateur et le fonctionnement de chacune de ses briques : <br/><ul><li>**Les deux instances de données** de démonstration (Instance A et Instance B)</li><li>**Les 3 applications** permettant de cartographier les données dans chaque instance</li><li>L'**instance miroir**</li><li>Les **visualisation en réseau**</li></ul>|

## Déploiement via Docker

|Projets d'origines|Git clone + AddOn|
|----|----|
|![](/img/demonstrator-docker.png)|Chaque projet docker d'un **serveur A ou B** effectue un **git clone** des projets **Archipelago-carto4CH** (middleware et frontend) ou **My-competence**. <br/><br/>Ensuite, il récupère ses fichiers spécifiques dans le répertoire **AddOn** pour customiser les couleurs, le titre ou les paramètres d'interopérabilité|

## Briques déployées

Vous trouverez sur le [Gitlab Huma-Num du projet Carto 4CH > Carto 4CH](https://gitlab.huma-num.fr/4ch/carto4ch), l'ensemble des configurations docker permettant de faire tourner le démonstrateur sur le serveur Huma-Num.

La configuration Docker est constituée de 11 conteneurs dockers.

|Rappel de l'infrastructure|Les containers|
|----|----|
|![](/img/demonstrator-hosting.png)|**Les conteneurs "supports"** <br/><br/>- **Traefik** (gestion du réseau, ports, https, certificats)<br/>- **Jena/Fuseki** (Triplestore)<br/>- **Redis** (cache)<br/><br/>**Les conteneurs "applicatifs"**<br/><br/>De l'**instance A** <br/>- Middleware A (data-a)<br/>- BO A<br/>- MY A <br/>De l'**instance B**<br/>- Middleware B (data-b)<br/>- BO Serveur B<br/>- MY Serveur B<br/><br/>De l'instance **miroir** (C)<br/><br/>Du **portail Carto 4CH**|

Tous ces containers peuvent être lancés par une simple commande : 
```make start-prod```

Cette commande appelle le fichier [docker-compose-prod.yaml](https://gitlab.huma-num.fr/4ch/carto4ch/-/blob/main/docker-compose-prod.yaml?ref_type=heads), qui lance les 11 conteneurs.

Il est possible de voir les logs du serveur en utilisant la commande : ```make log-prod```

## Fonctionnement des conteneurs applicatifs

### Principe de fonctionnement
Chaque conteneur applicatif commence par faire un ```git clone``` d'un des projets Carto 4CH (Archipelago ou My-competences), puis écrase certains fichiers par une configuration spécifique pour l'instance.

### Les middlewares A/B/C
Les 3 middlewares (data-a, data-b et data-c) clonent le projet [Archipelago Carto4CH > middleware](https://gitlab.huma-num.fr/4ch/archipelago-carto4ch/-/tree/main/middleware?ref_type=heads), et surchargent les fichiers présents dans le répertoire **AddOn**.

Dans le cas du miroir, on ajoute le fichier **services/mirror.service.js**.

### Les Back-office A/B
Les 2 back-office (bo-a et bo-b) clonent le projet [Archipelago Carto4CH > frontend](https://gitlab.huma-num.fr/4ch/archipelago-carto4ch/-/tree/main/frontend?ref_type=heads), et surchargent leurs fichiers en récupérant ceux présents dans le répertoire **AddOn** : 
* titre "Server A"
* couleurs, 
* configuration d'interopérabilité : en effet, nous allons permettre au serveur A de voir les données du serveur B et inversement.

### My-competences A/B
Les 2 interfaces My-competences (my-a et my-b) clonent le projet [My-competences](https://gitlab.huma-num.fr/4ch/my-competences), et surchargent leurs fichiers en récupérant ceux  présents dans le répertoire **AddOn** : 
* titre, 
* couleurs
* configuration d'interopérabilité

## Les visualisations

Le démonstrateur propose 3 visualisations : 
* Une visualisation des données du middleware A
* Une visualisation des données du middleware B
* Une visualisation des données du middleware C (miroir) pour la visualisation générale

Pour en savoir plus, lire [Visualisation en réseau > démonstrateur](/docs/technical-documentation/visualisation#fonctionnement-pour-le-démonstrateur)


---
title: Accueil
sidebar_position: 0
---

# Accueil

## Introduction

Nous présentons ci-dessous **le projet européen 4CH**, puis **l'initiative Carto4CH**, et enfin **les porteurs de cette initiative**.

## Le projet européen 4CH (CCCCH)
Le projet 4CH est une étude européenne sur la potentialité de créer un **Centre de Compétence européen sur la Conservation du patrimoine Culturel**. Cette étude va durer 3 ans (2021-2023).
Une vingtaine d'équipes de différents pays interviennent dans ce projet, dont la France, qui propose **une cartographie des acteurs et des compétences en France** dans ce domaine.
* [Site de référence](https://4ch-project.eu)
* [Page du projet sur le site de la MSH Val de Loire](https://www.msh-vdl.fr/projet/4ch-project/)

## La cartographie des acteurs et des compétences (Carto4CH)
L'objectif de l'initiative Carto4CH est de proposer à la communauté 4CH une **cartographie sémantique distribuée** des acteurs et des compétences dans le patrimoine culturel Français.
Ainsi, chaque partenaire possèdera son propre environnement, conservera ses données, et choisira de publier ses acteurs et ses compétences dans la cartographie générale.
Chaque serveur utilisera un socle technique commun, contenant des standards et une **ontologie partagée**.
C'est ce socle que nous présentons sur ce portail.
* Plus d'information dans la [Présentation de Carto4CH](/docs/project-documentation/presentation-generale)

## Les porteurs du projet Carto4CH

|[Université de Tours](https://www.univ-tours.fr/)|[LIFAT](https://lifat.univ-tours.fr/)|[CNRS](https://www.cnrs.fr/fr)|[CITERES](http://citeres.univ-tours.fr/)|[MSH Val de Loire](https://www.msh-vdl.fr/)|
| -------- | -------- | ------ | ------ | ------ |
|![](/img/logo-UniversitedeTours.png)|![](/img/lifat.jpg)|![](/img/CNRS-Logo-fond-blanc.jpg)|![](/img/logoCITERES.png)|![](/img/logo_msh.jpg)|

---
title: RGPD
sidebar_position: 3
---

# RGPD & ActivityPOD

```
┌────────────────────────────────────────────┐
│        RGPD by SOLID (ActivityPOD)         |
│--------------------------------------------│
│       Application                Data      |
│--------------------------------------------│
│   ┌─────────────────┐            DB        |
│   │                 │       ┌───────────┐  |
│   │                 │       |   App     │  │
│   │                 │  <=>  |   Data    |  │
│   │      User       │       └───────────┘  │
│   │    Interface    │                      │
│   │                 │            POD       │
│   │                 │       ┌───────────┐  │
│   │                 │       |  Personal |  │
│   │                 │  <=>  |  Data     │  |
│   └─────────────────┘       └───────────┘  │
└────────────────────────────────────────────┘
```

## Introduction

Nous apporterons dans cette page nos recommandations concernant le RGPD, dans le cadre du projet Carto4CH.
Nous aurions pu appliquer les mêmes attentions que dans un site web habituel, en suivant les [préconisations du RGPD](https://www.cnil.fr/fr/rgpd-de-quoi-parle-t-on), mais nous souhaitons profiter de ce projet (pour le moment expérimental) pour proposer à l'avenir une architecture qui serait RGPD par essence.

## Généralités
L'inventeur du web (Tim Berners Lee) propose depuis une dizaine d'années une nouvelle architecture [SOLID](/docs/technical-documentation/standards/presentation-solid) afin que chaque humain soit responsable de ses données personnelles.

L'Europe nous demande de respecter le RGPD, ce qui revient globalement au même.

L'[Assemblée Virtuelle](https://virtual-assembly.org/) propose donc de créer de nouvelles applications qui séparent les données applicatives des données personnelles.

Cela va s'appeler des **POD** (Personnal Online Data).

Chaque humain pourra avoir un POD, qu'il décidera de stocker chez le fournisseur de POD de son choix (chaton, mairie, département, région, gouvernement...).

Le POD détiendra aussi ses **clés d'authentification**, ce qui évitera d'avoir à recréer un login/mdp sur chaque application.

## ActivityPOD = SOLID + Activity POD
Pour répondre à ce besoin, l'[Assemblée virtuelle](https://virtual-assembly.org/) propose une technologie basé sur des standards (même si SOLID ne l'est pas encore), et appelé ActivityPOD.

**ActivityPOD** est une nouvelle technologie, basée sur la boite à outils [SemApps](https://semapps.org), qui utilise principalement deux projets :
* [SOLID](https://fr.wikipedia.org/wiki/Solid_(projet_de_web_d%C3%A9centralis%C3%A9)), 
* [ActivityPUB](https://fr.wikipedia.org/wiki/ActivityPub).

Vous trouverez plus d'informations sur le site https://activitypods.org/.

## Conclusion
Grâce à cette solution technique, les utilisateurices de Carto4CH pourront enregistrer leurs compétences et autres données personnelles dans leur POD, et pourront choisir de les ouvrir aux applications qui souhaitent les afficher.

Les utilisateurices resteront donc responsable de leurs données personnelles.

Evidemment, il faudra aussi respecter le fonctionnement et les contrats entre les personnes et les organisations. Il y aura donc un défi à la fois technique et organisationnel.

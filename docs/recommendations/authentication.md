---
title: Authentification
sidebar_position: 2
---

# Recommandations sur l'authentification

```
┌────────────────────────────────────────────┐
│     Choix d'authentification / SOLID-OIDC  |
│--------------------------------------------│
│       Application            choix         |
│   ┌───────────────┐                        │
│   │  ┌─────────┐  │          ┌─────────┐   │
│   │  |  Login  │  │          |  SOLID- │   │
│   │  └─────────┘  │  <====>  |  OIDC   │   │
│   │  ┌─────────┐  │          └─────────┘   │
│   │  |  pass   │  │          ┌─────────┐   │
│   │  └─────────┘  │  <====>  |  CAS    │   |
│   └───────────────┘          └─────────┘   │
└────────────────────────────────────────────┘
```

## Introduction

Nous apporterons dans cette page nos recommandations concernant l'authentification dans SemApps. Ce sont des proposition d'amélioration à pousser dans l'avenir pour faciliter son appropriation dans différents contextes.

## Proposer plusieurs types d'authentification
Un premier point d'amélioration est que nous ne pouvons pas, à l'heure actuelle, offrir à l'utilisateur plusieurs choix d'authentification (comme on peut le voir sur certains sites qui propose de nous connecter avec Google, Facebook, mail, etc...).

La solution, pour le démonstrateur (en attendant...), sera d'utiliser : 
* Pour le serveur A : le SSO des communs
* Pour le serveur B : le SSO d'INFN

## SOLID-OIDC
La communauté SOLID a mis au point des spécifications d'un standard d'authentification appelé **SOLID-OIDC** : https://solidproject.org/TR/oidc.

L'équipe SemApps a commencé à s'y intéresser en fin d'année 2023, en intégrant une partie des spécifications. Nous attendons un financement supplémentaire pour terminer cette compatibilité.

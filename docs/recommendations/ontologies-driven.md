---
title: Pilotage par l'ontologie
sidebar_position: 2
---

# Pilotage par l'ontologie

```
┌────────────────────────────────────────────┐
│ Et si l'ontologie pilotait l'application ? |
│--------------------------------------------│
│       Ontologie              Application   |
│   ┌───────────────┐                        │
│   │    O---O---O  │                        │
│   │   /   / \  |  │          ┌─────────┐   │ 
│   │  O---O---O-O  │   ====>  |   App   │   │
│   │   \ /     \|  │          └─────────┘   │
│   │   O-O---O--0  │                        |
│   └───────────────┘                        │
└────────────────────────────────────────────┘
```

## Introduction

Un vieux rêve des ontologistes (informatiques) est de pouvoir gérer / paramétrer / piloter le fonctionnement d'une application directement via une ou plusieurs ontologies.

Nous allons voir dans cette page quel est le lien entre l'application et l'ontologie.

## Exemple avec Carto4CH
Dans le projet Carto4CH, si par exemple, nous souhaitions ajouter une nouvelle qualité (autre que discipline, secteur, ...), ou encore modifier le nom d'une de ces qualité, il "suffirait" de mettre à jour la classe de l'ontologie, et vu que les applications seraient directement lié à l'ontologie, elles se mettraient automatiquement à jour.

## Complexité de mise en place
En effet, cette vision est très intéressante, mais très complexe à mettre en place. 
Au niveau de la boite à outil SemApps, nous commençons tout juste à apporter des améliorations dans ce sens.

## Cas des relations inverses
Un premier "pilotage par l'ontologie" a été fait pour les relations inverses.
En effet, pour rechercher des relations inverses, **SemApps consulte l'ontologie Heco** (voir les explications dans la rubrique [Relations inverses](/docs/technical-documentation/standards/inverse-relationship)). Il faut donc être très prudent lors de la modification de l'ontologie en ligne, car si vous faites une erreur, cela peut avoir de graves conséquences sur le fonctionnement des relations inverses, et donc sur le fonctionnement global de l'application.

## Duplication de la logique
Mais pour tout le reste, la logique de l'ontologie est "dupliquée" dans le code, ce qui "fige" un peu les choses. Lorsque nous devons modifier l'ontologie, il faut aussi modifier le code. C'est ainsi pour le moment, mais nous réfléchissons régulièrement à faire mieux.

## Amélioration de la gestion des ontologies et du contexte JSON-LD
Fin décembre 2023, une amélioration de la gestion des ontologies et du contexte JSON-LD a été effectuée dans SemApps.

Elle a été mise en place dans Carto4CH en février 2024.

Elle permet de déclarer chaque ontologie avec son propre contexte JSON-LD, ce qui est beaucoup plus clair. SemApps se charge ensuite de faire le mixte pour regrouper tous les contextes en un seul, qui est alors disponible à l'adresse : 
https://data-a.carto4ch.huma-num.fr/.well-known/context.jsonld

---
title: Référentiels
sidebar_position: 4
---

# Référentiels / Vocabulaires

```
┌────────────────────────────────────────────┐
│      Reference systems / Vocabularies      |
│--------------------------------------------│
│       Application           Référentiels   |
│--------------------------------------------│
│   ┌─────────────────┐            R1        |
│   │      User       │       ┌───────────┐  |
│   │    Interface    │       |   voc1    │  │
│   │                 │  <=>  |   voc2    |  │
│   │   Choice list   │       └───────────┘  │
│   │      - voc1     │                      │
│   │      - voc2     │            R2        │
│   │      - voc3     │       ┌───────────┐  │
│   │      - voc4     │       |   voc3    |  │
│   │                 │  <=>  |   voc4    │  |
│   └─────────────────┘       └───────────┘  │
└────────────────────────────────────────────┘
```

## Introduction

Lors du projet Carto4CH, nous avons beaucoup réfléchi à la manière de maintenir des référentiels, ou de faire référence à des référentiels existants au niveau européen. Nous aurions souhaité les mettre en place, mais la tâche était trop complexe pour le peu de temps/énergie que nous avions.

Nous avons donc opté pour des listes internes à chaque serveur Carto4CH.
* Les utilisateurs peuvent peupler ces listes, aidés par des propositions venant de [ESCO](https://esco.ec.europa.eu/fr) ou [Wikidata](https://m.wikidata.org/wiki/Wikidata:Main_Page?uselang=fr).
* En utilisant l'interopérabilité entre les serveurs, il est possible de voir les listes du serveur A sur le serveur B.

C'est une solution acceptable pour un démonstrateur, mais pas pour une utilisation en production.

Dans cette page, nous proposons une future architecture pour mettre en place des référentiels (aussi appelés "vocabulaires") externes aux serveurs Carto4CH.

## Rappel
Pour comprendre de quoi nous parlons, vous pouvez relire [les prérequis de la documentation utilisateurice au sujet des vocabulaires](/docs/project-documentation/doc-demonstrator/requirements/referentiels).

## Des référentiels internes aux référentiels externes

### Référentiels internes

Pour le moment, chaque serveur possède ses propres listes.

```
┌──────────────────────────────────────────────────┐
│             Internal vocabularies                |
│--------------------------------------------------│
│        Server A                  Server B        |
│--------------------------------------------------│
│   ┌─────────────────┐       ┌─────────────────┐  |
│   │      User       │       │      User       │  |
│   │    Interface    │       │    Interface    │  |
│   │                 │  <=>  │                 │  │
│   │   Choice list   │       │   Choice list   │  │
│   │      - voc1     │       │      - voc3     │  │
│   │      - voc2     │       │      - voc4     │  │
│   └─────────────────┘       └─────────────────┘  │
│             ^                      ^             │
│             └──  Interoperability ─┘             │
└──────────────────────────────────────────────────┘
```

### Référentiels externes

Nous proposons de passer par des référentiels externes, qui seront gérés comme des communs par des communautés. Ils pourront être spécialisés dans un domaine, et être utilisés par n'importe quel serveur Carto4CH.

```
┌──────────────────────────────────────────────────┐
│             External vocabularies                |
│--------------------------------------------------│
│        Server A                  Server B        |
│--------------------------------------------------│
│   ┌─────────────────┐       ┌─────────────────┐  |
│   │      User       │       │      User       │  |
│   │    Interface    │       │    Interface    │  |
│   │                 │       │                 │  │
│   │   Choice list   │       │   Choice list   │  │
│   │      - voc1     │       │      - voc1     │  │
│   │      - voc2     │       │      - voc2     │  │
│   │      - voc3     │       │      - voc3     │  │
│   │      - voc4     │       │      - voc4     │  │
│   └─────────────────┘       └─────────────────┘  │
│             ^          \ /          ^            │
│             |           X           |            │
│             |          / \          |            │
│   ┌─────────────────┐       ┌─────────────────┐  |
│   │      Vocab 1    │       │      Vocab 2    │  |
│   │      - voc1     │       │      - voc3     │  |
│   │      - voc2     │       │      - voc4     │  │
│   └─────────────────┘       └─────────────────┘  │
└──────────────────────────────────────────────────┘
```

## Technologie / standards
Ces référentiels pourront d'ailleurs être des **thésaurus implémentés par des serveurs SOLID**, utilisant par exemple l'ontologie [SKOS](https://fr.wikipedia.org/wiki/Simple_Knowledge_Organization_System), comme celui que nous avons utilisé dans le [Tutoriel SOLID](/docs/technical-documentation/tutorials/solid) et le [Lexique de Carto4CH](/docs/project-documentation/glossary).


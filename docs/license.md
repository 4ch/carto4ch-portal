---
title: Licence
sidebar_position: 60
---

# Licence

## Introduction
Ce projet inclu un ensemble de livrables, et nous utilisons dans chaque livrable, des logiciels open-source dont nous devons citer la licence.

## La licence du projet Carto4CH
Globalement, le projet Carto4CH est publié sous licence [CC-BY-SA version 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.fr).

Cela inclu : 
* Le **portail**
* Le **livre blanc**
* Les **tutoriels** (et le code associé)
* L'**ontologie** HeCO
* Le **démonstrateur**
  * Archipelago-Carto4CH
  * My-competence
* **Carto4CH-deploy** (pour les partenaires qui souhaitent déployer leur serveur)

## Les projets utilisés et leur licence
Carto4CH utilise principalement les projets suivants : 
* **SemApps** : [Sous licence Apache](https://github.com/assemblee-virtuelle/archipelago/blob/master/LICENSE)
* **Minicourse** : [Sous licence Apache](https://github.com/assemblee-virtuelle/minicourses/blob/master/LICENSE)
* **Librairie SOLID-client** (pour les tutos) : [Sous licence MIT](https://github.com/inrupt/solid-client-js/blob/main/LICENSE)
* **Docusaurus** : [Sous licence MIT](https://github.com/facebook/docusaurus/blob/main/LICENSE)

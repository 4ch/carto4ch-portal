---
title: Ateliers compétences
sidebar_position: 40
---

# Ateliers compétences

## Introduction

Nous organisons depuis octobre 2022 des **ateliers compétences**.
Ils permettent : 
* aux partenaires de participer aux réflexions, de proposer des améliorations.
* à l'équipe Carto4CH de tester le démonstrateur en conditions réelles.

## Prochains ateliers

:::caution
Nous allons bientôt reprogrammer des ateliers pour accompagner les partenaires dans leremplissage de leurs compétences.
:::

## Objectifs

* Recueillir régulièrement l'avis et les besoins des partenaires
* Présenter les nouveautés de l'[ontologie HeCo](./technical-documentation/ontology)
* Présenter les nouveautés du démonstrateur
* Améliorer l'expérience utilisateur
* Accompagner les utilisateurs dans la saisie des compétences
* Adapter le [Calendrier](./planning)
* Former les développeurs partenaires

## Quand ?

Les dates seront communiquées par mail, et les destinataires seront choisis en fonction des thèmes abordés.

## Fréquence

Les ateliers seront réalisés suite à des périodes de développement.

Une première série d'ateliers sera proposée fin 2022, pour recueillir les demandes d'amélioration, puis une autre début 2023, suite à la réalisation des améliorations, et ainsi de suite.

## Méthode

Ces ateliers se feront en ligne, le lien visio sera communiqué au préalable par mail.

Les personnes invitées seront issues de plusieurs organisations.

Certains ateliers seront plus techniques, feront intervenir des experts, afin de répondre à des besoins précis.
D'autres seront plus ouverts, adapté aux non initié.e.s, a vocation de partage, de vulgarisation et de demande d'avis, de besoins terrains.

## Historique des ateliers

* Vendredi **7/04/23** : Test des interfaces BO et MY
* Vendredi **31/03/23** : Test des interfaces BO et MY
* Vendredi **24/03/23** : Test des interfaces BO et MY
* Jeudi **8/12/22 à 15h** : un atelier de synthèse collaborative
* Mardi **6/12/22 à 10h** : un atelier orienté sur l'ontologie et les référentiels (pas besoin d'être ontologiste pour participer)
* Vendredi **25/11/22 à 15h** : un atelier pratique de saisie collaborative sur l'interface SemApps en ligne (sur le démonstrateur) de saisie des compétences, pour avoir vos retours, et discuter ensemble des améliorations à apporter
* Lundi **21/11/22 à 15h** : un atelier d'introduction aux partenaires, présentant le projet, l'état des lieu, et nos réflexions sur la manière de gérer et de saisir des compétences
* **14/10/2022** : Présentation de l'[ontologie HeCo](./technical-documentation/ontology) aux membres de l'Assemblée Virtuelle.

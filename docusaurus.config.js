// @ts-check
// Note: type annotations allow type checking and IDEs autocompletion

const {themes} = require('prism-react-renderer');
const lightTheme = themes.github;
const darkTheme = themes.dracula;

/** @type {import('@docusaurus/types').Config} */
const config = {
  title: 'Cartographie des acteurs et des compétences',
  tagline: 'Voir fichier pages/index.js',
  url: 'https://portal.carto4ch.huma-num.fr',
  baseUrl: '/',
  onBrokenLinks: 'throw',
  onBrokenMarkdownLinks: 'warn',
  favicon: 'img/favicon.ico',

  // GitHub pages deployment config.
  // If you aren't using GitHub pages, you don't need these.
  organizationName: 'MSH VdL', // Usually your GitHub org/user name.
  projectName: 'carto4ch', // Usually your repo name.

  // Even if you don't use internalization, you can use this field to set useful
  // metadata like html lang. For example, if your site is Chinese, you may want
  // to replace "en" with "zh-Hans".
  i18n: {
    defaultLocale: 'fr',
    locales: ['fr', 'en'],
  },

  presets: [
    [
      'classic',
      /** @type {import('@docusaurus/preset-classic').Options} */
      ({
        docs: {
          sidebarPath: require.resolve('./sidebars.js'),
          // Please change this to your repo.
          // Remove this to remove the "edit this page" links.
          // editUrl:
          //   'https://github.com/facebook/docusaurus/tree/main/packages/create-docusaurus/templates/shared/',
        },
        blog: {
          showReadingTime: false,
          // Please change this to your repo.
          // Remove this to remove the "edit this page" links.
          // editUrl:
          //   'https://github.com/facebook/docusaurus/tree/main/packages/create-docusaurus/templates/shared/',
          blogSidebarCount: 100,
          postsPerPage: 10,
        },
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
      }),
    ],
  ],

  themeConfig:
    /** @type {import('@docusaurus/preset-classic').ThemeConfig} */
    ({
      navbar: {
        title: 'Carto 4CH',
        logo: {
          alt: 'Carto 4CH',
          src: 'img/logo-carto4ch.png',
        },
        items: [
          {
            to: '/docs/home',
            position: 'left',
            label: 'navbar-item-1',
          },
          {
            to: '/docs/category/applications-category-label',
            position: 'left',
            label: 'navbar-item-2',
          },
          {
            to: '/docs/category/project-documentation-category-label',
            position: 'left',
            label: 'navbar-item-3',
          },
          {
            to: '/docs/category/technical-documentation-category-label',
            position: 'left',
            label: 'navbar-item-4',
          },
          {
            to: 'docs/category/schemas-category-label',
            position: 'left',
            label: 'navbar-item-5',
          },
          {
            to: '/docs/workshops',
            position: 'left',
            label: 'navbar-item-6',
          },
          {
            to: '/docs/planning',
            position: 'left',
            label: 'navbar-item-7',
          },
          {
            to: '/blog',
            position: 'left',
            label: 'navbar-item-8',
          },
          {
            to: '/docs/contacts',
            position: 'right',
            label: 'navbar-item-9',
          },
          {
            href: 'https://gitlab.huma-num.fr/4ch',
            position: 'right',
            label: 'navbar-item-10',
          },
          {
            type: 'localeDropdown',
            position: 'right',
          },
        ],
      },
      footer: {
        style: 'dark',
        links: [
          {
            title: 'footer-title-1',
            items: [
              {
                label: 'footer-item-1',
                to: '/docs/category/project-documentation-category-label',
              },
              {
                label: 'footer-item-2',
                to: '/docs/category/technical-documentation-category-label',
              },
            ],
          },
          {
            title: 'footer-title-2',
            items: [
              {
                label: '4CH-project',
                href: 'https://4ch-project.eu',
              },
            ],
          },
          {
            title: 'footer-title-3',
            items: [
              {
                label: 'Gitlab Carto4CH',
                href: 'https://gitlab.huma-num.fr/4ch/',
              },
            ],
          },
        ],
        copyright: `footer-copyright`,
      },
      prism: {
        theme: lightTheme,
        darkTheme: darkTheme,
      },
    }),
};

module.exports = config;
